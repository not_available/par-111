\documentclass[12pt]{article}

\usepackage[french]{babel}
\usepackage[utf8x]{inputenc}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{subcaption}

\usepackage{hyperref}
\usepackage{url}

\usepackage{fancyhdr}
\usepackage{vmargin}

%\setmarginsrb{2.5 cm}{2.2 cm}{2.5 cm}{2.2 cm}{1 cm}{1.5 cm}{1 cm}{1.5 cm}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\newcommand*\Diff{\mathop{}\!\mathrm{D}}
\renewcommand{\refname}{{\small Références bibliographiques}}

\setlength\headheight{26pt} 
\hypersetup{
    colorlinks=true,
    linkcolor=purple,
    filecolor=magenta,      
    urlcolor=cyan,
    citecolor=blue,
}

\title{}
\author{}
\date{\today}

\makeatletter
\let\thetitle\@title
\let\theauthor\@author
\let\thedate\@date
\makeatother

\pagestyle{fancy}
\fancyhf{}
\lhead{\leftmark}
\cfoot{\includegraphics[scale=.22]{images/logo_ecl.jpg}}
\rhead{Page \thepage}

\begin{document}

\begin{titlepage}
	\centering
    \vspace*{0.5 cm}
    \includegraphics[scale = 0.75]{images/logo_ecl.jpg}\\[1.0 cm]	
    \textsc{\LARGE \newline\newline Compte rendu 2}\\[2.0 cm]	
	\textsc{\Large UE Projet d'Application recherche}\\[0.5 cm]
	\rule{\linewidth}{0.2 mm} \\[0.4 cm]
	{ \huge \bfseries \thetitle Étude mathématique et numérique des équations de Lorenz}\\
	\rule{\linewidth}{0.2 mm} \\[1.5 cm]
	
	\begin{minipage}{0.5\textwidth}
		\begin{flushleft} \large
			\emph{Élèves :}\\
			\textsc{Beurville} Allan\\
		\end{flushleft}
	\end{minipage}~
	\begin{minipage}{0.4\textwidth}   
		\begin{flushright} \large
			\emph{Responsable de l'activité :} \\
			\textsc{Nouguier} Cécile\\[.2cm]
			\emph{Encadrants :}\\
			\textsc{Cadiou} Anne\\
			\textsc{Raynal} Florence
		\end{flushright}
	\end{minipage}\\[2 cm]
	
    13 octobre 2020
\end{titlepage}

\tableofcontents
\listoffigures
\thispagestyle{empty}
\pagebreak
\setcounter{page}{1}

\section{Introduction}

Ce document fait suite au compte rendu qui nous a permis d'écrire les équations de Lorenz. Trois paramètres physique indépendant des inconnues ont été posés $\sigma$, $r$ et $b$. On va mener une étude mathématique des points d'équilibre du système en fonction des paramètres ainsi que leur nature. On pourra en déduire l'existence de bifurcations pour notre problème. On résolvera numériquement ces équation à l'aide d'un modèle mathématique dont on évaluera la précision pour des problèmes dont on connait la solution analytique, ainsi que pour les équations de Lorenz, dont on ne connait pas les solutions analytiques.

On rappelle le système de Lorenz :

\begin{equation}
	\label{eq:lorenz}
	\left\lbrace
	\begin{array}{l}
		\dfrac{\diff X}{\diff \tau} = -\sigma(X - Y)\\[.2cm]
		\dfrac{\diff Y}{\diff \tau} = X(r-Z) - Y\\[.2cm]
		\dfrac{\diff Z}{\diff \tau} = XY - bZ
	\end{array}
	\right.
\end{equation}
où $\sigma$ est le nombre de Prandtl, r est le nombre de Rayleigh modifié, b est la longueur d'onde.

Le précédent document nous a permis d'obtenir $b=8/3$, il s'agit de la plus petite longueur d'onde pour laquelle l'état initial au repos est instable.

$\sigma$ est donné par le fluide utilisé (Lorenz prend $\sigma = 10$ dans sa publication originale \cite{L63}), ce qui est environ la valeur de l'eau ($\sigma = 7$ \cite{baines_lorenz_2008}).

La stabilité du problème dépend donc avant tout de la valeur du Rayleigh modifié $r$.

\newpage

\section{Équilibres et stabilité}

On cherche les points d'équilibre du système, c'est-à-dire les triplets $U = (X,\,Y,\,Z)$ constant au cours du temps, ce qui revient au problème suivant :

\begin{equation}
	\label{eq:lorenzeq}
	\left\lbrace
	\begin{array}{l}
		X = Y\\
		(r-1)X=Z\\
		X^2 = bZ
	\end{array}
	\right.
\end{equation}

Sachant que $\sigma > 0$ et $b > 0$, on peut donc distinguer deux cas :

\begin{itemize}
	\item soit $\mathbf{r<1}$ et dans ce cas $X$ est réel si et seulement si $X$ est nul. Le seul équilibre du problème est donc $u = (0,\; 0,\; 0)$.
	\item soit $\mathbf{r\geq 1}$ et dans ce cas, il n'y a aucune contrainte sur les choix de $X$, $Y$ et $Z$. Il se dégage trois points d'équilibres :
	
	\begin{align*}
		&v_0=(0,\;0,\;0)\\
		&v_+=(\sqrt{b(r-1)},\;\sqrt{b(r-1)},\;r-1)\\
		&v_-=(-\sqrt{b(r-1)},\;-\sqrt{b(r-1)},\;r-1)
	\end{align*}
\end{itemize}

On va maintenant déterminer la stabilité des équilibres dans chacun des cas. Pour se faire, on calcule la matrice Jacobienne du système \ref{eq:lorenzeq}. Pour un équilibre $u$, on a $u$ stable si et seulement si la partie réelle des valeurs propres de la Jacobienne est strictement négative.

\[\mathit{J}_{(\ref{eq:lorenzeq})} = 
\begin{bmatrix}
	-\sigma & \sigma & 0\\
	r-Z & - 1 & X\\
	Y & X & -b
\end{bmatrix}\]

Pour la cas $\mathbf{r<1}$, on a un seul équilibre $u$. Le polynôme caractéristique de $\mathit{J}_{(\ref{eq:lorenzeq})}(u)$ est le suivant :

\[\chi(X) = (X+b)[X^2 + (\sigma+1)X +(1-r)\sigma]\]

En particulier, on remarque que l'on a un polynôme de degré 1 multiplier par un polynôme de degré 2 qui a tout ses coefficients positifs. On vérifie facilement que le polynôme de degré 2 a des racines à partie réelles strictement négatives. De même pour celui de degré 1. Ainsi $u$ est un équilibre stable.

\newpage

Pour le cas $\mathbf{r\geq 1}$, on a plusieurs équilibres. \\Le polynôme caractéristique de $\mathit{J}_{(\ref{eq:lorenzeq})}(v_0)$ est le même que le précédent. Cependant, $\chi(0) < 0$ et $\lim_{x\rightarrow\infty} \chi(x)= \infty$, ainsi, par le théorème des valeurs intermédiaires, on déduit l'existence d'une valeur propre à partie réelle strictement positive. $v_0$ n'est pas stable.\\
Les polynômes caractéristiques de $\mathit{J}_{(\ref{eq:lorenzeq})}(v_1)$ et $\mathit{J}_{(\ref{eq:lorenzeq})}(v_2)$ sont les mêmes :

\[\chi(X) = X^3 + (1+\sigma + b)X^2 + b(\sigma +r)X + 2b\sigma (r-1)\]

En utilisant le théorème des valeurs intermédiaires sur l'intervalle $]-\infty;\:0[$, on trouve une racine réelle strictement négative.

On suppose que la solution peut se mettre sous la forme $X_0\exp(i\omega t)$, ce qui nous permettra de trouver la valeur de $r$ critique pour laquelle les deux autres valeurs propres sont à partie réelle nulle. Pour cela, on pose $\omega \in \mathbb{R}$ et on impose :

\[\chi(i\omega) = 0\]

En annulant partie réelle et partie imaginaire, on impose deux égalités.

\begin{align*}
	\left\lbrace
	\begin{array}{l}
		\omega^2=b(\sigma + r)\\
		(1+\sigma + b)\omega^2 = 2b\sigma(r-1)
	\end{array}
	\right.
	\Rightarrow r(1+\sigma + b - 2 \sigma) = -2\sigma - \sigma - \sigma^2 - \sigma b
\end{align*}

Ainsi $r_{critique} = \sigma\dfrac{\sigma + b + 3}{\sigma - b - 1}$. Pour $r < r_{critique}$,  $v_1$ et $v_2$ sont des équilibres stables, sinon ils sont instables.

Avec $\sigma$ de l'ordre de 10 pour l'eau, on trouve $r_{critique} \approx 24.74$ (valeur proposée par Saltzmann en 1962 \cite{S62}). Lorenz a montré le comportement de la solution pour $r=28$. Il observe des équilibres instables, avec un comportement chaotique de la solution \cite{L63}.

\newpage

\section{Simulations numériques}

\subsection{Convergence de méthode et modèle de référence}

Nous allons désormais passer à une première étude numérique pour pouvoir vérifier les quelques résultats énoncés. Avant toutes choses, il est nécessaire de devoir écrire des méthodes d'intégration permettant de résoudre numériquement des équations différentielles du type :

\begin{equation}
    \label{eq:ex}
	\left\lbrace
	\begin{array}{l}
		U' = f(t, U(t))\\
		U(0) = a
	\end{array}
	\right.
\end{equation}
où $U$ est un vecteur.

On se propose d'écrire trois méthode d'intégration numérique : 

\begin{itemize}
	\item Euler explicite :
		\begin{equation*}
			\left\lbrace
			\begin{array}{l}
				U_{n+1} = U_n + \diff t f(t_n, U_n)\\
				U_0 = a
			\end{array}
			\right.
		\end{equation*}
	\item Runge Kutta 2 (ou Euler modifié) :
		\begin{equation*}
			\left\lbrace
			\begin{array}{l}
				U_{n+1} = U_n + \diff t f\left(t_n + \dfrac{\diff t}{2}, U_n + \dfrac{\diff t}{2}f(t_n, U_n)\right)\\
				U_0 = a
			\end{array}
			\right.
		\end{equation*}
	\item Runge Kutta 4:
		\begin{equation*}
			\left\lbrace
			\begin{array}{l}
				U_{n+1} = U_n + \dfrac{\diff t}{6}(k_1+2k_2+2k_3+k_4)\\[.2cm]
				k_1 = f(t_n, U_n)\\[.2cm]
				k_2 = f\left(t_n + \dfrac{\diff t}{2}, U_n + \dfrac{\diff t}{2}k_1\right)\\[.4cm]
				k_3 = f\left(t_n + \dfrac{\diff t}{2}, U_n + \dfrac{\diff t}{2}k_2\right)\\[.3cm]
				k_4 = f\left(t_{n+1}, U_n + \diff tk_3\right)\\[.2cm]
				U_0 = a
			\end{array}
			\right.
		\end{equation*}
\end{itemize} 

Les équations sont intégrées en temps sur l'intervalle [0, $t_f$]. En prenant un pas de temps régulier $\mathrm{d}t$, le nombre de pas de temps effectués est $N = \dfrac{t_f}{\mathrm{d}t}$.

Ces méthodes sont respectivement d'ordre 1, 2 et 4. On dit d'une méthode qu'elle est d'ordre $k\in \mathbb{N}^*$ si pour une solution $y$ de \ref{eq:ex}, alors

\[\underset{n\leq N}{\mathrm{max}} \left\Vert y(t_n) - y_n \right\Vert = \mathcal{O}\left(\mathrm{d}t^k\right)\]

Les trois méthodes présentées ont une complexité temporelle linéaire avec le nombre de points que l'on calcule. On comprend l'intérêt d'utiliser une méthode Runge Kutta 4 par rapport à une Runge Kutta 2. \cite{resode}\newpage

On implémente ces trois méthodes sous Python. La création de quelques objets nous permet de rassembler les paramètres d'un système sous la forme d'un vecteur et leur évolution temporelle est rassemblée dans un objet de trajectoire. Quelques opérations utiles ont été créées pour l'occasion, on peut les retrouver dans le script \texttt{my\_objects.py}. On introduit la norme d'un vecteur (donc d'un état) avec la norme deux usuelle :
\[\forall U = \begin{bmatrix} u_1 & \dots & u_k\end{bmatrix}^\top,\: \Vert U\Vert = \sqrt{\sum_{i=1}^k u_i^2}\] cette norme nous permettra de connaître la distance entre deux états (au même instant) quand on fera l'étude de convergence des schémas d'intégration temporelle.

Ces fonctions sont dans le script \texttt{integration.py}. On peut d'assurer de la complexité temporelle et de l'ordre des méthodes pour quelques systèmes d'équations différentielles connues via le script \texttt{complexity\_and\_error.py}.

On peut s'assurer rapidement que la complexité est en $\mathcal{O}(N)$ et que les ordres des trois méthodes sont respectivement 1, 2 et 4. En revanche ces ordres ont été déterminés pour des systèmes dont on connait les solutions analytiques (trois exemples de systèmes d'équations différentielles non-linéaires sont à la fin du script de \newline \texttt{complexity\_and\_error.py}). Le but va être de de vérifier la précision de la solution afin de pouvoir distinguer les erreurs numériques liées à l'intégration du schéma, et les perturbations amplifiées par le modèle physique.

Pour se faire, on admet qu'il existe un $N \in \mathbb{N}$ tel que l'erreur avec la vraie solution soit de l'ordre de $10^{-16}$ ce qui correspond au zéro machine d'un réel codé dans la précision de la norme IEEE 754. Pour toutes simulations avec $K \geq N$ valeurs calculées, on peut dire que la simulation est le modèle de référence du problème. L'étude de la convergence du modèle numérique se fera en calculant différentes simulation et en calculant l'erreur vis-à-vis d'une solution de référence. Par exemple pour la méthode Runge-Kutta 4, on trace ci dessous l'évolution de l'erreur en fonction du nombre de points calculés par rapport au modèle de référence.

\newpage

\begin{figure}[!h]
	\centering
	\begin{subfigure}{.48\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
			width=7.5cm,
			height=6cm,
			xmode=log,
			ymode=log,
			grid=both,
			grid style={line width=.2pt, draw=gray!10},
	    	xlabel = $N$,
	    	ylabel = $\left\Vert U_{N,\, REF} - U_N \right\Vert$]
			\addplot [color=black,line width=1, mark=*]
			table[x=N, y=err] {data/err5.txt};
			\end{axis}
		\end{tikzpicture}
		\caption{$t=3$}
	\end{subfigure} 
	\hfill
	\begin{subfigure}{.48\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
			width=7.5cm,
			height=6cm,
			xmode=log,
			ymode=log,
			grid=both,
			grid style={line width=.2pt, draw=gray!10},
	    	xlabel = $N$,
	    	ylabel = $\left\Vert U_{N,\, REF} - U_N \right\Vert$]
			\addplot [color=black,line width=1, mark=*]
			table[x=N, y=err] {data/err20.txt};
			\end{axis}
		\end{tikzpicture}
		\caption{$t=12$}
	\end{subfigure}\\
	\begin{subfigure}{.48\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
			width=7.5cm,
			height=6cm,
			xmode=log,
			ymode=log,
			grid=both,
			grid style={line width=.2pt, draw=gray!10},
	    	xlabel = $N$,
	    	ylabel = $\left\Vert U_{N,\, REF} - U_N \right\Vert$]
			\addplot [color=black,line width=1, mark=*]
			table[x=N, y=err] {data/err50.txt};
			\end{axis}
		\end{tikzpicture}
		\caption{$t=30$}
		\label{sfug:sat}
	\end{subfigure} 
	\hfill
	\begin{subfigure}{.48\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
			width=7.5cm,
			height=6cm,
			xmode=log,
			ymode=log,
			grid=both,
			grid style={line width=.2pt, draw=gray!10},
	    	xlabel = $N$,
	    	ylabel = $\left\Vert U_{N,\, REF} - U_N \right\Vert$]
			\addplot [color=black,line width=1, mark=*]
			table[x=N, y=err] {data/err100.txt};
			\end{axis}
		\end{tikzpicture}
		\caption{$t=60$}
	\end{subfigure}
	\caption{Évolution de l'erreur à différents $t$ en fonction de l'inverse du pas de temps}
	\label{fig:err}
\end{figure}

Le modèle de référence a été déterminé pour $r = 28$, $\sigma = 10$, $b = \frac{8}{3}$, ${N=100\,000\,001}$, $t_f=60$, $a=\begin{bmatrix}
	0 & 1 & 0
\end{bmatrix}^\top$. La condition initiale et le temps de simulation sont ceux que Lorenz avait choisi dans son article \cite{L63}. Seuls cent points sont conservés par simulation, ils correspondent aux instants 0.6, 1.2, ..., 60.

On remarque que l'erreur globale augmente avec le temps. On remarque aussi qu'il faut des pas de temps plus faible pour espérer arriver à la saturation de la convergence pour des temps de simulation plus longs. Pour le temps de simulation $t_f$, la méthode ne converge pas.

Si l'on se concentre sur la \textsc{Figure} \ref{sfug:sat}, trois phénomènes peuvent être interprétés. 

Pour $N$ inférieur à $20\,000$, on a un seuil puis un chute légère de l'erreur. Ce phénomène peut s'apparenter à de la saturation. Le pas de temps permet au système de ne pas diverger, en revanche il ne se comporte pas de la manière attendue. Ainsi l'erreur est limitée par la taille de l'attracteur lui-même.

Pour $N$ compris entre $20\,000$ et $500\,000$, on a une chute de l'erreur qui se fait en $N^{-4.08}$ (lecture graphique), ce qui correspond presque à la vitesse de convergence de la méthode numérique Runge-Kutta 4.

Pour $N$ supérieur à $500\,000$ on observe l'apparition d'un seuil. C'est une fois de plus de la saturation et elle témoigne d'un arrêt de convergence de la méthode. On peut se demander pourquoi la convergence ne se fait pas jusqu'au zéro machine. On remarque qu'une simulation faite pour $N = 5\,000\,001$ a été effectuée et que l'on observe une chute de l'erreur avant de la voir remonter, le problème vient probablement du fait que les paramètres $r$, $\sigma$ et $b$ ont été choisis tels que le système est chaotique (voir \ref{ssec:chaos}). 

On se propose d'illustrer cette apparition du chaos en traçant, pour un $N$ donné, l'évolution de l'erreur en fonction du temps où l'on regarde le système :

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\begin{axis}[
		width=12cm,
		height=8cm,
		ymode=log,
		grid=both,
		grid style={line width=.2pt, draw=gray!10},
    	xlabel = $t$,
    	ylabel = $\left\Vert U_{N,\, REF} - U_N \right\Vert$,
    	]
		\addplot [color=black, mark=*, line width=1]
		table[x=t, y=err] {data/errN100000.txt};
		\end{axis}
	\end{tikzpicture}
	\caption{Évolution de l'erreur en fonction de l'instant pour $N=100\,001$}
\end{figure}

On remarque que l'erreur initiale est inférieur à $10^{-8}$ jusqu'à l'instant $t=20$. À partir de cet instant, l'erreur devient croissance en $10^{0.26t}$ (lecture graphique) jusqu'à arriver au phénomène de saturation pour lequel le système ne converge pas, sans partir à l'infini.

Cette restriction en temps nous permet de voir que le système devient amplificateur seulement à partir d'un certain instant. La nature chaotique du système est peut être à l'origine de cette amplification d'erreur.

On remarque que pour des temps de simulation de l'ordre de 20, la simulation obtenue pour $N = 10^5$ est relativement proche de celle faite pour $N=10^8$.

\newpage

On décide d'étudier la convergence du modèle en fonction de $r$. On représente ci dessous l'évolution de l'erreur à $t=60$ en fonction de $r$, pour une simulation de $N = 100\,001$. Chaque modèle de référence a été établi pour une simulation où $N = 5\,000\,001$.

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\begin{axis}[
		width=12cm,
		height=7cm,
		ymode=log,
		grid=both,
		grid style={line width=.2pt, draw=gray!10},
    	xlabel = $r$,
    	ylabel = $\left\Vert U_{N,\, REF} - U_N \right\Vert$,
    	]
    	\addplot [mark=*,color=black, line width=1]
		table[x=r, y=err] {data/errRt60.txt};
		\end{axis}
	\end{tikzpicture}
	\caption{Évolution de l'erreur à $t_f$ en fonction de $r$}
\end{figure}

On constate que l'erreur augmente avec $r$ (cinq ordres de grandeur pour $r$ allant de 0.1 à 20) pour $\rho > \rho_{crit}$ l'erreur à l'instant $t_f$ augmente de douze ordres de grandeur. Ainsi le chaos est à l'origine de nos problèmes de convergence.\\[.2cm]

On se propose de se placer dans un cas non chaotique et de suivre l'évolution de l'erreur pour les trois méthodes d'intégrations temporelles introduites dans ce document, à savoir Euler explicite, Runge-Kutta 2 et Runge-Kutta 4. On choisit arbitrairement $\rho = 20$ et un modèle a été calculé par chacune de ces méthodes avec $N=100\,000\,001$.

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\begin{axis}[
		width=12cm,
		height=7cm,
		xmode=log,
		ymode=log,
		grid=both,
		grid style={line width=.2pt, draw=gray!10},
    	xlabel = $N$,
    	ylabel = $\left\Vert U_{N,\, REF} - U_N \right\Vert$,
    	]
    	\addplot [mark=*,color=black, line width=1]
		table[x=N, y=err] {data/errsnrk1.txt};
		\addplot [mark=*,color=black!50!red, line width=1]
		table[x=N, y=err] {data/errsnrk2.txt};
		\addplot [mark=*,color=red, line width=1]
		table[x=N, y=err] {data/errsnrk4.txt};
		\end{axis}
	\end{tikzpicture}
	\caption{Évolution de l'erreur à $t_f$ pour les trois modèles, cas non chaotique}
	\label{fig:err}
\end{figure}

\newpage

On conclut sur la convergence du modèle pour les méthodes proposées avec les vitesse de convergence que l'on est censé avoir. En revanche, cette convergence n'est possible que dans les cas non chaotique. Le graphe ci-dessus nous encourage à utiliser la méthode Runge-Kutta 4 pour $N$ valant au moins $10^5$.\\[.2cm]

On se propose de tracer l'attracteur de Lorenz, dans un cas chaotique, à savoir $\rho = 28$, avec $t_f=28$, $a = \begin{bmatrix}0&1&0\end{bmatrix}^\top$ et $N = 1\,000\,001$ (le graphe \ref{sfug:sat} nous confirme la convergence pour cette valeur).

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\begin{axis}[
		width=15cm,
		height=12cm,
    	xlabel = $X$,
    	ylabel = $Y$,
    	zlabel=$Z$]
		\addplot3[color=black,line width=1]
		table[x=x, y=y, z=z] {data/attractor.txt};
		\addplot3[scatter, draw=red]
		({0}, {1}, {0});
		\end{axis}
	\end{tikzpicture}
	\caption{Diagramme de phase du modèle de référence}
\end{figure}

\newpage

Pour une meilleure lisibilité et puisqu'ils n'ont pas la même signification physique, on se propose de tracer indépendamment $X$, $Y$ et $Z$ en fonction de $t$.


\begin{figure}[!h]
	\hspace*{-2cm}
	\begin{subfigure}{.48\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
			xmin=0, xmax=20,
			ymin=-25, ymax=25,
			width=8cm,
			height=7cm,
			grid=both,
			grid style={line width=.2pt, draw=gray!10},
	    	xlabel = $t$,
	    	ylabel = $X$]
			\addplot [color=black,line width=1]
			table[x=t, y=x] {data/attractor.txt};
			\end{axis}
		\end{tikzpicture}
		\caption{$X$ selon $t$}
		\label{fig:refx}
	\end{subfigure}
	\hfill
	\begin{subfigure}{.48\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
			xmin=0, xmax=20,
			ymin=-25,
			width=8cm,
			height=7cm,
			grid=both,
			grid style={line width=.2pt, draw=gray!10},
	    	xlabel = $t$,
	    	ylabel = $Y$]
			\addplot [color=black,line width=1]
			table[x=t, y=y] {data/attractor.txt};
			\end{axis}
		\end{tikzpicture}
		\caption{$Y$ selon $t$}
		\label{fig:refy}
	\end{subfigure}\\
	\centering
	\begin{subfigure}{.95\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
			xmin=0, xmax=20,
			width=12cm,
			height=7cm,
			grid=both,
			grid style={line width=.2pt, draw=gray!10},
	    	xlabel = $t$,
	    	ylabel = $Z$]
			\addplot [color=black,line width=1]
			table[x=t, y=z] {data/attractor.txt};
			\end{axis}
		\end{tikzpicture}
		\caption{$Z$ selon $t$}
		\label{fig:refz}
	\end{subfigure}
	\caption{Évolution des coordonnées du système au cours du temps}
\end{figure}

Avec cette représentation, on peut par exemple savoir combien de temps on orbite autour d'un équilibre avant d'orbiter vers l'autre grâce à la \textsc{Figure} \ref{fig:refx} ou la \textsc{Figure} \ref{fig:refy}. Ce nombre semble relativement chaotique : parfois un, deux, trois, \dots \\Si l'on observe sur une plus longue durée, aucun schéma ne semble apparaître.

\newpage

\subsection{Étude numérique des équilibres}

L'étude des positions d'équilibre et de leur nature se fera par étude graphique. Il sera plus raisonnable de tracer les composantes $X$, $Y$ et $Z$ en fonction de $t$ plutôt que le diagramme de phases.

Pour $r <1$, on s'assure que seul $0$ est un équilibre et qu'il est stable. Voici un exemple pour $r = 0.1$ (valeur choisie lors de notre étude sur l'influence de $r$) et $a=\begin{bmatrix}
	0 & 1 & 0
\end{bmatrix}^\top$.

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\begin{axis}[
		width=12cm,
		height=8cm,
		xmin=0, xmax=10,
    	xlabel = $t$,
    	legend entries={$X$, $Y$, $Z$},]
		\addplot[color=black,line width=1]
		table[x=t, y=x] {data/stab0.txt};
		\addplot[dashed,color=black,line width=1]
		table[x=t, y=y] {data/stab0.txt};
		\addplot[densely dotted,color=black,line width=1]
		table[x=t, y=z] {data/stab0.txt};
		\end{axis}
	\end{tikzpicture}
	\caption{$r=0.1$ étude des équilibres}
\end{figure}

On observe la convergence de notre condition initiale vers 0.

\newpage

Pour $1<r<r_{critique}$, on s'assure d'avoir trois points d'équilibre, deux sont stables mais pas le troisième. Voici quelques exemples avec $r = 20$ (valeur deux fois choisie précédemment) et $a_1=\begin{bmatrix}
	0 & 1 & 0
\end{bmatrix}^\top$, $a_2=\begin{bmatrix}
	0 & -1 & 0
\end{bmatrix}^\top$, $a_3=\begin{bmatrix}
	0 & 10^{-12} & 0
\end{bmatrix}^\top$, $a_4=\begin{bmatrix}
	0 & -10^{-12} & 0
\end{bmatrix}^\top$. Ces conditions initiales représentent respectivement le choix de Lorenz dans sa publication \cite{L63}, son opposé dans la direction $X$ et $Y$, une perturbation à l'origine de l'ordre de $10^{-12}$, valeur représentant le seuil de convergence pour $r=20$ (\ref{fig:err}) et son opposé dans les directions $X$ et $Y$.

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\begin{axis}[
		width=12cm,
		height=6.5cm,
		xmin=0, xmax=20,
    	xlabel = $t$,
    	legend entries={$Y_1$, $Y_2$, $Y_3$, $Y_4$},]
		\addplot[color=black,line width=1]
		table[x=t, y=y] {data/stab-.txt};
		\addplot[color=red,line width=1]
		table[x=t, y=y] {data/stab+.txt};
		\addplot[dashed, color=black,line width=1]
		table[x=t, y=y] {data/instab-.txt};
		\addplot[dashed, color=red,line width=1]
		table[x=t, y=y] {data/instab+.txt};
		\end{axis}
	\end{tikzpicture}
	\caption{$r=20$ étude des équilibres}
\end{figure}

Enfin, pour le cas $r_{critique}<r$, tous les points d'équilibres sont instables. On le vérifie avec $r=28$ de la même manière que précédemment. On se propose cette fois ci de tracer l'instant pour lequel, l'écart relatif entre l'équilibre et l'état instable est supérieur à 1\% en fonction de la perturbation $\varepsilon$. On effectuera cette perturbation selon les directions $X$, $Y$ ou $Z$. On considère à chaque fois les trois directions principales en guise de condition initiale à savoir : $\begin{bmatrix}
	1 & 0 & 0
\end{bmatrix}^\top$, $\begin{bmatrix}
	0 & 1 & 0
\end{bmatrix}^\top$ et $\begin{bmatrix}
	0 & 0 & 1
\end{bmatrix}^\top$. On peut noter pour la condition initiale de direction $Z$ et perturbée selon $Z$, $X$ et $Y$ deviennent les fonctions nulles et $Z$ converge. On n'étudiera donc pas ce cas.

\newpage

\begin{figure}[!h]
	\centering
	\begin{subfigure}{.8\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
			width=12cm,
			height=6cm,
	    	xlabel = $\varepsilon$,
	    	ylabel = Temps d'écart relatif à 1\%,
	    	xmode=log,
	    	legend entries={$X$, $Y$, $Z$},]
			\addplot[color=black,line width=1]
			table[x=eps, y=x] {data/errgrowthX.txt};
			\addplot[dashed, color=black,line width=1]
			table[x=eps, y=y] {data/errgrowthX.txt};
			\addplot[dotted, color=black,line width=1]
			table[x=eps, y=z] {data/errgrowthX.txt};
			\end{axis}
		\end{tikzpicture}
		\caption{Condition initiale $\begin{bmatrix}1 & 0 & 0\end{bmatrix}^\top$}
		\label{sfig:growthx}
	\end{subfigure}
	\\
	\begin{subfigure}{.8\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
			width=12cm,
			height=6cm,
	    	xlabel = $\varepsilon$,
	    	ylabel = Temps d'écart relatif à 1\%,
	    	xmode=log,
	    	legend entries={$X$, $Y$, $Z$},]
			\addplot[color=black,line width=1]
			table[x=eps, y=x] {data/errgrowthY.txt};
			\addplot[dashed, color=black,line width=1]
			table[x=eps, y=y] {data/errgrowthY.txt};
			\addplot[dotted, color=black,line width=1]
			table[x=eps, y=z] {data/errgrowthY.txt};
			\end{axis}
		\end{tikzpicture}
		\caption{Condition initiale $\begin{bmatrix}0 & 1 & 0\end{bmatrix}^\top$}
		\label{sfig:growthy}
	\end{subfigure}\\
	\begin{subfigure}{.8\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
			width=12cm,
			height=6cm,
	    	xlabel = $\varepsilon$,
	    	ylabel = Temps d'écart relatif à 1\%,
	    	xmode=log,
	    	legend entries={$X$, $Y$, $Z$},]
			\addplot[color=black,line width=1]
			table[x=eps, y=x] {data/errgrowthZ.txt};
			\addplot[dashed, color=black,line width=1]
			table[x=eps, y=y] {data/errgrowthZ.txt};
			\end{axis}
		\end{tikzpicture}
		\caption{Condition initiale $\begin{bmatrix}0 & 0 & 1\end{bmatrix}^\top$}
		\label{sfig:growthz}
	\end{subfigure}
	\caption{Temps de croissance de l'instabilité en fonction de la perturbation initiale}
\end{figure}

Les graphes en \textsc{Figure} \ref{sfig:growthx} et \ref{sfig:growthy} nous permet de déduire une croissance de la perturbation de manière exponentielle en $\exp^{0.9t}$ (lecture graphique). En ce qui concerne le graphe en \textsc{Figure} \ref{sfig:growthz}, la croissance de l'erreur est en $\exp^{14.5t}$.

\newpage

Ces coefficients ne sont pas anodins et sont appelés les coefficients de Lyapunov. On peut les définir de la manière suivante :\\[.2cm]

On pose pour tout $t$, $U(t)\in\mathbb{R}^d$ et $U_0(t)\in\mathbb{R}^d$, de manière à ce que $U$ et $U_0$ décrivent des trajectoires dans un espace de phases de dimension $d$. On note pour tout $t$, $\delta U(t) = U(t) - U_0(t)$ et $\delta U_0 = U(0) - U_0(0)$. Si pour $t$ suffisamment grand :

\[\vert\delta U(t)\vert \approx \vert \exp(\lambda t)\delta U_0\vert\]
alors $\lambda$ est ce que l'on appelle un coefficient de Lyapunov. Cette étude ne permet d'extraire qu'un seul coefficient (celui avec la partie réelle la plus grande).\\[.2cm]

Pour un système tel que le notre, c'est à dire de la forme $\dot{U} = F(U)$, on peut extraire tous les coefficients de Lyapunov à l'aide de la matrice Jacobienne de F. Pour ce faire, il suffit de trouver les valeurs propres de la matrice :

\[\Lambda = \int_0^t \mathit{J}_F(U_0(s))\mathrm{d}s\]
où $\mathit{J}_F$ désigne la matrice Jacobienne de $F$.

Pour s'en convaincre il suffit d'écrire :

\[\left.
\begin{array}{r}
\displaystyle U(t) =  U(0) + \int_0^t F(U(s))\mathrm{d}s\\
	\displaystyle U_0(t) =  U_0(0) + \int_0^t F(U_0(s))\mathrm{d}s
\end{array}
	\right\rbrace	\displaystyle \Rightarrow \delta U(t) \approx \delta U_0 + \int_0^t \mathit{J}_F(U_0(s))\mathrm{d}s
\]

Il suffit ensuite de remarquer que l'en posant $M$ comme étant l'intégrale, on a :

\[\delta U(t) = \exp(M)\delta U_0\]
en notant $\alpha_i$ ses valeurs propres, les coefficients de Lyapunov sont $\lambda_i = \lim_{t\rightarrow\infty} \alpha_i / t$\cite{Lyap}.

\newpage

On représente l'évolution des coefficients de Lyapunov en fonction du temps pour la condition initiale suivante : $\begin{bmatrix}0 & 1 & 0\end{bmatrix}^\top$. Un moyen de mesurer la fiabilité de la méthode est de s'assurer que la somme des coefficients donne la trace de la Jacobienne (pour le cas $r=28$, $\sigma=10$ et $b=8/3$, on doit trouver -13,667).

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\begin{axis}[
		width=12cm,
		height=7cm,
    	xlabel = $t$,
    	legend entries={$\lambda_1$, $\lambda_2$, $\lambda_3$},]
		\addplot[color=black,line width=1]
		table[x=t, y=l1] {data/lyapunov.txt};
		\addplot[color=red,line width=1]
		table[x=t, y=l2] {data/lyapunov.txt};
		\addplot[color=blue,line width=1]
		table[x=t, y=l3] {data/lyapunov.txt};
		\end{axis}
	\end{tikzpicture}
	\caption{Coefficients de Lyapunov cas $\begin{bmatrix}0 & 1 & 0\end{bmatrix}^\top$}
\end{figure}

On observe une convergence des trois coefficients et celui de plus grande valeur vaut environ 0.9 ce qui correspond à la valeur attendue. On remarque que la somme des coefficients respecte aussi la valeur attendue à l'ordre de $10^{-10}$ près. Ces trois valeurs se retrouvent souvent dans la littérature \cite{LyapVal}\\[.2cm]

On fait de même avec la condition initiale $\begin{bmatrix}0 & 0 & 1\end{bmatrix}^\top$ qui converge vers 0 et amplifie plus les instabilités.

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\begin{axis}[
		width=12cm,
		height=7cm,
    	xlabel = $t$,
    	legend entries={$\lambda_1$, $\lambda_2$, $\lambda_3$},]
		\addplot[color=black,line width=1]
		table[x=t, y=l1] {data/lyapunov1.txt};
		\addplot[color=red,line width=1]
		table[x=t, y=l2] {data/lyapunov1.txt};
		\addplot[color=blue,line width=1]
		table[x=t, y=l3] {data/lyapunov1.txt};
		\end{axis}
	\end{tikzpicture}
	\caption{Coefficients de Lyapunov cas $\begin{bmatrix}0 & 0 & 1\end{bmatrix}^\top$}
\end{figure}

On constate que l'on trouve une valeur supérieur à celle attendue, en revanche on a une amplification plus rapide que pour le cas précédent. On vérifie toujours que la somme des coefficients vaut environ -13,667.

\newpage

\subsection{Chaos}
\label{ssec:chaos}

Nous décidons de réaliser une dernière simulation numérique nous permettant de témoigner du caractère chaotique de l'attracteur de Lorenz. Pour se faire, nous prenons deux conditions initiales sensiblement proches et on trace la différence entre chaque composante des deux trajectoires obtenues.

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\begin{axis}[
		width=12cm,
		height=8cm,
    	xlabel = $t$,
    	legend entries={$X$, $Y$, $Z$},]
		\addplot[color=black,line width=1]
		table[x=t, y=x] {data/chaos.txt};
		\addplot[color=red,line width=1]
		table[x=t, y=y] {data/chaos.txt};
		\addplot[color=blue,line width=1]
		table[x=t, y=z] {data/chaos.txt};
		\end{axis}
	\end{tikzpicture}
	\caption{Comportement chaotique}
\end{figure}

Les deux conditions initiales sont : $a_1=\begin{bmatrix}
	0 & 1 & 0
\end{bmatrix}^\top$ et $a_2=\begin{bmatrix}
	0 & 1 + 10^{-6} & 1
\end{bmatrix}^\top$. La condition initiale choisie est celle introduite par Lorenz, on a vu que le choix de la composante perturbée n'avait que peu d'impact sur la croissance de l'erreur. Ce comportement montre que le système est extrêmement sensible aux conditions initiales. En revanche on constate que la déviation significative a lieu environ dix unités de temps après le début de la simulation ce qui permettrait de faire un correction en temps réel, notamment si l'on veut prédire le comportement d'un système instable dans le futur proche. 

Les phénomènes de saturation obtenus en \textsc{Figure} \ref{fig:err} pourrait être expliqués à partir du comportement chaotique. En effet la moindre erreur de codage des flottants entrainera des erreurs significatives à partir d'un certain moment. Le système se comporte comme un amplificateur d'erreurs. Si les erreurs numériques sont trop grandes, alors la solution calculée n'est pas correcte.

\newpage

\section{Conclusion}

Ce document nous a apporté des éléments essentiels de l'étude de la stabilité et du comportement chaotique de l'attracteur de Lorenz pour certains jeu de paramètres. Le coefficient $r$ décide de la stabilité du système. 

Nous avons aussi implémenté plusieurs méthodes d'intégrations numériques afin d'effectuer nos simulation. Même si ces algorithmes ont des performances connues face à des problèmes dont on connait la solution analytique, certains problèmes se posent sur un problème chaotique tel que le système de Lorenz puisque la sensibilité aux conditions initiales est très importante.

Dans la suite du projet, nous allons tenter d'anticiper le comportement chaotique d'un système en ne connaissant que certains paramètres à des instants discrets avec une erreur de mesure. Avant de pourvoir utiliser une méthode de 4D-VAR, il nous faut pouvoir simuler numériquement des erreurs de mesures.

\nocite{*}
\bibliographystyle{plain}
\bibliography{bib}

\end{document}
