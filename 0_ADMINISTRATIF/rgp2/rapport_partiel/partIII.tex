\subsection{Variable aléatoire de densité quelconque}

L'un des buts du projet est de connaître l'influence de la prédiction par assimilation d'un système non-linéaire. Il faut donc disposer de diverses variables aléatoires dont la loi de densité peut être spécifiée. Cette génération de variables aléatoires sera implémentée en Python, nous devons donc supposer que l'on ne dispose uniquement de la variable aléatoire de distribution uniforme sur [0, 1[ via la fonction \texttt{random.random}.\\[.2cm]

\emph{Note : l'aléatoire machine n'est pas un aléatoire scientifique, mais un pseudo-aléatoire. Cela signifie que l'aléatoire est reproductible en partant d'une graine (seed).}

\subsubsection{Exemple - Loi normale}

Il existe une méthode pour transformer deux lois uniformes sur le segment [0, 1] en deux lois normales centrées réduites. Il s'agit de la méthode Box-Muller \cite{M58}.\\[.2cm]

La méthode consiste à prendre deux variables aléatoires indépendantes de loi uniforme sur le segment [0, 1] notées $U_1$ et $U_2$. On pose ensuite deux variables aléatoire telles que :

\[X_1 = \sqrt{-2\ln(U_1)}\cos(2\pi U_2)\]
\[X_2 = \sqrt{-2\ln(U_1)}\sin(2\pi U_2)\]

Ces deux variables suivent des lois normales centrées et réduites et sont indépendantes.\\[.2cm]

Bien que cette méthode soit rapide à mettre en place, elle ne permet seulement d'engendrer des lois normales, or nous voulons nous étendre à d'autres lois. Il faut une méthode plus générale.

\newpage

\subsubsection{Principe de la méthode générale}

Soit $g$ une fonction positive définie sur l'intervalle [$a$, $b$] d'intégrale 1 sur cet intervalle. On souhaite construire une variable aléatoire de densité de probabilité $g$, c'est à dire,avoir $X$ telle que :

\[\forall A\subset[a,\,b],\: \mathbb{P}(X\in A) = \int_A g(x) \mathrm{d}x\]

On note $F$ la fonction de répartition de la variable aléatoire, ce qui signifie que l'on a :

\[F(t) = \mathbb{P}(X\leq t) = \int_{a}^t g(x)\mathrm{d}x\]

Si $g$ est strictement positive presque partout sur [$a$, $b$], alors $F$ est strictement croissante presque partout sur [$a$, $b$] et on a l'existence de la réciproque de $F$ presque partout sur [0, 1].\\[.2cm]

On cherche à lier une variable aléatoire uniforme $U$ à la variable aléatoire que l'on cherche à obtenir $X$. On note $f$ l'application reliant ces deux objets : $U = f(X)$. La probabilité de trouver la variable aléatoire $U$ entre $u$ et $u + \mathrm{d}u$ est la même que celle de trouver la variable aléatoire $X$ entre $x$ et $x+\mathrm{d}x$, ainsi :

\[g(x)\mathrm{d}x = \underbrace{1}_{\substack{\text{densité de probabilité}\\\text{d'une loi uniforme sur [0, 1]}}}\times f'(x)\mathrm{d}x\]

Ainsi, $f =F$ et on a donc $f$ bijective presque partout sur [$a$, $b$] et :

\[X = F^{-1}(U)\]\\[.3cm]

Ainsi, pour créer une variable aléatoire de densité quelconque $g$ sur [$a$, $b$], il suffit de calculer sa primitive nulle en $a$ et de considérer sa fonction réciproque. Il suffit ensuite d'injecter un bruit blanc dans cette fonction pour avoir une issue.

\newpage

\subsubsection{Implémentation algorithmique}

On suppose dans un cas général, que la fonction $g$ n'admet pas de primitive que l'on peut facilement exprimer. Ainsi la primitive de $g$ n'est autre qu'une fonction qui à $x$ va associer une approximation de l'intégrale de $g$ entre $a$ et $x$. On pose $N$ le nombre de subdivisions de l'intervalle d'intégration [$a$, $b$].\\[.2cm]

Intégration par la méthode de Simpson :

\begin{algorithm}
	\caption{Méthode de Simpson}
	\algsetup{indent=3em}
	\begin{algorithmic}
		\REQUIRE $g$, $a$, $b$, $N$
		\ENSURE $\displaystyle x\mapsto\int_a^x g(t)\mathrm{d}t + \mathcal{O}\left(N^{-4}\right)$
		\STATE $F \leftarrow [0]$
		\STATE $h \leftarrow (b-a)/N$
		\FOR{$i=1..N-1$}
		\STATE $temp \leftarrow$ dernière valeur de $F$
		\STATE $temp \leftarrow temp + \dfrac{h}{6}\left[g(a + ih) + g\left(a+\left(i+1/2\right)h\right) + g(a+(i+1)h)\right]$\\[.1cm]
		\STATE Ajouter à $F$ la valeur $temp$
		\ENDFOR
		\RETURN $x \mapsto \lfloor (x-a) / h \rfloor^\text{ième}$ valeur de $F$ 
	\end{algorithmic}
\end{algorithm}

La fonction bijective de $F$ se fait par une recherche dichotomique dans un tableau (puisque $F$ est un tableau déguisé en fonction).

\begin{algorithm}
	\caption{Fonction réciproque (version tableau)}
	\algsetup{indent=3em}
	\begin{algorithmic}
		\REQUIRE $F$, $x$, $a$, $b$, $N$
		\ENSURE $y \, \left\vert \: F(x) = y + \mathcal{O}\left(N^{-1}\right)\right.$
		\STATE $h \leftarrow (b-a)/2$
		\WHILE{$b-a > \varepsilon$}
		\IF{$F(a+h) < x$}
		\STATE $a\leftarrow a+h$
		\STATE $h \leftarrow h/2$
		\ELSE
		\STATE $b\leftarrow b-h$
		\STATE $h \leftarrow h/2$
		\ENDIF
		\ENDWHILE
		\RETURN $a$
	\end{algorithmic}
\end{algorithm}

\newpage

Ainsi, nous sommes en mesure de créer une variable aléatoire de densité de probabilité quelconque. L'obtention de la variable aléatoire se fait en $\mathcal{O}(n)$, la réalisation d'un évènement quant à lui est en $\mathcal{O}(\log_2(n))$. L'objet occupe un espace mémoire en $\mathcal{O}(n)$.\\[.2cm]

On propose de créer une variable aléatoire dont la densité de probabilité est $g(x) = \frac{1}{\sqrt{2\pi}}\exp\left(-\frac{x^2}{2}\right)$ entre -10 et 10 soit une loi gaussienne tronquée d'écart-type 1 et de moyenne nulle. On simule différents nombres de réalisations afin de comparer les principaux paramètres de distribution à savoir la moyenne, la variance, l'asymétrie et l'aplatissement (voir \ref{sec:def_params}) dont les valeurs convergent respectivement vers 0, 1, 0 et 3 pour l'intervalle des réels. Le domaine choisi nous permet de négliger les contributions au delà du domaine car $\int_{\mathbb{R}\backslash[-10, 10]} g(x) \mathrm{d}x \approx 10^{-23}$.

\input{tikz/III/params_VA}

\newpage

On remarque que, globalement, les paramètres convergent vers leur vraie valeur en $\sqrt{N}$ où $N$ est le nombre de réalisations, ce qui est un résultat classique pour la moyenne. On se propose de superposer la distribution pour 10 000 000 de réalisations et la fonction probabilité de densité sur un même graphe.

\input{tikz/III/VA}

L'allure de la densité de probabilité souhaitée apparait, la méthode choisie semble donc fonctionnelle.\\[.2cm]

Nous disposons de plusieurs moyens pour agir perturber le système. Nous pouvons perturber la condition initiale, perturber les équation du modèle (faire du forçage) et perturber les mesures.

\newpage

\subsection{Influence des perturbations sur l'attracteur}
\label{sec:def_params}

Dans cette partie, on va étudier l'influences de perturbations sur différents systèmes physiques afin de connaître l'influence de la nature d'un système (linéaire/non-linéaire et chaotique/non-chaotique) sur le bruit. Afin d'avoir des erreurs de mesures les plus faibles, on réalisera plusieurs essais. On note $N$ le nombre de réalisation. On note ${U_i}'$ l'écart de la solution non perturbé à la solution perturbée ($i^{\text{ème}}$ réalisation). Afin de pouvoir interpréter les résultats, on va suivre les quelques caractéristiques suivantes à savoir :

\begin{itemize}
	\item le vecteur moyen de la distribution translaté du vecteur non perturbé
	\[\overline{U}(t) = \frac{1}{N}\sum_{i=1}^N {U_i}'(t)\]
	\item le vecteur variance de la distribution
	\[\mathbb{V}(U(t)) = \frac{1}{N}\sum_{i=1}^N ({U_i}' - \overline{U})^2(t)\]
	\item le vecteur asymétrie de la distribution
	\[S(U(t)) = \frac{1}{N\mathbb{V}(U(t))^{3/2}}\sum_{i=1}^N ({U_i}' - \overline{U})^3(t)\]
	\item le vecteur d'aplatissement de la distribution
	\[T(U(t)) = \frac{1}{N\mathbb{V}(U(t))^{2}}\sum_{i=1}^N ({U_i}' - \overline{U})^4(t)\]
\end{itemize}

Dans un premier temps, on va venir perturber la condition initiale et dans un second temps, on va faire du forçage aléatoire.

\newpage
\subsubsection{Perturber les conditions initiales}
\label{sssec:pert_ci}

On va appliquer des perturbations de lois données à la condition initiale. On étudie nos caractéristiques sur : 

\begin{itemize}
	\item un système linéaire : un pendule simple dans la limite des petits mouvements et en négligeant les frottements, dont l'équation d'évolution est :
	\[\ddot{\theta} + {\omega_0}^2\theta = 0 \hspace*{.2cm} \text{avec} \hspace{.2cm} \omega_0 = \sqrt{\dfrac{g}{l}}\]
	\item un système non-linéaire non chaotique : pendule rigide dans la zone de forte non linéarité avec frottements, dont l'équation d'évolution est :
	 \[\ddot{\theta} + \frac{\alpha}{m}\dot{\theta} + \frac{g}{l}\sin(\theta) = 0\]
	 \item un système non linéaire chaotique : le système de Lorenz avec les paramètres $r=28$, $\sigma=10$ et $b=8/3$, de condition initiale $\begin{bmatrix}0 & 1 & 0\end{bmatrix}^\top$
\end{itemize}
	 
Dans un premier temps, on ne perturbe qu'avec un bruit gaussien de moyenne nulle dont la variance est un paramètre de libre choix. Dans tous les cas suivants, il a été choisi de prendre 10 000 réalisations (compromis entre précision et temps de calcul). \emph{(les résultats présentés jusqu'à la fin de la partie sont en cours d'interprétation)}

\newpage

Pour le système linéaire, on perturbe la position et on fixe arbitrairement la variance à 0.1 (le système est linéaire, changer l'amplitude d'entrée changera l'amplitude de sortie de façon linéaire).

\input{tikz/III/pertCI_lin}

On constate que tous les paramètres considérés semblent être de l'ordre de grandeur d'une gaussienne de variance 0.1 pour $X$ et de 1 pour $\dot{X}$.Le caractère gaussien de la perturbation initiale est conservée et transmise pour les autres composantes du vecteur. Ce résultat était prévisible puisque le système est linéaire.

\newpage

Pour le système non-linéaire, on perturbe la position et on fixe arbitrairement la variance à 0.1.

\input{tikz/III/pertCI_non_lin}

On observe que la distribution de nos deux paramètres physiques ont des caractéristiques de variance, d'asymétrie et d'aplatissement qui oscillent légèrement autour d'une valeur fixe. Seule la moyenne est oscillante. On peut donc supposer que la loi de répartition de la perturbation tend vers une loi dont seule la moyenne oscille de manière périodique.

\newpage

Nous voulons savoir si ces constantes sont des fonction de la variance initialement choisie. On se propose donc de refaire un simulation avec une variance de 0.01.

\input{tikz/III/pertCI_non_lin2}

On constate que l'on converge exactement vers le même jeu de paramètres pour la variance, l'asymétrie et l'aplatissement. En ce qui concerne la moyenne, la période d'oscillation est la même et les amplitudes sont les mêmes. Pour un même système, ces grandeurs caractéristiques doivent être fonction de la condition initiale.

\newpage

On propose donc de garder le même système mais de choisir comme condition initiale $\theta_0 = 3$rad. La variance initiale est choisie à 0.01.

\input{tikz/III/pertCI_non_lin3}

Nos distributions ont des grandeurs caractéristiques qui ne sont plus les mêmes, mais restent du même ordre de grandeur.

\newpage

Pour le système de Lorenz, on perturbe la composante $X$ et on fixe arbitrairement la variance à 10$^{-4}$.

\input{tikz/III/pertCI_non_lin_cha}

La moyenne de la distribution semble relativement chaotique mais on observe que les temps d'oscillations correspondent au temps d'oscillations autour des deux positions d'équilibres non nulles. Pour ce qui concerne les autres paramètres, il y a trois phases : phénomène de croissance des perturbations, un régime transitoire (car le système amplifie les perturbations et son comportement est modifié) et le régime chaotique.\\[.2cm]

\newpage

Pour interpréter les instants où la convergence est atteinte, on peut supposer que la distribution a atteint une forme qui ne changera plus par la suite et que celle ci va se translatée au cours du temps pour les trois composantes.

À la vue des valeurs de convergence des paramètres de distribution (à savoir 0 pour l'asymétrie de $X$ et de $Y$, 0.2 pour l'asymétrie de $Z$ et entre 2.1 et 2.8 pour l'aplatissement) on peut imaginer que l'on se retrouve avec une distribution quasi-gaussienne de variance fixe et qui glisse au cours du temps. On décide donc de tracer ces distributions une fois le régime permanent atteint ($t=100$).

\input{tikz/III/dist}

Les distributions obtenues ne pas pas gaussiennes. Pour celles selon $X$ et $Y$ on observe deux excroissances plus ou moins importantes de part et d'autre de la valeur moyenne, ce qui aurait pu être anticipé à la vue du caractère symétrique de $X$ et $Y$ dans le système. En ce qui concerne la distribution dans la direction $Z$, la distribution n'est pas symétrique (comme on a pu le voir plus tôt).\\[.2cm]

Si on trace les distributions pour d'autres instants, postérieurs à $t=60$, on obtient des résultats similaires, où seule la moyenne change.

\newpage

Si on perturbe à présent la condition initiale selon les autres ($Y$ ou $Z$) avec un bruit gaussien de moyenne nulle et de variance 0.0001 (la même que celle étudiée avant), on retrouve les mêmes résultats que ce soit en ce qui concerne le comportement (trois régimes qui durent environ les mêmes durées), les valeurs de convergence des principaux paramètres de distribution en dehors de la moyenne et l'allure des distributions.

\input{tikz/III/pertCI_non_lin_cha_y}

\input{tikz/III/pertCI_non_lin_cha_z}

\newpage

\subsubsection{Forçage du système}

Dans cette sous-section, on suppose que le système ne se comporte pas exactement comme le jeu d'équation que l'on possède mais que le système a pour loi d'évolution la suivante :

\[U'(t) = F(t, U(t)) + w(t)\]
où $w$ est un vecteur représentant du bruit de loi donnée et qui varie au cours du temps.