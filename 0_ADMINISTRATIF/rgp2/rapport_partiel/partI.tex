\subsection{Le problème posé}

L'attracteur de Lorenz tel qu'il a été proposé en 1963 \cite{L63} est un système dynamique constitué de trois équations différentielles non linéaire, établies à partir d'un modèle \emph{très} simplifié de la convection thermique dans l'atmosphère. Le modèle est lui-même une simplification du modèle initialement proposé par Saltzman en 1962 \cite{S62} pour étudier la convection d'amplitude finie. Il traite du problème d'instabilité convective décrit par Rayleigh en 1916 \cite{R16}. Nous allons ici retrouver l'établissement des équations de Lorenz afin d'en connaître la signification physique et d'exposer les plages de validité de ses paramètres. Ce document s'inspire du coure de M. Faure \cite{coursfaure} et des papiers originaux.

\subsection{Le modèle de Rayleigh}

Le système de coordonnées utilisé est le cartésien. On considère le problème invariant selon $y$.\\

Soit un fluide dans une couche infinie d'épaisseur $h$. On impose une température $T(z=0)=T_0$ et $T(z=h)=T_0-\delta T$. Nous allons étudier le champ de vitesse $\overrightarrow{v}(x,z, t)$ ainsi que le champ de température $T(x, z, t)$.

\input{tikz/I/convec}

\newpage

\subsubsection{Hypothèses}

Le fluide considéré de viscosité $\mu$ est supposé incompressible.

\renewcommand{\theequation}{H\arabic{equation}}

\begin{equation}
	\label{eq:incomp}
	\overrightarrow{\nabla}\cdot \overrightarrow{v} = 0
\end{equation}

La masse volumique $\rho$ est une fonction linéaire de la température.

\begin{equation}
	\label{eq:rline}
	\exists \alpha,\,\beta\:\vert\: \rho = \alpha T + \beta
\end{equation}

On néglige la dépendance de $\rho$ vis-à-vis de la température pour le terme $\rho\frac{\Diff \overrightarrow{v}}{\Diff t}$. C'est l'approximation de Boussinesq.

\begin{equation}
	\label{eq:bouss}
	\rho\frac{\Diff \overrightarrow{v}}{\Diff t} = \rho_0\frac{\Diff \overrightarrow{v}}{\Diff t}
\end{equation}

On suppose qu'au repos, c'est-à-dire pour $\overrightarrow{v} = 0$, on a :

\begin{equation}
	\label{eq:tline}
	T_r(z) = T_0 - \dfrac{z}{h}\delta T
\end{equation}
On peut remarquer que cette forme vérifie les conditions limites imposées sur la température.

Pour les champs vectoriels $\overrightarrow{v}(x, z, t)$ tels que $\overrightarrow{\nabla}\cdot\overrightarrow{v} = 0$, on admet l'existence d'une fonction courant $\psi$ telle que :

\begin{equation}
	\label{eq:courant}
	\left\lbrace
	\begin{array}{l}
		v_z = \dfrac{\partial \psi}{\partial x} \\[.2cm]
		v_x = -\dfrac{\partial \psi}{\partial z}
	\end{array}
	\right.
\end{equation}

On pose le crochet de Poisson de $A$ et de $B$ :

\begin{equation}
	\label{eq:poisson}
	\left\lbrace A,\,B\right\rbrace = \partial_x A\partial_zB - \partial_z A\partial_x B
\end{equation}

Le fluide possède une structure périodique en espace de paramètre $a$, c'est à dire que si un champ scalaire ou vectoriel $A$ décrit une grandeur physique de ce fluide alors :

\begin{equation}
	\label{eq:perio}
	\forall x, z \in \mathbb{R}, \, A(x, z) = A(x + a, z)
\end{equation}

Au repos, pour tout $\delta T\neq 0$, le fluide est instable.

\newpage

\renewcommand{\theequation}{\arabic{equation}}
\setcounter{equation}{0}
\subsection{Obtention des équations de Lorenz}

\subsubsection{Équations de Saltzman}

\subsubsection*{Le fluide au repos}

Le fluide incompressible vérifie les équations de Navier-Stokes incompressibles. À ce jeu d'équations se rajoute l'équation de la chaleur de diffusivité thermique $\mathcal{D}$.

\begin{align}
	\label{eq:NS}
	\rho \dfrac{\Diff \overrightarrow{v}}{\Diff t} &= \rho \overrightarrow{g} - \overrightarrow{\nabla}p + \mu \Delta \overrightarrow{v} \\
	\label{eq:EC}
	\dfrac{\Diff T}{\Diff t} &= \mathcal{D}\Delta T
\end{align}

Or $\rho$ est une fonction linéaire de la température, donc en utilisant le développement de Taylor-Young à l'ordre 1 à $T_0$, on peut écrire :
\begin{align*}
	\rho(T)& = \rho(T_0) + (T-T_0)\dfrac{\partial \rho}{\partial T}(T_0) + \underset{T\rightarrow T_0}{o}(T - T_0)\\
	&\overset{\ref{eq:rline}}{\approx} \rho_0\left(1-\alpha\left(T-T_0\right)\right)
\end{align*}
avec $\rho_0 = \rho(T_0)$ et $\alpha=-\frac{1}{\rho_0}\frac{\partial \rho}{\partial T}(T_0)$.

Avec l'expression que l'on a de la température au repos, on peut déduire l'expression de la masse volumique au repos.

\[\rho_r(z) \overset{\ref{eq:tline}}{=} \rho_0\left(1-\alpha(T_r - T_0)\right)\]

Au repos, l'équation (\ref{eq:NS}) devient :

\begin{align*}
	\overrightarrow{\nabla}p_r &= \rho_r \overrightarrow{g} \\
	&= - \rho_0\left(1+\alpha\dfrac{z}{h}\delta T\right)g \overrightarrow{u_z}\\
	p_r(z) &\overset{\int\diff z}{=} p_0 - g \rho_0 z - \dfrac{\rho_0\alpha\delta T}{2h}z^2
\end{align*}

\newpage

\subsubsection*{Adimensionnement des équations de Navier-Stokes et de la chaleur}

Il sera plus aisé de résonner avec des paramètres adimensionnés. On pose donc $t' = \frac{\mathcal{D}}{h^2}t$, $x' = \frac{1}{h}x$, $z'=\frac{1}{h}z$. On exprime les différents termes des équations (\ref{eq:NS}) et (\ref{eq:EC}) de manière adimensionnée.

Les opérateurs de dérivations se réécrivent : $\dfrac{\Diff}{\Diff t'} = \dfrac{h^2}{\mathcal{D}}\dfrac{\Diff}{\Diff	t}$, $\overrightarrow{\nabla}' = h\overrightarrow{\nabla}$ et $\Delta' = h^2\Delta$.

Pour les champs scalaires, on introduit un champ scalaire décrivant les variations du champ autour d'une position d'équilibre. On introduit $\pi$ pour la pression et $\theta$ pour la température.

\begin{align}
	p(x, z, t) &= p_r(z) + \dfrac{\rho_0 \mathcal{D}^2}{h^2}\pi(x', z', t') \\
	T(x, z, t) &= T_r(z) + \delta T \theta(x', z', t')
\end{align}

On peut rapidement vérifier l'homogénéité de ces deux équations. On peut exprimer $\rho$ en fonction de $\theta$.


\begin{align*}
	\rho - \rho_r &= (\rho - \rho_0) - (\rho_r - \rho_0)\\
	&= -\rho_0\alpha((T-T_0) - (T_r - T_0))\\
	&= -\rho_0\alpha\delta T\theta
\end{align*}

Faisons encore quelques calculs préliminaires afin d'adimensionner nos équations :

\begin{minipage}{0.48\textwidth}
		\begin{align*}
			\overrightarrow{\nabla}'\pi &= \dfrac{h^3}{\rho_0\mathcal{D}^2}\overrightarrow{\nabla}(p-p_r)\\
			&= \dfrac{h^3}{\rho_0\mathcal{D}^2}\left(\overrightarrow{\nabla}p - \rho_r \overrightarrow{g}\right)
		\end{align*}
\end{minipage}~
\begin{minipage}{0.48\textwidth}   
	\begin{align*}
			\Delta' \theta &= \dfrac{h^2}{\delta T}\Delta(T - T_r)\\
			&= \dfrac{h^2}{\delta T}\Delta T
		\end{align*}
\end{minipage}\\	

Tout d'abord, adimensionnons l'équation de Navier-Stokes (\ref{eq:NS}) : 

\begin{align*}
	\dfrac{\Diff \overrightarrow{v}'}{\Diff t'} &= \dfrac{h^3}{\mathcal{D}^2}\dfrac{\Diff \overrightarrow{v}}{\Diff t}\\
	& \overset{\ref{eq:bouss}\mathrm{, }\ref{eq:NS}}{=} \dfrac{h^3}{\mathcal{D}^2\rho_0}\left(\rho \overrightarrow{g} - \overrightarrow{\nabla}p + \mu \Delta \overrightarrow{v}\right)\\
	&= \dfrac{h^3}{\mathcal{D}^2\rho_0}\left(\rho \overrightarrow{g} -\dfrac{\mathcal{D}^2\rho_0}{h^3}\overrightarrow{\nabla}'\pi - \rho_r \overrightarrow{g} + \mu \dfrac{\mathcal{D}}{h^3}\Delta'\overrightarrow{v}' \right)\\
	&= \dfrac{h^3}{\mathcal{D}^2\rho_0}\left(-\rho_0\alpha\delta T\theta \overrightarrow{g} -\dfrac{\mathcal{D}^2\rho_0}{h^3}\overrightarrow{\nabla}'\pi + \mu \dfrac{\mathcal{D}}{h^3}\Delta'\overrightarrow{v}' \right)\\
	&= \dfrac{h^3\alpha g\delta T}{\mathcal{D}^2}\theta\overrightarrow{u_z} - \overrightarrow{\nabla}'\pi + \dfrac{\mu}{\mathcal{D}\rho_0}\Delta'\overrightarrow{v}'
\end{align*}

\newpage

Ensuite adimensionnons l'équation de la chaleur (\ref{eq:EC}) :

\begin{align*}
	\dfrac{\Diff \theta}{\Diff t'} &= \dfrac{h^2}{\delta T\mathcal{D}}\dfrac{\Diff}{\Diff t}(T-T_r)\\
	&\overset{\ref{eq:EC}}{=} \dfrac{h^2}{\delta T\mathcal{D}}\left(\Delta T - \dfrac{\partial T_r}{\partial t} - \left(\overrightarrow{v}\cdot\overrightarrow{\nabla}\right)T_r\right)\\
	&= \Delta'\theta - \dfrac{h^2}{\delta T\mathcal{D}}\dfrac{\mathcal{D}}{h^2}v_z' \times(-\delta T)\\
	&= v_z' + \Delta'\theta
\end{align*}


Si on pose $\sigma = \dfrac{\mu}{\rho_0 \mathcal{D}}$ le nombre de Prandtl et $R = \dfrac{\alpha g h^3 \rho_0 \delta T}{\mu\mathcal{D}}$ le nombre de Rayleigh, on trouve finalement :

\begin{align}
	\label{eq:saltz1}
	\dfrac{\Diff \overrightarrow{v}'}{\Diff t'} = R\sigma\theta\overrightarrow{u_z} - \overrightarrow{\nabla}'\pi + \sigma\Delta'\overrightarrow{v}'\\
	\label{eq:saltz2}
	\dfrac{\partial \theta}{\partial t'} + \left(\overrightarrow{v}'\cdot\overrightarrow{\nabla}'\right)\theta - v_z' = \Delta'\theta
\end{align}

À la vue des derniers résultats obtenus, on se passera de la notation $'$.

On peut simplifier l'équation  (\ref{eq:saltz1}) en lui appliquant un opérateur rotationnel :

\begin{align*}
	\partial_z \dfrac{\Diff v_x}{\Diff t} - \partial_x \dfrac{\Diff v_z}{\Diff t} &= -R\sigma \partial_x \theta + \sigma \partial_z \Delta v_x - \sigma \partial_x \Delta v_z\\
	\overset{Schwarz}{\Rightarrow} \dfrac{\Diff \overrightarrow{\nabla}\wedge\overrightarrow{v}}{\Diff t} &= -R\sigma \partial_x \theta + \sigma \Delta \left(\overrightarrow{\nabla}\wedge \overrightarrow{v}\right)
\end{align*}

Or $\overrightarrow{\nabla}\wedge\overrightarrow{v} = \partial_z v_x - \partial_x v_z \overset{\ref{eq:courant}}{=} -\partial_x^2 \psi - \partial_z^2\psi = -\Delta \psi$, donc :

\begin{align*}
	\dfrac{\Diff \overrightarrow{\nabla}\wedge \overrightarrow{v}}{\Diff t} &= -\dfrac{\Diff \Delta \psi}{\Diff t}\\
	&= -\partial_t \Delta \psi - \left(\overrightarrow{v}\cdot\overrightarrow{\nabla}\right)\Delta\psi\\
	&\overset{\ref{eq:poisson}}{=} -\partial_t \Delta \psi - \left\lbrace\Delta\psi,\,\psi\right\rbrace
\end{align*}

De même on peut réécrire l'équation (\ref{eq:saltz2}), ce qui nous donne les équations de Saltzman :

\begin{align}
	\label{eq:SM1}
	\partial_t \Delta \psi + \left\lbrace\Delta\psi,\,\psi\right\rbrace -R\sigma\partial_x\theta - \sigma \Delta^2\psi &= 0\\
	\label{eq:SM2}
	\partial_t\theta - \left\lbrace\psi,\,\theta\right\rbrace+\partial_x\psi  - \Delta \theta &= 0
\end{align}

Ce système est désormais d'inconnues $\psi$ et $\theta$. Lorenz formule quelques hypothèses supplémentaires pour pouvoir établir les équations de Lorenz.

\newpage

\subsubsection{Approximations de Lorenz}

L'hypothèse de périodicité spatiale (\ref{eq:perio}) nous permet de développer les champs $\theta$ et $\psi$ en modes de Fourier, suivant :

\[\phi(x, z, t) = \sum_{n={-\infty}}^\infty \sum_{m=-\infty}^\infty	\phi_0(t) \exp\left[\pi i \left(\frac{2mx}{a} + nz\right)\right]\]

Lorenz ne retient que sur les trois modes de plus basse fréquence, soit :

\begin{align*}
	&\psi(x, z, t) = \tilde{X}(t) \sin(\pi z)\sin\left(2\pi \dfrac{x}{a}\right)\\
	&\theta(x, z, t) = \tilde{Y}(t) \sin(\pi z)\cos\left(2\pi \dfrac{x}{a}\right) -  \tilde{Z}(t)\sin(2\pi z)
\end{align*}

Ce choix a été fait de telle sorte à vérifier les conditions de bord sur la température constante en $z=0$ et en $z=1$ ainsi que la vitesse verticale et la force de cisaillement nulle en $z=0$ et en $z=1$.

Maintenant que cette forme de solution est choisie, on va chercher à exprimer les nouvelles inconnues de notre système : $\tilde{X}$, $\tilde{Y}$ et $\tilde{Z}$. Pour se faire, il nous faut exprimer chaque terme des équation (\ref{eq:SM1}) et (\ref{eq:SM2}).

\begin{align*}
	&\Delta \psi = -\pi^2\left(1+\dfrac{4}{a^2}\right)\psi\\
	&\Delta^2 \psi = \pi^4\left(1+\dfrac{4}{a^2}\right)^2\psi\\
	&\left\lbrace \Delta \psi,\,\psi\right\rbrace = -\pi^2\left(1+\dfrac{4}{a^2}\right)\left\lbrace \psi,\,\psi\right\rbrace = 0\\
	&\partial_x \psi = \dfrac{2\pi}{a}\tilde{X}(t) \sin(\pi z)\cos\left(2\pi \dfrac{x}{a}\right)\\
	&\partial_t \psi = \dot{\tilde{X}}(t) \sin(\pi z)\sin\left(2\pi \dfrac{x}{a}\right)\\
	&\partial_x \theta = -\dfrac{2\pi}{a}\tilde{Y}(t) \sin(\pi z)\sin\left(2\pi \dfrac{x}{a}\right)\\
	&\partial_t \theta = \dot{\tilde{Y}}(t) \sin(\pi z)\cos\left(2\pi \dfrac{x}{a}\right) -  \dot{\tilde{Z}}(t)\sin(2\pi z)\\
	&\Delta \theta = -\pi^2\left(1+\dfrac{4}{a^2}\right)\tilde{Y}\sin(\pi z)\cos\left(2\pi\dfrac{x}{a}\right) + (2\pi)^2\tilde{Z}\sin(2\pi z)\\
	& \left\lbrace \psi,\,\theta\right\rbrace = \dfrac{\pi^2}{a}\tilde{X}\tilde{Y}\sin(2\pi z) + \dfrac{2\pi^2}{a}\tilde{X}\tilde{Z}[\sin(\pi z) - \sin(3\pi z)]\cos\left(2\pi\dfrac{x}{a}\right)
\end{align*}

On peut revenir sur la dernière égalité et constater qu'un terme de mode supérieur à trois apparait, on ne le prend donc pas en compte à la vue de l'hypothèse réalisée.

\newpage

On peut donc réécrire l'équation (\ref{eq:SM2}) :

\begin{align*}
	\sin(\pi z)\cos\left(2\pi\dfrac{x}{a}\right)&\left[-\dot{\tilde{Y}}(t)- \dfrac{2\pi^2}{a}\tilde{X}\tilde{Z} + \dfrac{2\pi}{a}\tilde{X}(t) -\pi^2\left(1+\dfrac{4}{a^2}\right)\tilde{Y}\right]\\ -  \sin(2\pi z)&\left[-\dot{\tilde{Z}}(t) + \dfrac{\pi^2}{a}\tilde{X}\tilde{Y} -(2\pi)^2\tilde{Z}\right]  = 0
\end{align*}

La liberté de la famille $\left( z \mapsto \sin(\pi z),\, z\mapsto\sin(2\pi z)\right)$ dans $\mathbb{R}^\mathbb{R}$ nous permet de déduire l'annulation des deux coefficients. Si on réécrit de plus l'équation \ref{eq:SM1}, on trouve le système suivant :

\begin{align*}
	\dot{\tilde{X}}(t) &= \dfrac{2R\sigma}{a\pi\left(1+\dfrac{4}{a^2}\right)}\tilde{Y}(t)-\sigma \pi^2\left(1+\dfrac{4}{a^2}\right)\tilde{X}(t)\\
	\dot{\tilde{Y}}(t) &= - \dfrac{2\pi^2}{a}\tilde{X}\tilde{Z} + \dfrac{2\pi}{a}\tilde{X}(t) -\pi^2\left(1+\dfrac{4}{a^2}\right)\tilde{Y}\\
	\dot{\tilde{Z}}(t) &= \dfrac{\pi^2}{a}\tilde{X}\tilde{Y} -(2\pi)^2\tilde{Z}
\end{align*}

En effectuant quelques changements de variables : $t = \alpha \tau$, $\tilde{X} = \beta X$, $\tilde{Y} = \gamma Y$, $\tilde{Z} = \delta Z$, et en posant : $\alpha = \dfrac{1}{4\pi^2}b$, $\beta = \dfrac{4a}{\sqrt{2}b}$, $\gamma = \dfrac{\sqrt{2}}{\pi r}$ et $\delta = \dfrac{1}{\pi r}$, on aboutit au système suivant :

\begin{equation}
\left\lbrace
\begin{array}{l}
	\dot{X}(\tau) = -\sigma(X-Y)\\
	\dot{Y}(\tau) = rX - Y - XZ\\
	\dot{Z}(\tau) = XY - bZ
\end{array}
\right.
\end{equation}
de paramètres indépendants du modèle : $r = \dfrac{4R}{\pi^4a^2\left(1+\dfrac{4}{a^2}\right)^3}$, $b = \dfrac{4}{1+\dfrac{4}{a^2}}$ et $\sigma$

Il reste néanmoins à déterminer la valeur de $a$ la plus probable. La dernière hypothèse formulée (instabilité du fluide au repos pour toute valeur de $\delta T\neq 0$) nous permet de conclure. Ici $\delta T$ n'agit que sur $r$, il faut donc rendre le point $(0,\,0,\,0)$ instable pour toute valeur de $r$\newpage

On linéarise donc le système à l'origine :
\hspace*{-.5cm}
\begin{align*}
	\begin{bmatrix}
		\dot{X}\\
		\dot{Y} \\ 
		\dot{Z} \\
	\end{bmatrix} =
	\begin{bmatrix}
		-\sigma & \sigma & 0\\
		r & -1 & 0\\
		0 & 0 & -b
	\end{bmatrix}
	\begin{bmatrix}
		{X}\\
		{Y} \\ 
		{Z} \\
	\end{bmatrix}
\end{align*}

On a instabilité si le spectre de notre application linéaire possède au moins une valeur propre à partie réelle positive. $-b$ est une valeur propre évidente et n'est pas à partie réelle positive. Supposons que l'on ait une valeur propre à partie réelle positive pour la sous matrice :

\[M = \begin{bmatrix}
		-\sigma & \sigma \\
		r & -1 
\end{bmatrix}\]

Or $\mathrm{tr}(M) = \lambda_1 + \lambda_2 = -1-\sigma < 0$. Donc on a on a nécessairement une valeur propre à partie réelle négative. On déduit notamment que nos deux valeurs propres sont réelles. Ainsi $\mathrm{det}(M) = \lambda_1\lambda_2 = \sigma(1-r)<0$. On conclut :

\begin{equation}
	R > \dfrac{\pi^4 a^2}{4}\left(1 + \dfrac{4}{a^2}\right)^3
\end{equation}

On maximise nos chance de satisfaire cette condition pour $a$ qui minimise le second membre. $a=2\sqrt{2}$ minimise ce terme.

Ainsi, on peut réécrire les coefficients de notre problème $\alpha = \frac{2}{3\pi^2}$, $\beta = 3$, $\gamma = \frac{\sqrt{2}}{\pi r}$ et $\delta = \frac{\sqrt{2}}{\pi r}$.

On peut conclure que les équations de Lorenz s'écrivent :

\begin{equation}
\label{eq:lorenz}
\left\lbrace
\begin{array}{l}
	\dot{X}(\tau) = -\sigma(X-Y)\\
	\dot{Y}(\tau) = rX - Y - XZ\\
	\dot{Z}(\tau) = XY - bZ
\end{array}
\right.
\end{equation}

Avec les paramètres : $r = \dfrac{4R}{27\pi^4}$, $b = \dfrac{8}{3}$ et $\sigma$.

Pour l'eau, on a $\sigma$ de l'ordre de 10. \cite{baines_lorenz_2008}. \\[.2cm]

Ainsi $r$, le nombre de Rayleigh modifié, est le seul paramètre que l'on pourra faire varier, si notre fluide étudié est l'eau.\\[.2cm]

\emph{Note : ces équations peuvent se retrouver pour d'autres systèmes physiques, comme le moulin de Lorenz.}

\newpage

On peut revenir à la signification de $X$, $Y$ et $Z$. On peut noter que l'équation de variation de $X$, provient de l'équation de quantité de mouvement et que le terme source en $Y$ y représente la contribution de la diffusion thermique alors que celui en $Z$ ,de la diffusion visqueuse. $X$ représente l'intensité de la convection. $Y$ et $Z$ représentent les variations de la température. Par construction des modes retenus par Lorenz dans son modèle, $Z$ n'intervient pas explicitement dans l'équation de convection. Il joue donc un rôle particulier dans le système dynamique. $Y$ est le mode explicitant la différence de température entre les flux convectifs ascendant et descendant alors que $Z$ est la distorsion de la température verticale, et intervient sur un mode plus élevé.

\newpage

\subsection{Équilibres et stabilité}

On cherche les points d'équilibre du système \ref{eq:lorenz}, c'est-à-dire les triplets ${U = (X,\,Y,\,Z)}$ constant au cours du temps, ce qui revient au problème suivant :

\begin{equation}
	\label{eq:lorenzeq}
	\left\lbrace
	\begin{array}{l}
		X = Y\\
		(r-1)X=Z\\
		X^2 = bZ
	\end{array}
	\right.
\end{equation}

Sachant que $\sigma > 0$ et $b > 0$, on peut donc distinguer deux cas :

\begin{itemize}
	\item soit $\mathbf{r<1}$ et dans ce cas $X$ est réel si et seulement si $X$ est nul. Le seul équilibre du problème est donc $u = (0,\; 0,\; 0)$.
	\item soit $\mathbf{r\geq 1}$ et dans ce cas, il n'y a aucune contrainte sur les choix de $X$, $Y$ et $Z$. Il se dégage trois points d'équilibres :
	
	\begin{align*}
		&v_0=(0,\;0,\;0)\\
		&v_+=(\sqrt{b(r-1)},\;\sqrt{b(r-1)},\;r-1)\\
		&v_-=(-\sqrt{b(r-1)},\;-\sqrt{b(r-1)},\;r-1)
	\end{align*}
\end{itemize}

On va maintenant déterminer la stabilité des équilibres dans chacun des cas. Pour se faire, on calcule la matrice Jacobienne du système \ref{eq:lorenzeq}. Pour un équilibre $u$, on a $u$ stable si et seulement si la partie réelle des valeurs propres de la Jacobienne est strictement négative.

\[\mathit{J}_{(\ref{eq:lorenzeq})} = 
\begin{bmatrix}
	-\sigma & \sigma & 0\\
	r-Z & - 1 & X\\
	Y & X & -b
\end{bmatrix}\]

Pour la cas $\mathbf{r<1}$, on a un seul équilibre $u$. Le polynôme caractéristique de $\mathit{J}_{(\ref{eq:lorenzeq})}(u)$ est le suivant :

\[\chi(X) = (X+b)[X^2 + (\sigma+1)X +(1-r)\sigma]\]

En particulier, on remarque que l'on a un polynôme de degré 1 multiplier par un polynôme de degré 2 qui a tout ses coefficients positifs. On vérifie facilement que le polynôme de degré 2 a des racines à partie réelles strictement négatives. De même pour celui de degré 1. Ainsi $u$ est un équilibre stable.

\newpage

Pour le cas $\mathbf{r\geq 1}$, on a plusieurs équilibres. \\Le polynôme caractéristique de $\mathit{J}_{(\ref{eq:lorenzeq})}(v_0)$ est le même que le précédent. Cependant, $\chi(0) < 0$ et $\lim_{x\rightarrow\infty} \chi(x)= \infty$, ainsi, par le théorème des valeurs intermédiaires, on déduit l'existence d'une valeur propre à partie réelle strictement positive. $v_0$ n'est pas stable.\\
Les polynômes caractéristiques de $\mathit{J}_{(\ref{eq:lorenzeq})}(v_1)$ et $\mathit{J}_{(\ref{eq:lorenzeq})}(v_2)$ sont les mêmes :

\[\chi(X) = X^3 + (1+\sigma + b)X^2 + b(\sigma +r)X + 2b\sigma (r-1)\]

En utilisant le théorème des valeurs intermédiaires sur l'intervalle $]-\infty;\:0[$, on trouve une racine réelle strictement négative.

On suppose que la solution peut se mettre sous la forme $X_0\exp(i\omega t)$, ce qui nous permettra de trouver la valeur de $r$ critique pour laquelle les deux autres valeurs propres sont à partie réelle nulle. Pour cela, on pose $\omega \in \mathbb{R}$ et on impose :

\[\chi(i\omega) = 0\]

En annulant partie réelle et partie imaginaire, on impose deux égalités.

\begin{align*}
	\left\lbrace
	\begin{array}{l}
		\omega^2=b(\sigma + r)\\
		(1+\sigma + b)\omega^2 = 2b\sigma(r-1)
	\end{array}
	\right.
	\Rightarrow r(1+\sigma + b - 2 \sigma) = -2\sigma - \sigma - \sigma^2 - \sigma b
\end{align*}

Ainsi $r_{critique} = \sigma\dfrac{\sigma + b + 3}{\sigma - b - 1}$. Pour $r < r_{critique}$,  $v_1$ et $v_2$ sont des équilibres stables, sinon ils sont instables.

Avec $\sigma$ de l'ordre de 10 pour l'eau, on trouve $r_{critique} \approx 24.74$ (valeur proposée par Saltzmann en 1962 \cite{S62}). Lorenz a montré le comportement de la solution pour $r=28$. Il observe des équilibres instables, avec un comportement chaotique de la solution \cite{L63}.