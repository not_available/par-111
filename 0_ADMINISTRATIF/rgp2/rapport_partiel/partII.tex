\subsection{Convergence de méthode et modèle de référence}

Nous allons désormais passer à une première étude numérique pour pouvoir vérifier les quelques résultats énoncés. Avant toutes choses, il est nécessaire de devoir écrire des méthodes d'intégration permettant de résoudre numériquement des équations différentielles du type :

\begin{equation}
    \label{eq:ex}
	\left\lbrace
	\begin{array}{l}
		U' = f(t, U(t))\\
		U(0) = a
	\end{array}
	\right.
\end{equation}
où $U$ est un vecteur.

On se propose d'écrire trois méthode d'intégration numérique : 

\begin{itemize}
	\item Euler explicite :
		\begin{equation*}
			\left\lbrace
			\begin{array}{l}
				U_{n+1} = U_n + \diff t f(t_n, U_n)\\
				U_0 = a
			\end{array}
			\right.
		\end{equation*}
	\item Runge Kutta 2 (ou Euler modifié) :
		\begin{equation*}
			\left\lbrace
			\begin{array}{l}
				U_{n+1} = U_n + \diff t f\left(t_n + \dfrac{\diff t}{2}, U_n + \dfrac{\diff t}{2}f(t_n, U_n)\right)\\
				U_0 = a
			\end{array}
			\right.
		\end{equation*}
	\item Runge Kutta 4:
		\begin{equation*}
			\left\lbrace
			\begin{array}{l}
				U_{n+1} = U_n + \dfrac{\diff t}{6}(k_1+2k_2+2k_3+k_4)\\[.2cm]
				k_1 = f(t_n, U_n)\\[.2cm]
				k_2 = f\left(t_n + \dfrac{\diff t}{2}, U_n + \dfrac{\diff t}{2}k_1\right)\\[.4cm]
				k_3 = f\left(t_n + \dfrac{\diff t}{2}, U_n + \dfrac{\diff t}{2}k_2\right)\\[.3cm]
				k_4 = f\left(t_{n+1}, U_n + \diff tk_3\right)\\[.2cm]
				U_0 = a
			\end{array}
			\right.
		\end{equation*}
\end{itemize} 

Les équations sont intégrées en temps sur l'intervalle [0, $t_f$]. En prenant un pas de temps régulier $\mathrm{d}t$, le nombre de pas de temps effectués est $N = \dfrac{t_f}{\mathrm{d}t}$.

\newpage

Ces méthodes sont respectivement d'ordre 1, 2 et 4. On dit d'une méthode qu'elle est d'ordre $k\in \mathbb{N}^*$ si pour une solution $y$ de \ref{eq:ex}, alors

\[\underset{n\leq N}{\mathrm{max}} \left\Vert y(t_n) - y_n \right\Vert = \mathcal{O}\left(\mathrm{d}t^k\right)\]

Les trois méthodes présentées ont une complexité temporelle linéaire avec le nombre de points que l'on calcule. On comprend l'intérêt d'utiliser une méthode Runge Kutta 4 par rapport à une Runge Kutta 2. \cite{resode} \\[.2cm]

On implémente ces trois méthodes sous Python. La création de quelques objets nous permet de rassembler les paramètres d'un système sous la forme d'un vecteur et leur évolution temporelle est rassemblée dans un objet de trajectoire. Quelques opérations utiles ont été créées pour l'occasion, on peut les retrouver dans le script \texttt{my\_objects.py}. On introduit la norme d'un vecteur (donc d'un état) avec la norme deux usuelle :
\[\forall U = \begin{bmatrix} u_1 & \dots & u_k\end{bmatrix}^\top,\: \Vert U\Vert = \sqrt{\sum_{i=1}^k u_i^2}\] cette norme nous permettra de connaître la distance entre deux états (au même instant) quand on fera l'étude de convergence des schémas d'intégration temporelle.

Ces fonctions sont dans le script \texttt{integration.py}. On peut d'assurer de la complexité temporelle et de l'ordre des méthodes pour quelques systèmes d'équations différentielles connues via le script \texttt{complexity\_and\_error.py}.

On peut s'assurer rapidement que la complexité est en $\mathcal{O}(N)$ et que les ordres des trois méthodes sont respectivement 1, 2 et 4. En revanche ces ordres ont été déterminés pour des systèmes dont on connait les solutions analytiques (trois exemples de systèmes d'équations différentielles non-linéaires sont à la fin du script de \newline \texttt{complexity\_and\_error.py}). Le but va être de de vérifier la précision de la solution afin de pouvoir distinguer les erreurs numériques liées à l'intégration du schéma, et les perturbations amplifiées par le modèle physique.

Pour se faire, on admet qu'il existe un $N \in \mathbb{N}$ tel que l'erreur avec la vraie solution soit de l'ordre de $10^{-16}$ ce qui correspond au zéro machine d'un réel codé dans la précision de la norme IEEE 754. Pour toutes simulations avec $K \geq N$ valeurs calculées, on peut dire que la simulation est le modèle de référence du problème. L'étude de la convergence du modèle numérique se fera en calculant différentes simulation et en calculant l'erreur vis-à-vis d'une solution de référence. Par exemple pour la méthode Runge-Kutta 4, on trace ci dessous l'évolution de l'erreur en fonction du nombre de points calculés par rapport au modèle de référence.

\newpage

\input{tikz/II/err_step}

Le modèle de référence a été déterminé pour $r = 28$, $\sigma = 10$, $b = \frac{8}{3}$, ${N=100\,000\,001}$, $t_f=60$, $a=\begin{bmatrix}
	0 & 1 & 0
\end{bmatrix}^\top$. La condition initiale et le temps de simulation sont ceux que Lorenz avait choisi dans son article \cite{L63}. Seuls cent points sont conservés par simulation, ils correspondent aux instants 0.6, 1.2, ..., 60.

On remarque que l'erreur globale augmente avec le temps. On remarque aussi qu'il faut des pas de temps plus faible pour espérer arriver à la saturation de la convergence pour des temps de simulation plus longs. Pour le temps de simulation $t_f$, la méthode ne converge pas.

\newpage

\noindent Si l'on se concentre sur la \textsc{Figure} \ref{sfug:sat}, trois phénomènes peuvent être interprétés. 

Pour $N$ inférieur à $20\,000$, on a un seuil puis un chute légère de l'erreur. Ce phénomène peut s'apparenter à de la saturation. Le pas de temps permet au système de ne pas diverger, en revanche il ne se comporte pas de la manière attendue. Ainsi l'erreur est limitée par la taille de l'attracteur lui-même.

Pour $N$ compris entre $20\,000$ et $500\,000$, on a une chute de l'erreur qui se fait en $N^{-4.08}$ (lecture graphique), ce qui correspond presque à la vitesse de convergence de la méthode numérique Runge-Kutta 4.

Pour $N$ supérieur à $500\,000$ on observe l'apparition d'un seuil. C'est une fois de plus de la saturation et elle témoigne d'un arrêt de convergence de la méthode. On peut se demander pourquoi la convergence ne se fait pas jusqu'au zéro machine. On remarque qu'une simulation faite pour $N = 5\,000\,001$ a été effectuée et que l'on observe une chute de l'erreur avant de la voir remonter, le problème vient probablement du fait que les paramètres $r$, $\sigma$ et $b$ ont été choisis tels que le système est chaotique (voir \ref{ssec:chaos}). 

On se propose d'illustrer cette apparition du chaos en traçant, pour un $N$ donné, l'évolution de l'erreur en fonction du temps où l'on regarde le système :

\input{tikz/II/err_temps}

On remarque que l'erreur initiale est inférieur à $10^{-8}$ jusqu'à l'instant $t=20$. À partir de cet instant, l'erreur devient croissance en $10^{0.26t}$ (lecture graphique) jusqu'à arriver au phénomène de saturation pour lequel le système ne converge pas, sans partir à l'infini.

Cette restriction en temps nous permet de voir que le système devient amplificateur seulement à partir d'un certain instant. La nature chaotique du système est peut être à l'origine de cette amplification d'erreur.

On remarque que pour des temps de simulation de l'ordre de 20, la simulation obtenue pour $N = 10^5$ est relativement proche de celle faite pour $N=10^8$.

\newpage

On décide d'étudier la convergence du modèle en fonction de $r$. On représente ci dessous l'évolution de l'erreur à $t=60$ en fonction de $r$, pour une simulation de $N = 100\,001$. Chaque modèle de référence a été établi pour une simulation où $N = 5\,000\,001$.

\input{tikz/II/err_r}

On constate que l'erreur augmente avec $r$ (cinq ordres de grandeur pour $r$ allant de 0.1 à 20) pour $\rho > \rho_{crit}$ l'erreur à l'instant $t_f$ augmente de douze ordres de grandeur. Ainsi le chaos est à l'origine de nos problèmes de convergence.\\[.2cm]

On se propose de se placer dans un cas non chaotique et de suivre l'évolution de l'erreur pour les trois méthodes d'intégrations temporelles introduites dans ce document, à savoir Euler explicite, Runge-Kutta 2 et Runge-Kutta 4. On choisit arbitrairement $\rho = 20$ et un modèle a été calculé par chacune de ces méthodes avec $N=100\,000\,001$.

\input{tikz/II/err_mod}

\newpage

On conclut sur la convergence du modèle pour les méthodes proposées avec les vitesse de convergence que l'on est censé avoir. En revanche, cette convergence n'est possible que dans les cas non chaotique. Le graphe ci-dessus nous encourage à utiliser la méthode Runge-Kutta 4 pour $N$ valant au moins $10^5$.\\[.2cm]

\newpage

On se propose de tracer l'attracteur de Lorenz, dans un cas chaotique, à savoir $\rho = 28$, avec $t_f=28$, $a = \begin{bmatrix}0&1&0\end{bmatrix}^\top$ et $N = 1\,000\,001$ (le graphe \ref{sfug:sat} nous confirme la convergence pour cette valeur).

\input{tikz/II/ref3d}

\newpage

Pour une meilleure lisibilité et puisqu'ils n'ont pas la même signification physique, on se propose de tracer indépendamment $X$, $Y$ et $Z$ en fonction de $t$.


\input{tikz/II/ref2d}

Avec cette représentation, on peut par exemple savoir combien de temps on orbite autour d'un équilibre avant d'orbiter vers l'autre grâce à la \textsc{Figure} \ref{fig:refx} ou la \textsc{Figure} \ref{fig:refy}. Ce nombre semble relativement chaotique : parfois un, deux, trois, \dots \\Si l'on observe sur une plus longue durée, aucun schéma ne semble apparaître.

\newpage

\subsection{Étude numérique des équilibres}

L'étude des positions d'équilibre et de leur nature se fera par étude graphique. Il sera plus raisonnable de tracer les composantes $X$, $Y$ et $Z$ en fonction de $t$ plutôt que le diagramme de phases.

Pour $r <1$, on s'assure que seul $0$ est un équilibre et qu'il est stable. Voici un exemple pour $r = 0.1$ (valeur choisie lors de notre étude sur l'influence de $r$) et $a=\begin{bmatrix}
	0 & 1 & 0
\end{bmatrix}^\top$.

\input{tikz/II/eq_stab0.1}

On observe la convergence de notre condition initiale vers 0.

\newpage

Pour $1<r<r_{critique}$, on s'assure d'avoir trois points d'équilibre, deux sont stables mais pas le troisième. Voici quelques exemples avec $r = 20$ (valeur deux fois choisie précédemment) et $a_1=\begin{bmatrix}
	0 & 1 & 0
\end{bmatrix}^\top$, $a_2=\begin{bmatrix}
	0 & -1 & 0
\end{bmatrix}^\top$, $a_3=\begin{bmatrix}
	0 & 10^{-12} & 0
\end{bmatrix}^\top$, $a_4=\begin{bmatrix}
	0 & -10^{-12} & 0
\end{bmatrix}^\top$. Ces conditions initiales représentent respectivement le choix de Lorenz dans sa publication \cite{L63}, son opposé dans la direction $X$ et $Y$, une perturbation à l'origine de l'ordre de $10^{-12}$, valeur représentant le seuil de convergence pour $r=20$ (\ref{fig:err}) et son opposé dans les directions $X$ et $Y$.

\input{tikz/II/eq_stab20}

Enfin, pour le cas $r_{critique}<r$, tous les points d'équilibres sont instables. On le vérifie avec $r=28$ de la même manière que précédemment. On se propose cette fois ci de tracer l'instant pour lequel, l'écart relatif entre l'équilibre et l'état instable est supérieur à 1\% en fonction de la perturbation $\varepsilon$. On effectuera cette perturbation selon les directions $X$, $Y$ ou $Z$. On considère à chaque fois les trois directions principales en guise de condition initiale à savoir : $\begin{bmatrix}
	1 & 0 & 0
\end{bmatrix}^\top$, $\begin{bmatrix}
	0 & 1 & 0
\end{bmatrix}^\top$ et $\begin{bmatrix}
	0 & 0 & 1
\end{bmatrix}^\top$. On peut noter pour la condition initiale de direction $Z$ et perturbée selon $Z$, $X$ et $Y$ deviennent les fonctions nulles et $Z$ converge. On n'étudiera donc pas ce cas.

\newpage

\input{tikz/II/err_growth}

Les graphes en \textsc{Figure} \ref{sfig:growthx} et \ref{sfig:growthy} nous permet de déduire une croissance de la perturbation de manière exponentielle en $\exp^{0.9t}$ (lecture graphique). En ce qui concerne le graphe en \textsc{Figure} \ref{sfig:growthz}, la croissance de l'erreur est en $\exp^{14.5t}$.\\[1em]

Ces coefficients ne sont pas anodins et sont appelés les coefficients de Lyapunov. On peut les définir de la manière suivante :\\[.2cm]

On pose pour tout $t$, $U(t)\in\mathbb{R}^d$ et $U_0(t)\in\mathbb{R}^d$, de manière à ce que $U$ et $U_0$ décrivent des trajectoires dans un espace de phases de dimension $d$. On note pour tout $t$, $\delta U(t) = U(t) - U_0(t)$ et $\delta U_0 = U(0) - U_0(0)$. Si pour $t$ suffisamment grand :

\[\vert\delta U(t)\vert \approx \vert \exp(\lambda t)\delta U_0\vert\]
alors $\lambda$ est ce que l'on appelle un coefficient de Lyapunov. Cette étude ne permet d'extraire qu'un seul coefficient (celui avec la partie réelle la plus grande).\\[.2cm]

Pour un système tel que le notre, c'est à dire de la forme $\dot{U} = F(U)$, on peut extraire tous les coefficients de Lyapunov à l'aide de la matrice Jacobienne de F. Pour ce faire, il suffit de trouver les valeurs propres de la matrice :

\[\Lambda = \int_0^t \mathit{J}_F(U_0(s))\mathrm{d}s\]
où $\mathit{J}_F$ désigne la matrice Jacobienne de $F$.

Pour s'en convaincre il suffit d'écrire :

\[\left.
\begin{array}{r}
\displaystyle U(t) =  U(0) + \int_0^t F(U(s))\mathrm{d}s\\
	\displaystyle U_0(t) =  U_0(0) + \int_0^t F(U_0(s))\mathrm{d}s
\end{array}
	\right\rbrace	\displaystyle \Rightarrow \delta U(t) \approx \delta U_0 + \int_0^t \mathit{J}_F(U_0(s))\mathrm{d}s
\]

Il suffit ensuite de remarquer que l'en posant $M$ comme étant l'intégrale, on a :

\[\delta U(t) = \exp(M)\delta U_0\]
en notant $\alpha_i$ ses valeurs propres, les coefficients de Lyapunov sont $\lambda_i = \lim_{t\rightarrow\infty} \alpha_i / t$\cite{Lyap}.

\newpage

On représente l'évolution des coefficients de Lyapunov en fonction du temps pour la condition initiale suivante : $\begin{bmatrix}0 & 1 & 0\end{bmatrix}^\top$. Un moyen de mesurer la fiabilité de la méthode est de s'assurer que la somme des coefficients donne la trace de la Jacobienne (pour le cas $r=28$, $\sigma=10$ et $b=8/3$, on doit trouver -13,667).

\input{tikz/II/lyap1}

On observe une convergence des trois coefficients et celui de plus grande valeur vaut environ 0.9 ce qui correspond à la valeur attendue. On remarque que la somme des coefficients respecte aussi la valeur attendue à l'ordre de $10^{-10}$ près. Ces trois valeurs se retrouvent souvent dans la littérature \cite{LyapVal}\\[.2cm]

\newpage

On fait de même avec la condition initiale $\begin{bmatrix}0 & 0 & 1\end{bmatrix}^\top$ qui converge vers 0 et amplifie plus les instabilités.

\input{tikz/II/lyap2}

On constate que l'on trouve une valeur supérieur à celle attendue, en revanche on a une amplification plus rapide que pour le cas précédent. On vérifie toujours que la somme des coefficients vaut environ -13,667.

\newpage

\subsection{Chaos}
\label{ssec:chaos}

Nous décidons de réaliser une dernière simulation numérique nous permettant de témoigner du caractère chaotique de l'attracteur de Lorenz. Pour se faire, nous prenons deux conditions initiales sensiblement proches et on trace la différence entre chaque composante des deux trajectoires obtenues.

\input{tikz/II/chaos}

Les deux conditions initiales sont : $a_1=\begin{bmatrix}
	0 & 1 & 0
\end{bmatrix}^\top$ et $a_2=\begin{bmatrix}
	0 & 1 + 10^{-6} & 1
\end{bmatrix}^\top$. La condition initiale choisie est celle introduite par Lorenz, on a vu que le choix de la composante perturbée n'avait que peu d'impact sur la croissance de l'erreur. Ce comportement montre que le système est extrêmement sensible aux conditions initiales. En revanche on constate que la déviation significative a lieu environ dix unités de temps après le début de la simulation ce qui permettrait de faire un correction en temps réel, notamment si l'on veut prédire le comportement d'un système instable dans le futur proche. 

Les phénomènes de saturation obtenus en \textsc{Figure} \ref{fig:err} pourrait être expliqués à partir du comportement chaotique. En effet la moindre erreur de codage des flottants entrainera des erreurs significatives à partir d'un certain moment. Le système se comporte comme un amplificateur d'erreurs. Si les erreurs numériques sont trop grandes, alors la solution calculée n'est pas correcte.