\documentclass[12pt]{article}

\input{preamble}

\begin{document}

\renewcommand\thepage{\alph{page}}
\pagedegarde

\tableofcontents
\setcounter{page}{2}
\thispagestyle{empty}
\pagebreak
\renewcommand\thepage{\arabic{page}}
\setcounter{page}{1}
\renewcommand\thefigure{{\thesection}.\arabic{figure}}
\renewcommand\thealgorithm{{\thesection}.\arabic{algorithm}}

\newpage
\section{Contexte et problématique}
\setcounter{figure}{0}
\setcounter{algorithm}{0}

La recherche en mécanique des fluides s'appuie très largement sur des systèmes d'équations aux dérivées partielles, dont la résolution numérique permet à la fois de mieux comprendre les phénomènes physiques et d'en affiner les modèles, en comparaison à des solutions théoriques et des données de mesures expérimentales. Pour autant, du fait de la nature nonlinéaire et instationnaire des phénomènes étudiés, les prédictions peuvent être amenées à s'écarter des observations sur des temps longs à partir d'un état initial donné. 
Ce problème est typiquement rencontré dans la modélisation du climat et des écoulements de géophysique, qui, depuis les travaux pionniers de Rayleigh en 1922 \cite{R22}, ont développé des techniques d'assimilation de données afin de pouvoir réajuster les modèles en fonction des observations. Ces méthodes consistent à calculer l'état initial optimal permettant de minimiser l'écart entre les solutions numériques et les données expérimentales dans une fenêtre temporelle donnée. Elles ont permis d'améliorer considérablement la qualité des prédictions météorologiques, avec l'accroissement de la résolution des modèles et l'augmentation du volume des données disponibles. 

L'assimilation de données fait partie des méthodes d'apprentissage automatique, et peuvent être vues dans le cadre plus vaste des méthodes d'intelligence artificielle, qui connaissent actuellement un engouement fort. Elles s'étendent en effet vers toutes les disciplines scientifiques, ce qui pose la question de l'impact de ces méthodes sur la précision des prédictions, en particulier dans des situations pour lesquelles ces algorithmes n'ont pas été initialement développés. 

La gestion des incertitudes est au cœur de l'assimilation des données, puisqu'elles composent avec une connaissance imparfaite des conditions initiales et une représentation nécessairement approchée de la réalité par un modèle des phénomènes physiques étudiés, auxquels s'ajoute la qualité de la résolution numérique des équations associées. L'un des algorithmes les plus utilisés est celui des filtres récursifs de Kalman \cite{K60} \cite{KB61}, qui réévalue l'état du système à chaque observation. Cette approche permet une reconstruction exacte pour des systèmes linéaires soumis à un bruit Gaussien \cite{H90}. Cette approche Bayésienne rigoureuse devient cependant difficilement applicable lorsque la dimension du système dynamique étudié croît, car il devient impossible, même avec les calculateurs actuels de construire les matrices de corrélation associées. En météorologie, l'ordre de grandeur du problème de de l'ordre de $\mathcal{O}$($10^9$), ce qui fait qu'un grand nombre d'études sont consacrées à l'extension de ces algorithmes dans le cas de grandes dimensions (voir e.g. \cite{L12} \cite{L18}). Les méthodes les plus populaires sont des méthodes variationnelles de type 3D-VAR ou 4D-VAR (\cite{S55},\cite{S58}), qui traitent le problème globalement sous la forme d'une minimisation d'une fonctionnelle, ce qui les rend proches des algorithmes d'optimisation.

\newpage

Plusieurs questions restent encore à explorer, en particulier le comportement de ces méthodes lorsque le système est fortement nonlinéaires et que les incertitudes s'écartent de la Gaussianité (\cite{T05}, \cite{VL11}). 
Des comparaisons peuvent être faites entre les méthodes 4D-VAR et des filtres de Kalman d'ensemble, notamment dans le cas de systèmes de petite dimension \cite{H17}, comme le système chaotique de Lorenz \cite{L63}, très largement utilisé comme jeu d'équations modèle en assimilation de données. Cependant l'évaluation de ces méthodes, en terme d'impact sur la précision numérique de la représentation est rarement abordée \cite{Li18}. 


C'est l'objet de ce travail de PAr que d'aller caractériser l'impact du caractère nonlinéaire et non Gaussion des équations du modèles sur la précision de la prédiction. Pour cela, le modèle choisi est celui du système chaotique de Lorenz \cite{L63}, donnant une représentation simplifiée du phénomène de convection thermique dans une couche limite atmosphérique.
Ces équations sont très simples à intégrer numériquement, et possèdent des solutions quasi-périodiques sujettes à de brusques changements lorsqu'elles sont intégrées sur des temps longs. Ses travaux ont initié de nombreuses études sur les systèmes chaotiques et popularisé l'effet papillon.

Dans une première partie, l'établissement des équations de Lorenz est ré-exposé \cite{coursfaure} \cite{courslaurette}, afin de décrire les caractéristiques du système, connaître la signification et les plages d'existence de ses paramètres et retrouver les principaux modes d'instabilité linéaire. Ces équations sont ensuite résolues numériquement pour spécifier l'impact des approximations numériques des schémas d'intégration temporelle sur la précision des prédictions, en particulier dans le régime chaotique. La propagation des erreurs est étudiée d'une part à partir de perturbations des conditions initiales et d'autre part en introduisant un forçage stochastique sur le système des équations. On analyse en particulier comment et pourquoi les erreurs s'écartent de la Gaussianité. Enfin, deux méthodes d'assimilation de données sont utilisées, en générant des jeux de données expérimentaux à partir de perturbation des solutions numériques. La précision des prédictions est de nouveau analysée en fonction des paramètres des algorithmes (fréquence de recalage sur les observations, nature du bruit, etc.). 

\newpage
\section{Le système de Lorenz et étude mathématiques}
\setcounter{figure}{0}
\setcounter{algorithm}{0}

Cette section va nous permettre d'obtenir les équations de Lorenz telles qu'il les a obtenues en 1963. Nous allons rappeler le problème tel qu'il a été posé en partant du modèle de Rayleigh, et rappeler les hypothèses émises pour obtenir ces équations. Une étude mathématiques nous permettra de connaître les équilibre du système, et leur nature, en fonction des paramètres physiques qui décrivent le système.

\input{partI}

\subsection*{Bilan}

Cette section nous a permis de comprendre ce qui se cache derrière les équations de Lorenz et la nature du problème. Ce système décrit de manière simplifier certains comportements météorologiques, l'objectif est alors de pouvoir comprendre et prédire l'état du système. En revanche, l'étude mathématique menée sur les équilibres du système avec des paramètres physiques réalistes nous a montré un système n'ayant pas d'équilibres stables. 

\newpage
\section{Simulations numérique et erreurs de modèle}
\setcounter{figure}{0}
\setcounter{algorithm}{0}

Cette section s'intéresse à la résolution numérique des équations de Lorenz (\ref{eq:lorenz}). Plusieurs modèles mathématiques existent, nous nous pencherons donc vers la précision de ces modèles pour résoudre numériquement notre problème, dont on ne connaît pas les solutions analytiques. Nous verrons aussi que, dans certains cas, ces méthodes ont leurs limites à cause de la nature même du système d'étude.

\input{partII}

\subsection*{Bilan}

Cette section nous a permis d'introduire plusieurs modèles mathématiques permettant la résolution numérique du problème. Ces méthodes ont aussi mis en avant la nature chaotique du système pour des paramètres physiques réalistes. Ce système chaotique va se comporter tel un amplificateur d'erreur, ce qui a été illustré avec l'existence d'un coefficient de Lyapunov strictement positif. 

\newpage
\section{Perturbation du système}
\setcounter{figure}{0}
\setcounter{algorithm}{0}

Cette section a pour but de caractériser l'influence de le caractère gaussien du bruit sur un système non-linéaire : le système de Lorenz. En effet dans la plupart des documents de recherche sur les méthodes d'assimilation de données, on fait l'hypothèse que toutes les perturbations extérieures sont gaussiennes. Avant de connaître les effets de la non gaussianité du bruit sur les méthodes d'assimilation de données, il faut connaître l'influence de ces dernières sur le système lui même.

\input{partIII}

\subsection*{Bilan}

\emph{résultats en attente}

\newpage
\section{Assimilation de données et correction}
\setcounter{figure}{0}
\setcounter{algorithm}{0}

Cette section va nous permettre d'introduire les différentes méthodes d'assimilation de données et de comprendre leur fonctionnement. Nous présenterons le procédé de façon théorique avant de proposer un procédé numérique nous permettant d'utiliser ces méthodes sur notre système. Nous utiliserons une méthode variationnelle (le 4D-VAR) ainsi que des filtres de Kalman afin de comparer ces deux méthodes et de déterminer leurs limites dans le cadre du système de Lorenz.

\input{partIV}

\subsection*{Bilan}

\emph{résultats en attente}

\newpage
\section*{Perspectives}

À l'heure actuelle, nous disposons d'une étude mathématiques des équilibres du système de Lorenz, d'une méthode de résolution numérique, dont on connaît les limites. Nous sommes aussi en mesure de créer n'importe quelle variable aléatoire dont la densité de probabilité est connue. Quelques méthodes d'assimilation de données ont été implémentées telles que le 3D-VAR, le 4D-VAR et un filtre de Kalman.

Il reste néanmoins à mener toutes les études sur l'influence du caractère nonlinéaire et non Gaussien des équations et du modèle sur la précision de la prédiction. Cette démarche nécessitera de nombreuses simulations numériques afin que l'on comprenne comment et pourquoi les erreurs s'éloignent de la Gaussianité, et que l'on détermine l'influence des paramètres algorithmiques sur les méthodes d'assimilation de données pour pouvoir comparer deux de ces méthodes et déterminer leurs limites. À la vue de tous les paramètres que l'on peut étudier, l'usage d'un calculateur sera probablement nécessaire.

\newpage

\appendix
\renewcommand\thepage{\roman{page}}
\setcounter{page}{1}
\renewcommand\thefigure{{\thesection}.\arabic{figure}}

\section{Gantts}
\setcounter{figure}{0}

\begin{figure}[!h]
	\centering
	\includegraphics[width=.8\textwidth]{ganttn.pdf}
	\caption{Gantt du RGP1 - 02/11/2020}
\end{figure}

Le scénario 1 correspond à une amélioration de la condition sanitaire. Le scénario 2 correspond à un nouveau confinement. Le scénario 1 n'aura pas lieu.

\begin{figure}[!h]
	\centering
	\includegraphics[width=.8\textwidth]{ganttj.pdf}
	\caption{Gantt du RGP2 - 22/01/2021}
\end{figure}

Les \emph{Activités} du scénario 1 ne sont donc plus présentes. Du retard est apparu dans plusieurs activités, cela est notamment dû à un besoin d'écrire plus de contenu et l'étude des méthodes variationnelles a pris plus de temps que prévu. Enfin la préparation aux partiels n'a pas été pris en compte lors de l'élaboration du Gantt du mois de novembre.

\newpage

\section{Budget}
\setcounter{figure}{0}

\subsection{Coût matériel}

\begin{center}
	\begin{tabular}{|l||l|}\hline
		\textbf{Matériel} & \textbf{Estimation}\\ \hline \hline
		\sout{\textit{Expérience}} & \sout{300\,\euro} \\ \hline
		\textit{Calculateur du laboratoire} & Coût horaire \\ \hline
		\sout{\textit{Publication d'un article}} & \sout{500\,\euro  }\\ \hline
		Ordinateur personnel & 0\,\euro \\ \hline
		Bibliographie & Fourni par l'école \\ \hline \hline
		Total & 0 - 800\,\euro\\ \hline
	\end{tabular}
\end{center}

Après le RGP1 de début novembre, nous avions conclu avec les tuteurs et le conseiller projet que, s'il y a publication, la publication se fera de manière gratuite. Le scénario 1 n'ayant pas lieu, il n'y aura pas d'expériences.

\subsection{Coût humain}

\begin{center}
	\begin{tabular}{|l||l|}\hline
		\textbf{Personnel} & \textbf{Estimation} \\ \hline \hline
		Les tuteurs & 4 heures/semaine\\ \hline
		Conseiller projet & 1 heure/semaine\\ \hline
		Étudiant & 6 heures/semaines\\ \hline \hline
		Total & 300 heures\\ \hline
	\end{tabular}
\end{center}

\section{Fiche de lancement de projet}

\hspace{-.425cm}\includegraphics[width=1.05\textwidth]{fiche.pdf}

\nocite{*}
\bibliographystyle{plain}
\bibliography{bib}

\newpage
\small{
\listoffigures
\listofalgorithms
}
\end{document}