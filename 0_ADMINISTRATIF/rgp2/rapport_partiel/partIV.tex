\subsection{Méthodes variationnelles}

Le but des méthodes variationnelles va être de construire un ensemble de conditions initiales tel que les simulations obtenues à partir de ces conditions initiales se rapprochent au mieux de ce qui a été réellement mesuré, tout en accordant plus ou moins d'importance à la prédiction et à la mesure en fonction de notre confiance relative sur ces des sources d'information. Cette recherche d'ensemble passe par la minimisation d'une fonction coût qui sera introduite dans la suite, après que toutes les notations aient été présentées.

\subsubsection{Notations et principe des méthodes}

Dans cette partie, on note :\\[.1cm]

\hspace*{-1.5cm}{\begin{tabular}{rl}
	$X(t)$ & L'état du système réel à l'instant $t$, il s'agit d'un vecteur de dimension $d$.\\[.1cm]
	$\tilde{X}(t)$ & Une perturbation du système à l'instant $t$ autour de $X(t)$.\\[.1cm]
	$T$ & L'ensemble des instants pour lesquels le système est mesuré.\\[.1cm]
	$n$ & Le nombre de mesures effectuées.\\[.1cm]
	$X_i$ & L'état réel du système à l'instant $t_i$, le $i^\text{ème}$ élément de $T$.\\[.1cm]
	$Y_i$ & L'état du système mesuré à l'instant $t_i$.\\[.1cm]
	$\mathcal{F}$ & La fonction qui gouverne l'évolution de notre système : $\frac{\mathrm{d}X}{\mathrm{d}t} = \mathcal{F}(t, X)$\\[.1cm]
	$F_{(t,\,X)}$ & La matrice obtenue par linéarisation (dans l'espace) de $\mathcal{F}$ en $X$ à l'instant $t$.\\[.1cm]
	$\mathcal{M}_{i,\, j}$ & L'opérateur qui à un état $X(t_i)$ associe l'état $X(t_j)$.\\[.1cm]
	$M_{i,\, j}$ & L'opérateur qui à un état $X_i$ et sa perturbation $\tilde{X}_i$ associe la perturbation $\tilde{X}_j$.\\[.1cm]
	$\mathcal{H}$ & L'opérateur de mesure, pas toujours linéaire.\\[.1cm]
	$H$ & La matrice obtenue par linéarisation de $\mathcal{H}$ en $X$. Si $\mathcal{H}$ est linéaire, alors $\mathcal{H} = H_XX$.\\[.1cm]
	$B$ & La matrice de covariance de prévision.\\[.1cm]
	$R$ & La matrice de covariance d'observation.
\end{tabular}}

\newpage

On suppose que l'on possède toutes les informations ci-dessus, en dehors de l'état réel du système. Le but des méthodes variationnelles va être de construire un ensemble de conditions initiales tel que les simulations obtenues à partir de ces conditions se rapprochent au plus de ce qui a été réellement mesuré. On introduit donc une fonction de coût qui prend l'ensemble de ces conditions initiales mesurer l'erreur entre les mesures et les résultats numériques trouvés à partir de ces conditions initiales. On prend en compte la confiance relative sur nos sources d'information à l'aide de matrices de covariance.
\vspace{-.2cm}
\[J((X_i)_i) = \dfrac{1}{2}[ X_1 - X_g ] ^\top B^{-1} [ X_1 - X_g ] + \dfrac{1}{2} \sum_{i=1}^n [ \mathcal{H}(X_i) - Y_i ]^\top R_i^{-1} [ \mathcal{H}(X_i) - Y_i ]\]
où $X_g$ est une supposition de la condition initiale.

Le problème réside dans la formulation des $(X_i)_i$. On peut choisir de les laisser suffisamment libres pour simplifier le problème, ou d'imposer la continuité de la solution proposée, c'est à dire  $\forall i,\, X_{i+1} = \mathcal{M}_{i,\,i+1}(X_i)$. Ces méthodes décrivent respectivement le 3D-VAR et le 4D-VAR, qui seront détaillées dans la suite. Il faut tout d'abord que l'on soit en mesure de réaliser de l'optimisation de façon numérique.

\newpage

La minimisation de $J$ se fait de manière numérique. Ici on choisit d'utiliser la méthode de Newton pour trouver le minimum sur l'espace $\mathbb{R}^d$. On se place dans le cas où seul $X_1$ est à choisir. La méthode est la suivante. On définit une suite $\left(X_1^{(i)}\right)_i$ de vecteur tels que :

\begin{algorithm}
	\caption{Méthode de quasi-Newton}
	\algsetup{indent=3em}
	\begin{algorithmic}
		\REQUIRE $J$, $X_1$, $\varepsilon$
		\ENSURE Valeur la plus probable pour $X_1$
		\STATE $\left\Vert \overrightarrow{\nabla}J\right\Vert \leftarrow 2\varepsilon$
		\WHILE{$\left\Vert \overrightarrow{\nabla}J\right\Vert\geq \varepsilon$}
		\STATE $X_1 \leftarrow {X_1} - \left[\mathbf{H}_J\left({X_1}\right)\right]^{-1} \overrightarrow{\nabla} J\left({X_1}\right)$
		\STATE $\left\Vert \overrightarrow{\nabla}J\right\Vert \leftarrow \left\Vert \overrightarrow{\nabla} J\left({X_1}\right) \right\Vert$
		\ENDWHILE
		\RETURN $X_1$
	\end{algorithmic}
\end{algorithm}

\emph{Remarque :} La méthode est dite de quasi-Newton, car celle-ci est une recherche de minimum, via une recherche d'annulation du gradient par la méthode de Newton.\\[.2cm]

Cette méthode est une variante de la méthode de descente du gradient. L'utilisation de la Hessienne de $J$, $\mathbf{H}_J$, nous permet de converger le plus rapidement possible sans faire exploser le système. 

On rappelle que : 
\[\overrightarrow{\nabla} J = \begin{bmatrix}
\dfrac{\partial J}{\partial x_1}\\
\vdots \\
\dfrac{\partial J}{\partial x_d}
\end{bmatrix} \text{  et que  } \mathbf{H}_J = \begin{bmatrix}
\dfrac{\partial^2 J}{\partial x_1^2} & \dfrac{\partial^2 J}{\partial x_2\partial x_1} & \dots & \dfrac{\partial^2 J}{\partial x_d\partial x_1} \\[.5cm]
\dfrac{\partial^2 J}{\partial x_1\partial x_2} & \dfrac{\partial^2 J}{\partial x_2^2} & \dots & \dfrac{\partial^2 J}{\partial x_d\partial x_2} \\[.5cm]
\vdots & \vdots & \ddots & \vdots \\[.2cm]
\dfrac{\partial^2 J}{\partial x_1\partial x_d} & \dfrac{\partial^2 J}{\partial x_2\partial x_d} & \dots & \dfrac{\partial^2 J}{\partial x_d^2} \\
\end{bmatrix}\]
où $x_i = [\delta_{ij}]_j$ est un vecteur colonne de la base naturelle de $\mathbb{R}^d$.

\newpage On peut remarquer que $J$ est de la forme d'une somme de termes ayant pour forme  \[\dfrac{1}{2}[ g(\cdot) - b ] ^\top A [ g(\cdot) - b] = j\]

Procédons au calcul de $\overrightarrow{\nabla}j$ et $\mathbf{H}_j$. On note que $A$ est symétrique (matrice de covariance, opérateur bilinéaire symétrique).

\begin{align*}
\overrightarrow{\nabla} j_i = \dfrac{\partial j}{\partial x_i} &= \dfrac{1}{2}\dfrac{\partial}{\partial x_i} \sum_{k=1}^d \sum_{l=1}^d [g_k(\cdot) - b_k] A_{kl}  [g_l(\cdot) - b_l]\\
&= \dfrac{1}{2} \sum_{k=1}^d \sum_{l=1}^d \left[\dfrac{\partial g_k}{\partial x_i}(\cdot) A_{kl}  [g_l(\cdot) - b_l] + [g_k(\cdot) - b_k](\cdot) A_{kl}  \dfrac{\partial g_k}{\partial x_i}(\cdot)\right]\\
&= \sum_{k=1}^d \sum_{l=1}^d \dfrac{\partial g_k}{\partial x_i}(\cdot) A_{kl}  [g_l(\cdot) - b_l]\\
&= \left(\left[\mathbf{\nabla} g\right]^\top A [g(\cdot) - b]\right)_i
\end{align*}

Ainsi on peut établir $\overrightarrow{\nabla} j = \left[\mathbf{\nabla} g\right]^\top A [g(\cdot) - b]$.

De même, on procède au calcul de la hessienne de $j$.

\begin{align*}
	\left(\mathbf{H}_j\right) _{i_0,\,i_1} = \dfrac{\partial^2 j}{\partial x_{i_0} \partial x_{i_1}} &= \dfrac{\partial}{\partial x_{i_0}} \sum_{k=1}^d \sum_{l=1}^d \dfrac{\partial g_k}{\partial x_{i_1}} A_{kl}  [g_l(\cdot) - b_l]\\
	&= \sum_{k=1}^d \sum_{l=1}^d \left[\dfrac{\partial g_k}{\partial x_{i_1}} A_{kl}  \dfrac{\partial g_l}{\partial x_{i_0}} \dfrac{\partial}{\partial x_{i_0}} + \dfrac{\partial^2 g_k}{\partial x_{i_0}\partial x_{i_1}} A_{kl}  [g_l(\cdot) - b_l]\right]\\
	&= \left(\left[\nabla g\right]^\top A \left[\nabla g\right] + \mathbf{H}_gA[g(\cdot) - b]\right)_{i_0,\,i_1}
\end{align*}

On note que si $g$ est linéaire, le terme $\mathbf{H}_g$ n'apparait plus.\\[.2cm]

Dans la suite, on supposera bien souvent que $g$ est linéaire, ainsi l'obtention de la hessienne de $J$ est une Lapalissade et toute la difficulté du problème réside dans l'obtention du gradient de $J$.

\newpage

\subsubsection{Méthode 3D-VAR}

La méthode 3D-VAR consiste à calculer une nouvelle condition initiale à chaque actualisation des données extérieures. On ne cherche pas une solution qui soit globalement bonne, mais la plus optimale pour un instant donné. L'historique n'est pas pris en compte pour cette méthode. Dans le cas où toutes les mesures sont parfaites cette méthode se révèlera très efficace, car on affinera de plus en plus vers le vrai état du système, alors que si les mesures ne sont pas exactes, on va chercher à se rapprocher de ces erreurs, bien que l'historique nous indiquerait des erreurs dans les données extérieures. 

On construit par récurrence la famille des $(X_i)_i$ de la façon suivante :

\begin{algorithm}
	\caption{Méthode 3D-VAR}
	\algsetup{indent=3em}
	\begin{algorithmic}
		\REQUIRE $J$, $X_1$, $X_g$, $(Y_i)_i$
		\ENSURE $(X_i)_i$ trouvée par la méthode 3D-VAR
		\STATE $X_i \leftarrow X_1$
		\FOR{$i = 2..d$}
		\STATE $X_i \leftarrow \underset{X\in\mathbb{R}^d}{\mathrm{argmin}}\left(J_i(X, X_g, Y_i)\right)$
		\STATE $X_g \leftarrow \mathcal{M}_{i,\,i+1}(X_i)$
		\ENDFOR
		\RETURN $(X_i)_i$
	\end{algorithmic}
\end{algorithm}

où $\displaystyle J_i =\dfrac{1}{2}[ X - X_{g,\,i} ] ^\top B^{-1} [ X - X_{g,\,i} ] + \dfrac{1}{2} [ \mathcal{H}(X) - Y_i ]^\top R_i^{-1} [ \mathcal{H}(X) - Y_i ]$ et argmin peut être vu comme la fonction décrite par l'algorithme de quasi-Newton.\\[.2cm]
 
L'avantage de cette méthode est la facilité de calcul du gradient de $J_i$ pour $i\in\left\lbrace 1,\,2,\,\dots,\,d\right\rbrace$. En effet, la fonction $g_i$ ici est toujours la même et vaut $\mathcal{H}$ qui, par approximation de Taylor s'écrit $\mathcal{H}(X) + \mathrm{d}X) = \mathcal{H}(X) + H_X\mathrm{d}X$. Ainsi :

\[\overrightarrow{\nabla}J_i = B^{-1} [ X - X_{g,\,i} ] + {H_X} ^\top R_i^{-1} [ \mathcal{H}(X) - Y_i ]\]
\[\mathbf{H}_{J_i} = B^{-1} + {H_X} ^\top R_i^{-1} H_X\]

En revanche cette méthode n'a pas d'effet de correction antérieure : un $X_i$ qui sort de argmin ne changera plus.

\newpage

On décide de réaliser un essai pour une particule partant de $\begin{bmatrix}
0 & 1 & 0
\end{bmatrix}^\top$. On réalise une mesure avec un pas de temps valant 1 pendant 10 unités de temps (soit 9 mesures, on ne connait pas l'instant 0). On suppose la mesure parfaite et initialement on a supposé que la particule était en \textbf{0}.

\input{tikz/IV/3d_var}

Sur le graphe ci-dessus, on constate que à chaque fois que l'on a accès à une mesure, on a l'apparition d'une discontinuité dans la solution calculée afin de corriger au mieux la position de la particule. On constate que rapidement on se rapproche de la particule. 

\newpage

On trace la distance qui sépare les deux particules dans l'espace des phases au cours du temps. Une fois l'état de la particule corrigé à l'instant $t=10$, on arrête de la corriger et on suit la distance qui sépare les deux particules pendant 10 unités ed temps

\input{tikz/IV/err3dvar}

La supposition initiale n'était pas la bonne, c'est pourquoi les deux particules deviennent très distantes, mais à chaque mesure l'erreur chute, puis augment à nouveau (puisque la correction n'était pas parfaite), mais celle ci tant à disparaître quand on a plus de mesures. Si le système est relaxé, les particules s'éloignent à nouveau à cause du comportement chaotique du système. On peut imaginer que plus on fera des mesures souvent, plus la correction sera précise. \emph{(résultats en attente)}

\newpage

On peut néanmoins étudier sa précision dans le cas où la mesure n'est pas parfaite. On propose de réaliser une simulation avec un bruit gaussien de variance 1 selon la composante $X$.

\input{tikz/IV/3d_var_pert}

On constate qu'il y a des erreurs de mesures qui semblent favorables à la méthode. En effet à l'instant $t=3$ la mesure selon $X$ est sur-estimée, or notre l'état de la particule sous-estime sa composante selon $X$, ainsi la correction sera plus importante et nous permet de nous approcher de la vérité plus vite que si la mesure avait été exacte. On a aussi des mesures qui ne sont pas favorables à la méthode comme celle qui a lieu à l'instant $t=7$. \emph{(résultats en attente)}

\newpage

\subsubsection{Méthode 4D-VAR}

La méthode 4D-VAR consiste à trouver la trajectoire la plus optimale dans son ensemble, de cette manière, on a une vision d'ensemble meilleure sur l'historique des mesures. En revanche cette méthode s'avère plus coûteuse en calculs que la méthode 3D-VAR. On ne recherche que $X_1$, la condition initiale, de telle manière que la fonction coût soit minimale :

\[J(X) = \dfrac{1}{2}[ X - X_g ] ^\top B^{-1} [ X - X_g ] + \dfrac{1}{2} \sum_{i=1}^n [ \mathcal{H}\left(\mathcal{M}_{1,\,i}(X)\right) - Y_i ]^\top R_i^{-1} [ \mathcal{H}\left(\mathcal{M}_{1,\,i}(X)\right) - Y_i ]\]

Avec cette formulation, on se rend compte que sans hypothèses de linéarisation, on doit calculer les $X_i$ avec le modèle $\mathcal{M}$. En revanche, utiliser le modèle est très couteux en terme de temps de calcul. On se propose donc de prendre une condition initiale $X_1$ et de calculer une fois les $X_i$ avec le modèle. Si $X_1$ est suffisamment correcte, on pourra ajuster les $X_i$ en appliquant une perturbation $\tilde{X}_1$ autour de $X_1$ et la reporter aux $X_i$ en utilisant $M_{1,\,i}(X_1)\tilde{X}_1$, soit $X_i + \tilde{X}_i = \mathcal{M}_{1,\,i}(X_1) + M_{1,\,i}(X_1)\tilde{X}_1$.

\begin{algorithm}
	\caption{Méthode 4D-VAR}
	\algsetup{indent=3em}
	\begin{algorithmic}
		\REQUIRE $J$, $X_1$, $X_g$, $X_i = \mathcal{M}_{1,\,i}(X_1)$, $(Y_i)_i$, $\varepsilon$
		\ENSURE $X_1$ trouvé par la méthode 4D-VAR
		\STATE err $\leftarrow 2\varepsilon$
		\WHILE{err $\geq \varepsilon$}
		\STATE $X \leftarrow X_1 - \left[\mathbf{H}_J\left({X_1}\right)\right]^{-1} \overrightarrow{\nabla} J\left({X_1}\right)$
		\STATE $\mathrm{d}X \leftarrow X - X_1$
		\FOR{$i=2..d$}
		\STATE $X_i \leftarrow X_i + M_{1,i}(X_1)\mathrm{d}X$
		\ENDFOR
		\STATE $X_1 \leftarrow X$
		\STATE err $\leftarrow J(X_i, Y_i, X_g)$
		\ENDWHILE
		\RETURN $X_1$
	\end{algorithmic}
\end{algorithm}

Ici, on a : \[\displaystyle \overrightarrow{\nabla} J\left({X}\right) = B^{-1} [ X - X_g ] + \sum_{i=1}^n  M_{1,\,i}(X_1)^\top H_X^\top R_i^{-1} [ \mathcal{H}\left(\mathcal{M}_{1,\,i}(X)\right) - Y_i ]\]
\[\mathbf{H}_{J_i} = B^{-1} + \sum_{i=1}^d M_{1,\,i}(X_1)^\top {H_X} ^\top R_i^{-1} H_X M_{1,\,i}(X_1)\]

\newpage

On procède à une étude similaire que pour la méthode 3D-VAR. On réalise un essai pour une particule partant de $\begin{bmatrix}0 & 1 & 0\end{bmatrix}^\top$. On réalise une mesure toutes les unités de temps jusqu'à $t=10$ (soit 9 mesures, sans prendre en compte l'instant initial). On suppose la mesure parfaite. \emph{(résultats en attente)}

\newpage

\subsection{Méthodes séquentielles}

Le principe des méthode séquentielles va être d'estimer l'état actuel en deux temps. Dans un premier temps, on cherche à prédire l'état à partir de l'état que l'on avait déterminé précédemment, à partir du modèle physique. Ensuite, cette estimation sera corrigée à partir des observation actuelles. Initialement cette famille de méthode fonctionne sur des modèles linéaires (de cette manière tous les calculs sont matriciels), mais elles peuvent êtres étendus à des systèmes non linéaires tels que le système de Lorenz. \\[1em]

Dans la suite, on note :\\[.1cm]

\begin{tabular}{rl}
	${x_i}^f$ & L'état prédit (\emph{forecast}) à l'instant $i$\\
	${x_i}^a$ & L'état corrigé (\emph{analyzed}) à l'instant $i$\\
	$y_i$ & L'état mesuré à l'instant $i$\\
	$M_i$ & L'opérateur qui modélise le système. Il associe ${x_i}^a$ à ${x_{i+1}}^f$\\
	$H$ &  L'opérateur de mesure de l'état\\
	$v_i$ & L'erreur associée au modèle à l'instant $i$\\
	$e_i$ & L'erreur associée à la mesure à l'instant $i$\\
	${P_i}^f$ & La matrice de covariance des erreurs de l'état prédit\\
	${P_i}^a$ & La matrice de covariance des erreurs de l'état corrigé\\
	$Q$ & La matrice de covariance des erreurs de modèles\\
	${R_i}$ & La matrice de covariance des erreurs de mesure
\end{tabular}

\vspace{1em}

Le calcul des nouveaux états suit donc une opération de la forme.
\[
\left\lbrace\begin{array}{l}
	{x_{k+1}}^f = M_{k+1}({x_k}^a) + v_{k+1}\\
	y_k = H({x_k}^f) + e_k
\end{array}
\right.
\]

\vspace{1em}

Les filtres de Kalman proposent d'introduire le gain de Kalman afin d'ajuster la confiance que l'on porte à la prédiction et à la mesure. Dans la suite on va présenter la premier version formulée du filtre de Kalman et aborder sa version étendue aux systèmes non linéaires afin que l'on puisse l'appliquer. Enfin nous étudierons les filtres d'ensemble de Kalman qui sera utile pour réaliser une étude particulaire du système de Lorenz.

\newpage

\subsubsection{Filtre de Kalman}

On introduit le gain de Kalman $K$ tel que :

\[K_i = \frac{{P_i}^fH^\top}{H{P_i}^fH^\top+R_i}\]

Cette matrice va nous permettre de pondérer la confiance que l'on porte à nos deux sources d'information à savoir la prédiction à partir d'un état antérieur et le modèle physique, ainsi que la mesure réelle effectuée. L'état corrigé s'écrit donc ainsi :

\[{x_i}^a={x_i}^f + K_i (y_i - H({x_i}^f))\]

On peut remarquer que l'on a écrit un quotient de matrices ce qui peut paraître surprenant. Il s'agit toujours de matrices, mais la notion de fraction est simplement figurative. En effet, si l'on suppose que l'on a très confiance en notre modèle, alors la matrice ${P_i}^f$ est relativement faible, ainsi $K$ est relativement proche de 0. De cette manière, on a $x^a \approx x_f$ ce qui traduit le fait que l'on ait confiance en notre méthode de prédiction par la simple donnée du modèle. Si à l'inverse on fait principalement confiance à notre mesure, alors on a $K$ relativement proche de $H^{-1}$. Ainsi on se retrouve avec $x^a \approx H^{-1}(y)$ ce qui traduit la confiance que l'on porte à la mesure réelle qui a été effectuée.\\[1em]

Il est donc important de tenir à jour la matrice $P^f$ afin de ne pas erroner la méthode. On a les relations suivantes :

\[
\left\lbrace\begin{array}{l}
	{P_{k+1}}^f = M_{k+1}({x_k}^a){P_k}^aM_{k+1}({x_k}^a)^\top + Q\\
	{P_{k}}^a = {P_k}^f - {P_k}^f H^\top K_k^\top
\end{array}
\right.
\]

Enfin, pour étendre le filtre à des systèmes non linéaires, il suffit de linéariser le modèle en ${x_k}^a$ pour calculer ${x_{k+1}}^f$.

\newpage

\subsection{Comparaison entre les méthodes}

Maintenant que nous disposons de méthode d'assimilation de données il faut pouvoir les comparer entre elles. Pour se faire, on considère une particule qui est déjà dans l'attracteur et que l'on fait évoluer pendant une certaine durée $T$ (pas d'intégration $\mathrm{d}t$, $N\mathrm{d}t = T$). À intervalle de temps régulier $\tau$, on réalise la mesure de l'état en ajoutant une erreur de mesure dans les directions $X$, $Y$ et $Z$. L'erreur considérée est gaussienne de moyenne nulle et d'écart-type $\sigma_X$, $\sigma_Y$ et $\sigma_Z$. Une fois les données acquises, on les donne à nos méthodes d'assimilation de données. Ces méthodes nous donnent une trajectoire $U_m$ que l'on compare avec la vraie trajectoire $U$ en calculant l'erreur moyenne quadratique suivante :
\[\text{RMSE} = \frac{1}{N}\sqrt{\dfrac{\displaystyle\sum_{i=1}^N \Vert (U_m - U)(i\mathrm{d}t)\Vert^2}{3}}\]

À la vue du nombre de paramètres que l'on peut faire varier, on impose $\sigma_X=\sigma_Y=\sigma_Z = \sigma$, car les composantes ayant toutes une influence les unes sur les autres (comme cela a pu être vu en \ref{sssec:pert_ci}), on sait que les perturbations se propageront entre chaque composantes. On fixe $\tau$ à $k\mathrm{d}t$, et dans un premier temps, on cherche à connaître l'influence de $k$ sur la performance des méthodes. Le procédé ainsi énoncé est répété un nombre suffisant de fois pour différentes conditions initiales appartenant à l'attracteur afin de ne pas être biaisé par des conditions initiales qui seraient \emph{particulières}. On regarde dans un premier les influences de $\sigma$ et de $T$ sur les différentes méthodes. On choisit $\mathrm{d}t = 0.01$. On a définit quinze conditions initiales différentes qui appartiennent à l'attracteur.

\newpage

\input{tikz/IV/ct1s2}

On constate que l'erreur globale augmente à mesure que $k$ augmente et notamment pour la méthode 3D-VAR, en revanche ce graphe a été obtenu après plusieurs essais. En effet les méthodes 4D-VAR et de Kalman font intervenir des inversions de matrices, ainsi il existe des cas dans lesquels les matrices ne sont pas inversibles, ou dont les inversions donnent des résultats trop grands pour être obtenus. Il est notamment très difficile d'obtenir des résultats pour des valeurs de $k$ supérieurs à trente pour la méthode de Kalman. Les méthodes 4D-VAR et de Kalman semblent partager des résultats assez précis pour ces valeurs de $T$ et de $\sigma$.

\newpage

\input{tikz/IV/ct5s2}

Dans cette expérience on a augmenté le temps final de simulation à $T=5$. L'erreur sur la méthode 3D-VAR est similaire à la la simulation précédente, ce qui est pertinent puisque cette méthode est sans mémoire. En revanche pour les méthodes 4D-VAR et de Kalman, l'erreur est sensiblement plus importante que pour la simulation précédente, car les points calculés dépendent les uns des autres. Il est aussi plus difficile d'obtenir des résultats avec ces méthodes à cause des problèmes cités précédemment. De plus la méthode RMSE4 est moins performante pour des valeurs de $k$ faible.

\newpage

\input{tikz/IV/ct5s1}

Pour cette simulation on a augmenté l'écart type des erreurs de mesures pour avoir $\sigma=10^{-1}$ et on a fait durer la simulation pendant $T=5$. L'erreur quadratique moyenne de la méthode 4D-VAR a augmenté de manière significative même sur les petites valeurs de $k$, alors que la méthode de Kalman arrive à rester performante même dans ces conditions. En revanche avec le même $T$, pour $\sigma = 1$, ni la méthode 4D-VAR, ni le filtre de Kalman ne convergent. Alors que la méthode 3D-VAR donne toujours un résultat. En revanche si l'on prend deux particules de l'attracteur complètement décorrélées et que l'on calcule leur RMSE, on trouve une valeur d'environ 17, ce qui correspond presque à ce que donne la méthode 3D-VAR, ainsi celle-ci n'est performante que pour un nombre restreint de valeurs de $k$. La RMSE de la méthode 3D-VAR est plus faible que celle trouvé précédemment probablement parce que l'on replace correctement la particule proche de là où l'on effectue la mesure.\newpage

Afin de savoir à quoi on peut comparer les valeurs de $T$ mentionnées plus tôt, on décide de trouver un temps caractéristique intrinsèque à l'attracteur de Lorenz. Dans son article, Lorenz a étudier les relations entre les amplitudes des maximums de $Z$. On décide donc de choisir notre temps caractéristique de l'attracteur sur la périodicité de $Z$. On part d'une condition initiale appartenant à l'attracteur et on réalise une simulation suffisamment longue. On identifie à postériori tous les instants pour lesquels on a un maximum local de $Z$. On trace ensuite la distribution des intervalles de temps entre deux maximums consécutifs.
\newpage
\input{tikz/IV/ct1s0}
\pagebreak
\input{tikz/IV/ct1s1}
\pagebreak
\input{tikz/IV/ct1s2}
\pagebreak
\input{tikz/IV/ct2s0}
\pagebreak
\input{tikz/IV/ct2s1}
\pagebreak
\input{tikz/IV/ct2s2}
\pagebreak
\input{tikz/IV/ct5s0}
\pagebreak
\input{tikz/IV/ct5s1}
\pagebreak
\input{tikz/IV/ct5s2}

\begin{figure}

\end{figure}