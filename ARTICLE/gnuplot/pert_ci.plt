set terminal epslatex size 12cm,10cm standalone

set xlabel "$t$"

set xrange[0:1]
set yrange[-0.5:0.5]
set xtics(0,0.2,0.4,0.6,0.8,1)

set output "M.tex"
plot "data/pert_ci.txt" u 1:2 with line lw 2 lc rgb "#0000FF",\
	"data/pert_ci.txt" u 1:3 with line lw 2 lc rgb "#FF7F3F",\
	"data/pert_ci.txt" u 1:4 with line lw 2 lc rgb "#00FF00"
set output 

set yrange[0:7]

set output "V.tex"
plot "data/pert_ci.txt" u 1:5 with line lw 2 lc rgb "#0000FF",\
	"data/pert_ci.txt" u 1:6 with line lw 2 lc rgb "#FF7F3F",\
	"data/pert_ci.txt" u 1:7 with line lw 2 lc rgb "#00FF00"
set output 

set yrange[-3.5:3.5]

set output "S.tex"
plot "data/pert_ci.txt" u 1:8 with line lw 2 lc rgb "#0000FF",\
	"data/pert_ci.txt" u 1:9 with line lw 2 lc rgb "#FF7F3F",\
	"data/pert_ci.txt" u 1:10 with line lw 2 lc rgb "#00FF00"
set output 

set yrange[0:22]

set output "T.tex"
plot "data/pert_ci.txt" u 1:11 with line lw 2 lc rgb "#0000FF",\
	"data/pert_ci.txt" u 1:12 with line lw 2 lc rgb "#FF7F3F",\
	"data/pert_ci.txt" u 1:13 with line lw 2 lc rgb "#00FF00"
set output 