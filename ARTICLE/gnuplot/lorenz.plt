set terminal epslatex size 12cm,10cm standalone
unset key
set xlabel "$X$"
set ylabel "$Y$"
set zlabel "$Z$"

set border 4095
set zrange [0:50]
set xrange [-20:20]
set yrange [-30:30]
set ticslevel 0
set xtics (-20,-10,0,10,20)
set ytics (-20,-10,0,10,20)
set ztics (10,20,30,40)

#set view 340,320

set output "lorenz.tex"
splot "data/path4.txt" u 2:3:4 with line lw 2 lc rgb "#0000FF",\
	"data/path5.txt" u 2:3:4 with line lw 2 lc rgb "#FF7F3F",\
	"data/path6.txt" u 2:3:4 with line lw 2 lc rgb "#00FF00",\
	"data/path7.txt" u 2:3:4 with line lw 2 lc rgb "#FF0000"
set output 