set terminal epslatex size 12cm,10cm standalone

set xlabel "$t$"
set ylabel "$Y$"
set xrange[0:60]
set yrange[0:70]
set xtics(0,10, 20, 30, 40, 50, 60)

set output "lorenzXcomp.tex"
plot "data/path4.txt" u 1:2 with line lw 2 lc rgb "#0000FF",\
	"data/path5.txt" u 1:2 with line lw 2 lc rgb "#FF7F3F",\
	"data/path6.txt" u 1:2 with line lw 2 lc rgb "#00FF00",\
	"data/path7.txt" u 1:2 with line lw 2 lc rgb "#FF0000"
set output

set output "lorenzYcomp.tex"
plot "data/path4.txt" u 1:3 with line lw 2 lc rgb "#0000FF",\
	"data/path5.txt" u 1:3 with line lw 2 lc rgb "#FF7F3F",\
	"data/path6.txt" u 1:3 with line lw 2 lc rgb "#00FF00",\
	"data/path7.txt" u 1:3 with line lw 2 lc rgb "#FF0000"
set output 

set output "lorenzZcomp.tex"
plot "data/path4.txt" u 1:4 with line lw 2 lc rgb "#0000FF",\
	"data/path5.txt" u 1:4 with line lw 2 lc rgb "#FF7F3F",\
	"data/path6.txt" u 1:4 with line lw 2 lc rgb "#00FF00",\
	"data/path7.txt" u 1:4 with line lw 2 lc rgb "#FF0000"
set output 

set output "lorenzXYZ60.tex"
plot "data/path7.txt" u 1:2 with line lw 2 lc rgb "#0000FF",\
	"data/path7.txt" u 1:3 with line lw 2 lc rgb "#FF7F3F",\
	"data/path7.txt" u 1:4 with line lw 2 lc rgb "#00FF00"
set output 

set xrange[0:2]
set xtics(0,.25,.5, .75, 1, 1.25, 1.5,1.75,2)

set output "lorenzXYZ2.tex"
plot "data/path7.txt" u 1:2 with line lw 2 lc rgb "#0000FF",\
	"data/path7.txt" u 1:3 with line lw 2 lc rgb "#FF7F3F",\
	"data/path7.txt" u 1:4 with line lw 2 lc rgb "#00FF00"
set output 