\newpage
\section{Prédiction par assimilation de données}

L'assimilation de données vise à estimer l'état d'un système dynamique à partir d'informations provenant de modèles numériques et d'observations. On notera $U^t(t)$ l'état vrai à l'instant $t$, correspondant à la solution du système dynamique décrit par les équations \ref{eq:princ}. $\mathcal{H}$ désignera l'opérateur des observations et $Y(t)$ l'état du système mesuré à l'instant $t$. Les données auxquelles on a accès pour cette section sont les suivantes : 
\begin{itemize}
	\item les paramètres du système de Lorenz, à savoir $\sigma$, $r$ et $b$,
	\item des mesures de l'état du système (composantes $X$, $Y$ et $Z$) à différents instants. 
\end{itemize}
Les mesures ne sont pas exactes, et sont bruitées par une variable aléatoire gaussienne de moyenne nulle et d'écart-type connu. À partir de ces mesures et de la connaissance de la physique qui régit l'évolution du système, il faut prédire l'évolution du système sur une fenêtre temporelle cible. On retrouve principalement deux familles de méthodes d'assimilations de données dans la littérature à savoir les méthodes variationnelles et les méthodes séquentielles. 

\paragraph{Méthode variationnelle}

Les méthodes d'assimilation sont dites variationnelles lorsqu'elles reposent sur la minimisation d'une fonctionnelle, représentant l'erreur de l'analyse prédictive à partir d'une estimation de l'état du système et des écarts aux observations, qui s'écrit :
\begin{linenomath*}
\[
  J(U) = \frac{1}{2} (U-U^b) B^{-1} (U-U^b)^\top
  +  \frac{1}{2} (Y-H U) R^{-1} (Y-H U)^\top
\]
\end{linenomath*}
où $U^b$ désigne un état d'ébauche, en général issu de l'intégration par le modèle d'évolution de l'état du système analysé à l'étape précédente. $B$ et $R$ sont respectivement les matrices des covariances d'erreur de l'ébauche et des observations. $H$ est obtenue par linéarisation de $\mathcal{H}$. On cherche $U$, l'état initial qui minimise la fonctionnelle $J$. C'est la base de la méthode 3D-Var, qui vise à obtenir l'état optimal à un instant donné et dans laquelle les covariances d'erreur sont statiques sur la période d'analyse. La méthode 4D-Var est l'extension temporelle de 3D-Var. Elle vise à obtenir la trajectoire optimale sur une période de temps donnée, c'est-à-dire celle qui est la plus proche possible de l'ensemble des $d$ observations utilisées :
\begin{linenomath*}
\[
  J(U) = \frac{1}{2} (U-U^b) B^{-1} (U-U^b)^\top
  +  \frac{1}{2} \sum_{i=1}^{d} (Y_i-H_i U_i) R_i^{-1} (Y_i-H_i U_i)^\top
\]
\end{linenomath*}
\paragraph{Méthode séquentielle}

Parmi les méthodes d'assimilation statistiques, le filtre de Kalman (KF) est le plus exploité. Dans ces méthodes, la matrice des covariances d'erreur d'analyse est notée $P^a$ et celle d'ébauche, précédemment notée $B$, s'appelle $P^f$. Le modèle d'évolution linéarisé est noté $M$. Il est appliqué entre deux instants $t_i$ et $t_{i+1}$ et la matrice de covariance d'erreur du modèle à chaque instant sera notée $Q$. À partir des équations du modèle, on détermine $U^f$ l'état prévu du système. Puis à partir de la mesure, on détermine $U^a$ l'état analysé du système. Ainsi l'analyse conduit à :
\begin{linenomath*}
\[
  U^a = U^f + K ( Y - H U^f)
\]
\end{linenomath*}
où la matrice de gain $K$ de prise en compte des observations est donnée par 
\begin{linenomath*}
\[
  K = P^f H^\top ( H P^f H^\top + R )^{-1}
\]
\end{linenomath*}
et
\begin{linenomath*}
\[
  P^a = (I - K H) \, P^f
\]
\end{linenomath*}
L'analyse est propagée par le modèle linéaire  d'évolution, suivant :
\begin{linenomath*}
\[
  {U_{i+1}}^f = M {U_{i}}^a
\]
\end{linenomath*}
et la matrice de covariance d'erreur de prévision est mise à jour par propagation de la matrice de covariance d'analyse de $t_i$ à $t_{i+1}$ par le modèle linéaire.
\begin{linenomath*}
\[
  {P_{i+1}}^f = M {P_{i}}^a M^\top + Q_i
\]
\end{linenomath*}
Ainsi, l'application récursive de ces filtres permet de prendre en compte des covariances d'erreurs variables dans chaque étape d'analyse et d'obtenir une estimation de l'erreur de covariance d'analyse. Le gain de Kalman $K$ permet d'accorder plus ou moins d'importance à nos différentes sources d'informations. En effet, si l'on a confiance en nos mesures alors $K\approx H^{-1}$ et donc $U^a \approx H^{-1} Y$, ce qui est cohérent avec notre confiance sur la mesure. Au contraire, si l'on a confiance en notre modèle, alors $K \approx 0$ et donc $U^a \approx U^f$.

\begin{figure}[!ht]
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{pdf/3d.pdf}	
		\caption{3D-Var}
	\end{subfigure}
	\hfill
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{pdf/4d.pdf}	
		\caption{4D-Var}
	\end{subfigure}\\
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=.5\textwidth]{pdf/k.pdf}	
		\caption{Kalman}
	\end{subfigure}
	\caption{Comparaison des différentes méthodes pour une fenêtre temporelle allant de $t=0$ à $t=20$, avec des mesures parfaites (non représentées ici). En noir la vraie trajectoire et en rouge la trajectoire restituée. Pour les méthodes 3D-Var et 4D-Var une mesure par unité de temps a été réalisée et pour le filtre de Kalman 5 mesures par unité de temps. État initial vrai $U_0 = [0,1,0]^\top$, état initial supposé $U_0=[0, 0.5, 0]^\top$, avec $\sigma=10$, $r=28$, $b=8/3$.}
	\label{fig:method}
\end{figure}

\newpage
On illustre le principe des trois méthodes en réalisant la simulation suivante (figure \ref{fig:method}). La fenêtre de prédiction est $t=20$, on réalise 20 mesures \textbf{parfaites} (sans erreur) pour les méthodes variationnelles et 100 pour la méthode séquentielle. Le vrai état initial est $U_0 = [0,1,0]^\top$, avec $\sigma=10$, $r=28$, $b=8/3$. La supposition initiale est la même pour toutes les méthodes à savoir $U^b_0 = [0,0.5,0]^\top$, qui ici correspond à une mesure fausse de l'état initial. La méthode séquentielle a besoin de plus de mesures que les autres à cause d'un problème de divergence de la méthode. On regarde l'évolution dans le temps de la composante $Y$ sur la fenêtre temporelle considérée. La méthode 3D-Var laisse apparaître des discontinuités, ceci à cause de la façon dont est formulée la méthode. On ne cherche pas à optimiser la prédiction uniquement de manière locale. En ce qui concerne le 4D-Var, la méthode semble moins précise que la précédente mais ne présente pas de discontinuités, car elle vise une optimisation globale. Le filtre de Kalman présente lui aussi des discontinuités à chaque nouvelle mesure. On a eu besoin de cinq fois plus de mesures pour le filtre de Kalman à cause de problèmes de divergence de la méthode.

\paragraph{Comparaison des méthodes} On cherche à comparer la précision des trois méthodes introduites. La méthode utilisée sera similaire à celle effectuée dans cet article Goodliff et al. \cite{Goodliff}. Pour ce faire, on réalise la simulation suivante : on fixe $\sigma=10$, $r=28$, $b=8/3$, on prend une condition initiale appartenant à l'attracteur de Lorenz et on calcule sa trajectoire $U$ sur une fenêtre temporelle $t$ avec un pas d'intégration de $\mathrm{d}t = 10^{-5}$ (on note $N$ le nombre d'états calculés i.e. $N=t/\mathrm dt$). On effectue des mesures avec une fréquence donnée $f$ de mesures par unités de temps physique. On introduit une erreur de mesure pour les composantes $X$, $Y$ et $Z$. Ces erreurs sont les mêmes pour les trois méthodes afin de ne pas en avantager une par rapport aux autres. Les erreurs à chaque mesure suivent toutes la même variable aléatoire : elles sont gaussiennes de moyenne nulle et d'écart-type $\sigma_X$, $\sigma_Y$ et $\sigma_Z$ afin de respecter les hypothèses des différentes méthodes d'assimilation de données. Afin de diminuer les degrés de liberté de la simulation on impose $\sigma_X =  \sigma_Y = \sigma_Z = \sigma_U$. Ainsi pour un $t$ donné, un $f$ donné et un $\sigma_U$ donné chaque méthode construit une trajectoire $U_m$ avec $N$ états successifs calculés. On évalue la précision d'une méthode en évaluant l'erreur quadratique moyenne suivante :
\begin{linenomath*}
\[
	\text{RSME} = \frac{1}{N}\sqrt{\frac{\displaystyle \sum_{i=1}^N \Vert U_m^i- U^i\Vert^2}{3}}
\]
\end{linenomath*}
La différence entre cette simulation et celle de l'article dont elle s'inspire réside dans le choix du pas de temps. L'article en question avait choisit $\mathrm dt = 10^{-2}$, c'est-à-dire comme Lorenz, mais il s'avère que les erreurs numériques sont importantes pour ce pas d'intégration, c'est pourquoi, il est utile de refaire cette simulation. Une valeur de RSME de référence est un RSME proche de 12. Cette valeur peut être obtenue en considérant deux particules décorrélées sur un temps suffisamment long. Un RSME nul correspond à une superposition des deux trajectoires considérées. Enfin, afin de ne pas être influencé par une condition initiale particulière, on réalise le même procédé sur différentes conditions initiales appartenant toutes à l'attracteur de Lorenz et on moyenne les différents résultats. On réalise les simulations pour différents $t$, et différents $\sigma_U$ tout en faisant varier la fréquence de mesure $f$. Les conditions initiales choisies sont les suivantes :
\begin{linenomath*}
\begin{align*}
	U_{0,1} &= [-14.1201584, -6.880168531, 40.71154324]\\
	U_{0,2} &= [-12.2275757, -13.28328434, 28.50731193]\\
	U_{0,3} &= [-4.91588085, -7.675093506, 17.30658601]\\
	U_{0,4} &= [-3.12346395, -3.12529803, 20.69823159]\\
	U_{0,5} &= [10.15287171, 17.13951935, 17.60219045]\\
	U_{0,6} &= [14.7894135, 10.17205532, 39.60479722]
\end{align*}
\end{linenomath*}

On présente les résultats de simulation pour $t=5$ et trois valeurs de $\sigma_U$ différentes (figure \ref{fig:res}). Quand une simulation atteint RSME = 12, cela signifie que la méthode a divergé et a donné un résultat qui sort de l'attracteur de Lorenz. La méthode 3D-Var est moins performante quand la fréquence des mesures diminue car cette méthode est sans mémoire, c'est-à-dire qu'elle ne se base que sur la dernière mesure, ainsi si on n'actualise pas les informations assez fréquemment, l'erreur de mesure ne peut être corrigée. En revanche, la méthode ne semble pas être beaucoup influencée par l'erreur de mesure pour les trois valeurs de $\sigma_U$ testées. Les valeurs de RSME trouvées pour les trajectoires obtenues avec la méthode de 4D-Var présentent une partie décroissante pour des fréquences de mesures faibles et une valeur plateau pour des fréquences de mesures élevées. La méthode 4D-Var est plus précise que la méthode 3D-Var pour des faibles fréquences de mesures, car la méthode 4D-Var est une méthode avec mémoire ainsi l'historique est pris en compte ce qui permet de réduire l'erreur sur la trajectoire construite dans sa globalité. En revanche, à partir d'une certaine fréquence de mesures, la méthode n'arrive plus à améliorer sa précision, les informations supplémentaires fournies saturent la méthode d'assimilation. De plus l'écart-type des mesures $\sigma_U$ a beaucoup d'influence sur la précision de la méthode. Quand $\sigma_U$ augmente le RMSE aussi. Enfin, en ce qui concerne le filtre de Kalman, la méthode offre un RMSE presque systématiquement inférieur à 1. En revanche quand la fréquence de mesures est trop faible, la trajectoire calculée sort de l'attracteur de Lorenz. La méthode est donc un tout ou rien : soit la trajectoire calculée sera très proche de la vraie trajectoire, soit elle en sera très éloignée. Ainsi, pour les hautes fréquences de mesures, on observe de la saturation de la précision, dans le sens où fournir plus d'informations n'améliore pas la prédiction de la trajectoire de la particule. En ce qui concerne les temps d'exécution, la méthode 4D-Var est en moyenne trois fois plus lente que la méthode 3D-Var et le filtre de Kalman (pour cette simulation en particulier la méthode 3D-Var a demandé en moyenne un temps de 35 secondes contre 108 secondes pour la méthode 4D-Var). Pour des très basses fréquences (figure \ref{fig:resb}) de mesures, le filtre de Kalman ne converge pas, c'est pourquoi il n'y a pas la courbe du RSME de Kalman n'est pas présentée en figure \ref{fig:resb}. Les méthodes variationnelles présentent des prédictions similaires. Si l'on ne dispose que d'une seule mesure alors les méthodes 3D-Var et 4D-Var réalisent la même prédiction.

\begin{figure}[!ht]
	\input{tikz/ct5s0}
\\
	\input{tikz/ct5s1}
\end{figure}
\begin{figure}[!ht]\ContinuedFloat
	\input{tikz/ct5s2}
	\caption{Comparaison des méthodes d'assimilation de données en fonction de la fréquence de mesure. Chaque point correspond à une moyenne des RMSE obtenus sur les six conditions initiales présentées. Ici chaque simulation a été réalisée sur le temps $t=5$ et l'écart type des mesures était de $\sigma_U$. RMSE désigne l'erreur quadratique moyenne sur toute la trajectoire entre la vraie trajectoire et la trajectoire obtenue par chacune des méthodes.}
	\label{fig:res}
\end{figure}
\input{tikz/ct5l}
\newpage
\pagebreak
\newpage

\paragraph{Hypothèse de gaussianité}

Pour ces deux familles de méthodes on fait intervenir les matrices de covariances de l'erreur des mesures et des modèles à savoir $R$ et $B$ pour les méthodes variationnelles et les matrices $P$ et $Q$ pour les méthodes séquentielles. Les hypothèses de ces approches supposent l'indépendance, et la gaussianité des variables aléatoires à l'origine des erreurs. Ces hypothèses permettent de construire des matrices diagonales dont la diagonale est les variances de nos différentes variables aléatoires. Cependant, une fois la perturbation introduite, celle-ci va se propager dans le système non-linéaire chaotique, il n'y a donc pas de raisons à ce que les bruits gaussiens du passé soient toujours gaussiens. Afin d'illustrer cette affirmation, la simulation suivante est réalisée. Soit $p$ particules de condition initiale $U_{0,1}$ ayant subies une perturbation dans la direction $Y$. Cette perturbation suit pour chaque particule un bruit gaussien de moyenne nulle et d'écart type 1. On considère aussi la trajectoire $U^*$, celle dont l'état initial est non perturbé. On laisse évoluer ces $p+1$ particules sur une fenêtre temporelle $t$. À chaque pas de temps, on calcule la moyenne, la variance, l'asymétrie et l'aplatissement de la distribution des particules perturbées comparées à la particule non perturbée pour $X$, $Y$ et $Z$. Pour une distribution gaussienne centrée réduite, on doit avoir respectivement les paramètres de distribution suivants : 0, 1, 0, 3. Enfin, on sauvegarde l'histogramme de la distribution à différents instants. La condition initiale $U_{0,1}$ nous permet de commencer dans l'attracteur de Lorenz. La valeur $p$ choisie doit être suffisamment grande pour avoir les résultats les plus précis (l'erreur sur les paramètres de distribution décroit en $\sqrt{p}$), on choisit $p=10^4$. Enfin à la vue des précédents résultats, on s'intéresse à une fenêtre temporelle $t=1$. Les différents graphes sont en figure \ref{fig:evo}. Les paramètres des distributions étudiées ne suivent par les paramètres caractéristiques d'une distribution gaussienne, quelque soit la composante considérée et quelque soit l'instant supérieur à 0 considéré. Les paramètres de distribution ne révèlent pas toute l'information sur ladite distribution c'est pourquoi on a sauvegardé les allures de quelques unes d'entre elles. On représente en figure \ref{fig:distpert} l'histogramme à $t=1$ des composantes $X$ et $Y$. En ce qui concerne la distribution dans la direction $Y$, celle ci a l'allure d'un bruit gaussien, mais fenêtré. En revanche pour la composante $X$, initialement non perturbée, la distribution est plus proche d'une loi de Poisson que d'une distribution gaussienne. Ainsi les erreurs de modèles qui se propagent dans le système ne sont pas gaussiennes et il faudrait en tenir compte pour les méthodes d'assimilation de données. 
\begin{figure}[!ht]
	\begin{subfigure}{.48\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{pdf/M.pdf}
		\caption{Moyenne}
	\end{subfigure}\hfill
	\begin{subfigure}{.48\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{pdf/V.pdf}
		\caption{Variance}
	\end{subfigure}\\
	\begin{subfigure}{.48\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{pdf/S.pdf}
		\caption{Asymétrie}
	\end{subfigure}\hfill
	\begin{subfigure}{.48\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{pdf/T.pdf}
		\caption{Aplatissement}
	\end{subfigure}
	\caption{Paramètre de la distribution système pour le système de Lorenz de paramètres $\sigma = 10$, $b = 8/3$ et $r = 28$ (chaotique). On étudie la propagation de bruit injecté dans les conditions initiales . La composante perturbée est dans la direction Y. La perturbation initiale est un bruit gaussien de moyenne nulle et d'écart-type 1. L'allure de la distribution initiale est en figure \ref{fig:yinit}.}
	\label{fig:evo}
\end{figure}

\input{tikz/disty0}

\begin{figure}[!ht]
	\input{tikz/distx20}
	\hfill
	\input{tikz/disty20}
	\caption{Histogramme de perturbation propagée jusqu'à $t=1$. On a suivi l'évolution de 10 000 particules soumises aux équations de Lorenz de même condition initiales, mais dont la composante $Y$ est perturbée par un bruit gaussien de moyenne nulle et d'écart type 1. On regarde la différence entre les particules perturbées et la particule non perturbée à $t=1$.}
	\label{fig:distpert}
\end{figure}