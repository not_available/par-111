#! /usr/bin/env python
"""
    plot result
"""

" arguments du script "
import argparse
parser = argparse.ArgumentParser(description='Solve dynamical system')
parser.add_argument('-b', '--batch', action='store_true',
                    help='generates pdf file and no display')
#parser.add_argument('-L63', 
#                    help='enter initial conditions [x0, y0, z0], tmax, nsteps')

" profiling "
import time as time

" set up path to the par-111 modules "
import os,sys
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../../CODES-anne/'))
if not path in sys.path:
    sys.path.insert(1, path)
del path

" par-111 modules "
import physical_systems_numpy as phys
import solver as s
import data_manager as iof
import math as m

if __name__ == "__main__":

    args = parser.parse_args()

    import matplotlib
    BATCH = args.batch
    if BATCH:
        matplotlib.use('PDF')
    else:
        matplotlib.use('TkAgg')
    import matplotlib.pyplot as plt

    import numpy as np
    from scipy.integrate import odeint

    import tikzplotlib

    # datapath
    data='data/'

    # Lorenz 63 reference
    cases = [
            [[0,1,0],60,1*10**8]
            ]

    plt.clf()
    for case in cases[:]:
        N = case[2]
        print("cas {:d} : ".format(N))
        start_time = time.time()
        dt=case[1]/N

        filename = data+"res_rk4_{:d}_dts.txt".format(N+1)
        texte = "dt={:.1e}".format(dt)
        print(texte)
        t, X, Y, Z = iof.read_res(filename)
        print('')

        plt.plot(t, Z,  label='$Z$')
        plt.plot(t, Y,  label='$Y$')
        plt.plot(t, X,  label='$X$')
        plt.xlim(0,60)
        plt.ylim(-30,50)
        plt.xlabel("$t$")
        plt.ylabel("$X(t), Y(t), Z(t)$")

        plt.legend(loc='best')

    plt.draw()
    plt.savefig('fig-rk4-t60b.png')

    plt.clf()
    for case in cases[:]:
        N = case[2]
        print("cas {:d} : ".format(N))
        start_time = time.time()
        dt=case[1]/N

        filename = data+"res_rk4_{:d}_dts.txt".format(N+1)
        texte = "dt={:.1e}".format(dt)
        print(texte)
        t, X, Y, Z = iof.read_res(filename)
        print('')

        plt.plot(t, Z,  label='$Z$')
        plt.plot(t, Y,  label='$Y$')
        plt.plot(t, X,  label='$X$')
        plt.xlim(0,3)
        plt.ylim(-30,50)
        plt.xlabel("$t$")
        plt.ylabel("$X(t), Y(t), Z(t)$")

        plt.legend(loc='best')

    plt.draw()
    plt.savefig('fig-rk4-t3b.png')


    # Lorenz 63
    cases = [
            [[0,1,0],60,1*10**5],
            [[0,1,0],60,1*10**6],
            [[0,1,0],60,1*10**7],
            [[0,1,0],60,1*10**8],
            [[0,1,0],60,6*10**3]
            ]
    plt.clf()
    for case in cases[:]:
        N = case[2]
        print("cas {:d} : ".format(N))
        start_time = time.time()
        dt=case[1]/N

        filename = data+"res_rk4_{:d}_dts.txt".format(N+1)
        texte = "dt={:.1e}".format(dt)
        print(texte)
        t, X, Y, Z = iof.read_res(filename)
        print('')

        plt.plot(t, X,  label=texte)
        plt.xlim(0,60)
        plt.ylim(-30,50)
        plt.xlabel("$t$")
        plt.ylabel("$X(t)$")
        plt.legend(loc='best')

    plt.draw()
    plt.savefig('fig-rk4-N-dts-Xb.png')


    plt.clf()
    for case in cases[:]:
        N = case[2]
        print("cas {:d} : ".format(N))
        start_time = time.time()
        dt=case[1]/N

        filename = data+"res_rk4_{:d}_dts.txt".format(N+1)
        texte = "dt={:.1e}".format(dt)
        print(texte)
        t, X, Y, Z = iof.read_res(filename)
        print('')

        plt.plot(t, Z,  label=texte)
        plt.xlim(0,60)
        plt.ylim(-30,50)
        plt.xlabel("$t$")
        plt.ylabel("$Z(t)$")
        plt.legend(loc='best')

    plt.draw()
    plt.savefig('fig-rk4-N-dts-Zb.png')

    plt.clf()
    for case in cases[:]:
        N = case[2]
        print("cas {:d} : ".format(N))
        start_time = time.time()
        dt=case[1]/N

        filename =data+ "res_rk4_{:d}_dts.txt".format(N+1)
        texte = "dt={:.1e}".format(dt)
        print(texte)
        t, X, Y, Z = iof.read_res(filename)
        print('')

        plt.plot(t, Y,  label=texte)
        plt.xlim(0,60)
        plt.ylim(-30,50)
        plt.xlabel("$t$")
        plt.ylabel("$Y(t)$")
        plt.legend(loc='best')

    plt.draw()
    plt.savefig('fig-rk4-N-dts-Yb.png')

    plt.clf()
    ax = plt.axes(projection='3d')
    for case in cases[:]:
        N = case[2]
        print("cas {:d} : ".format(N))
        start_time = time.time()
        dt=case[1]/N

        filename =data+ "res_rk4_{:d}_dts.txt".format(N+1)
        texte = "dt={:.1e}".format(dt)
        print(texte)
        t, X, Y, Z = iof.read_res(filename)
        print('')

        ax.plot(X, Y, Z,linewidth=1)
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')

    plt.draw()
    plt.savefig('phase-rk4-N-dts-b.png')

