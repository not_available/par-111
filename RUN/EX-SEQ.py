#! /usr/bin/env python
"""Lorenz dynamical system

ARGUMENTS
 -h     help
 -b     batch - no X11
"""

import argparse
parser = argparse.ArgumentParser(description='Solve dynamical system')
parser.add_argument('-b', '--batch', action='store_true',
                    help='generates pdf file and no display')
#parser.add_argument('-L63', 
#                    help='enter initial conditions [x0, y0, z0], tmax, nsteps')


import time as time

"Change folder"
import os,sys
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../CODES/'))
if not path in sys.path:
    sys.path.insert(1, path)
del path

from my_objects import Vector, Matrix
import integration as solver
import physical_systems as eq

"The main function"
def run_Lorenz(init=[0,1,0], tmax=20, nsteps=10**5):

    F = eq.Lorenz(sig=10, r=28, b=8/3)
    p = solver.rk4(F, Vector(init), tmax, nsteps)

    return p

if __name__ == "__main__":

    args = parser.parse_args()

    import matplotlib
    BATCH = args.batch
    if BATCH:
        matplotlib.use('PDF')
    else:
        matplotlib.use('TkAgg')
    import matplotlib.pyplot as plt

    cases = [
            [[0,1,0],10,1000],
            [[0,1,0],10,10000],
            [[0,1,0],10,10**5],
            [[0,1,0],20,10**5]
           ]

    start_time = time.time()
    res=[]
    for n in range(0, len(cases)):
        res.append(run_Lorenz(cases[n][0],cases[n][1],cases[n][2]))
    print("Execution time (s): {:.2f}".format((time.time() - start_time)))

    for p in res[:]:
        plt.clf()
        plt.plot(p.time,p.get_line(1))
        plt.draw()
        plt.savefig('res.pdf')
        if (not BATCH):
            print ('SHOW')
            plt.show()
 
