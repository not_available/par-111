"""
Plot DA results
"""

" set up path to the par-111 modules "
import os,sys
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../../../CODES-anne/'))
if not path in sys.path:
    sys.path.insert(1, path)
del path

" par-111 modules "
import data_manager as iof

if __name__ == "__main__":

    import matplotlib
    matplotlib.use('TkAgg')
    import matplotlib.pyplot as plt

    " read results "
    filename = "3dvar_pt_exact_obs.txt"
    t, Xt, Yt, Zt = iof.read_res(filename)
    filename = "3dvar_pm_exact_obs.txt"
    tm, Xm, Ym, Zm = iof.read_res(filename)
    filename = "3dvar_pf_exact_obs.txt"
    tb, Xb, Yb, Zb = iof.read_res(filename)
    filename = "3dvar_pa_exact_obs.txt"
    ta, Xa, Ya, Za = iof.read_res(filename)

    fig=0

    " 3D-var "
    fig=fig+1
    plt.figure(fig)
    plt.clf()
    #plt.plot(ta, Xa, color='tab:green',  label='$X$')
    plt.plot(t,  Xt, color='tab:green', linestyle='--', label='$X^t$')
    #plt.scatter(tm, Xm,color='tab:green',marker='o')
    #plt.plot(tb, Xb, color='tab:green', linestyle=':', label='$X^b$')

    #plt.plot(ta, Ya, color='tab:orange',  label='$Y$')
    plt.plot(t,  Yt, color='tab:orange', linestyle='--', label='$Y^t$')
    #plt.scatter(tm, Ym,color='tab:orange',marker='o')
    #plt.plot(tb, Yb, color='tab:orange', linestyle=':', label='$Y^b$')

    #plt.plot(ta, Za, color='tab:blue',  label='$Z$')
    plt.plot(t,  Zt, color='tab:blue', linestyle='--', label='$Z^t$')
    #plt.scatter(tm, Zm,color='tab:blue',marker='o')
    #plt.plot(tb, Zb, color='tab:blue', linestyle=':', label='$Z^b$')

    plt.ylim(-30,50)
    plt.xlim(0,20)
    plt.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=3)

    plt.xlabel("$t$")
    plt.draw()
    plt.savefig("3Dvar-t20-dt1-pres-v01.pdf", bbox_inches='tight')

    fig=fig+1
    plt.figure(fig)
    plt.clf()
    #plt.plot(ta, Xa, color='tab:green',  label='$X$')
    plt.plot(t,  Xt, color='tab:green', linestyle='--', label='$X^t$')
    plt.scatter(tm, Xm,color='tab:green',marker='o')
    #plt.plot(tb, Xb, color='tab:green', linestyle=':', label='$X^b$')

    #plt.plot(ta, Ya, color='tab:orange',  label='$Y$')
    plt.plot(t,  Yt, color='tab:orange', linestyle='--', label='$Y^t$')
    plt.scatter(tm, Ym,color='tab:orange',marker='o')
    #plt.plot(tb, Yb, color='tab:orange', linestyle=':', label='$Y^b$')

    #plt.plot(ta, Za, color='tab:blue',  label='$Z$')
    plt.plot(t,  Zt, color='tab:blue', linestyle='--', label='$Z^t$')
    plt.scatter(tm, Zm,color='tab:blue',marker='o')
    #plt.plot(tb, Zb, color='tab:blue', linestyle=':', label='$Z^b$')

    plt.ylim(-30,50)
    plt.xlim(0,20)
    plt.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=3)

    plt.xlabel("$t$")
    plt.draw()
    plt.savefig("3Dvar-t20-dt1-pres-v02.pdf", bbox_inches='tight')
 
    fig=fig+1
    plt.figure(fig)
    plt.clf()
    #plt.plot(ta, Xa, color='tab:green',  label='$X$')
    plt.plot(t,  Xt, color='tab:green', linestyle='--', label='$X^t$')
    plt.scatter(tm, Xm,color='tab:green',marker='o')
    plt.plot(tb, Xb, color='tab:green', linestyle=':', label='$X^b$')

    #plt.plot(ta, Ya, color='tab:orange',  label='$Y$')
    plt.plot(t,  Yt, color='tab:orange', linestyle='--', label='$Y^t$')
    plt.scatter(tm, Ym,color='tab:orange',marker='o')
    plt.plot(tb, Yb, color='tab:orange', linestyle=':', label='$Y^b$')

    #plt.plot(ta, Za, color='tab:blue',  label='$Z$')
    plt.plot(t,  Zt, color='tab:blue', linestyle='--', label='$Z^t$')
    plt.scatter(tm, Zm,color='tab:blue',marker='o')
    plt.plot(tb, Zb, color='tab:blue', linestyle=':', label='$Z^b$')

    plt.ylim(-30,50)
    plt.xlim(0,20)
    plt.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=3)

    plt.xlabel("$t$")
    plt.draw()
    plt.savefig("3Dvar-t20-dt1-pres-v03.pdf", bbox_inches='tight')
  
    fig=fig+1
    plt.figure(fig)
    plt.clf()
    plt.plot(ta, Xa, color='tab:green',  label='$X$')
    plt.plot(t,  Xt, color='tab:green', linestyle='--', label='$X^t$')
    plt.scatter(tm, Xm,color='tab:green',marker='o')
    plt.plot(tb, Xb, color='tab:green', linestyle=':', label='$X^b$')

    plt.plot(ta, Ya, color='tab:orange',  label='$Y$')
    plt.plot(t,  Yt, color='tab:orange', linestyle='--', label='$Y^t$')
    plt.scatter(tm, Ym,color='tab:orange',marker='o')
    plt.plot(tb, Yb, color='tab:orange', linestyle=':', label='$Y^b$')

    plt.plot(ta, Za, color='tab:blue',  label='$Z$')
    plt.plot(t,  Zt, color='tab:blue', linestyle='--', label='$Z^t$')
    plt.scatter(tm, Zm,color='tab:blue',marker='o')
    plt.plot(tb, Zb, color='tab:blue', linestyle=':', label='$Z^b$')

    plt.ylim(-30,50)
    plt.xlim(0,20)
    plt.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=3)

    plt.xlabel("$t$")
    plt.draw()
    plt.savefig("3Dvar-t20-dt1-pres-v04.pdf", bbox_inches='tight')

    " 4D var "
    filename = "4dvar_pa_exact_obs.txt"
    ta, Xa, Ya, Za = iof.read_res(filename)

    fig=fig+1
    plt.figure(fig)
    plt.clf()
    plt.plot(ta, Xa, color='tab:green',  label='$X$')
    plt.plot(t,  Xt, color='tab:green', linestyle='--', label='$X^t$')
    plt.scatter(tm, Xm,color='tab:green',marker='o')
    plt.plot(tb, Xb, color='tab:green', linestyle=':', label='$X^b$')

    plt.plot(ta, Ya, color='tab:orange',  label='$Y$')
    plt.plot(t,  Yt, color='tab:orange', linestyle='--', label='$Y^t$')
    plt.scatter(tm, Ym,color='tab:orange',marker='o')
    plt.plot(tb, Yb, color='tab:orange', linestyle=':', label='$Y^b$')

    plt.plot(ta, Za, color='tab:blue',  label='$Z$')
    plt.plot(t,  Zt, color='tab:blue', linestyle='--', label='$Z^t$')
    plt.scatter(tm, Zm,color='tab:blue',marker='o')
    plt.plot(tb, Zb, color='tab:blue', linestyle=':', label='$Z^b$')

    plt.ylim(-30,50)
    plt.xlim(0,20)
    plt.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=3)

    plt.xlabel("$t$")
    plt.draw()
    plt.savefig("4Dvar-t20-dt1-pres-v04.pdf", bbox_inches='tight')


    " KF "
    filename = "KF_pa_exact_obs.txt"
    ta, Xa, Ya, Za = iof.read_res(filename)

    fig=fig+1
    plt.figure(fig)
    plt.clf()
    plt.plot(ta, Xa, color='tab:green',  label='$X$')
    plt.plot(t,  Xt, color='tab:green', linestyle='--', label='$X^t$')
    plt.scatter(tm, Xm,color='tab:green',marker='o')
    plt.plot(tb, Xb, color='tab:green', linestyle=':', label='$X^b$')

    plt.plot(ta, Ya, color='tab:orange',  label='$Y$')
    plt.plot(t,  Yt, color='tab:orange', linestyle='--', label='$Y^t$')
    plt.scatter(tm, Ym,color='tab:orange',marker='o')
    plt.plot(tb, Yb, color='tab:orange', linestyle=':', label='$Y^b$')

    plt.plot(ta, Za, color='tab:blue',  label='$Z$')
    plt.plot(t,  Zt, color='tab:blue', linestyle='--', label='$Z^t$')
    plt.scatter(tm, Zm,color='tab:blue',marker='o')
    plt.plot(tb, Zb, color='tab:blue', linestyle=':', label='$Z^b$')

    plt.ylim(-30,50)
    plt.xlim(0,20)
    plt.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=3)

    plt.xlabel("$t$")
    plt.draw()
    plt.savefig("KF-t20-dt1-pres-v04.pdf", bbox_inches='tight')


