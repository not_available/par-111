import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from error import *
import numpy as np

def sub(l1, l2):
    """Substraction of to lists"""
    return [l1[i] - l2[i] for i in range(len(l1))]

def mul(a, l):
    """Multiplication of a list by an integer"""
    return [a * u for u in l]

class Vector:
    def __init__(self, array):
        """Python list with those operations :
            - addition of two vectors
            - substraction of two vectors
            - multiplication by a scalar
            - multiplication by a matrix (by the left)
            - prompt fonction that prints the list
            - length that returns euclidean length"""
        self.vector = array

    def zeros(d):
        return Vector([0]*d)
    def __len__(self):
        return len(self.vector)
    def __add__(self, vector):
        return Vector([vector.vector[i] + self.vector[i] for i in range(len(vector))])
    def __sub__(self, vector):
        return self + (vector * -1)
    def __mul__(self, obj):
        if type(obj) == Matrix:
            n = len(self.vector)
            j, k = obj.size
            assert n == j
            v = Vector([0 for _ in range(k)])
            for i in range(k):
                v.vector[i] = sum(self.vector[s] * obj.matrix[s][i] for s in range(n))
            return v
        elif type(obj) == Vector:
            return self * Matrix([[obj.vector[i]] for i in range(len(obj.vector))])
        else:
            return Vector([obj * u for u in self.vector])
    def __repr__(self):
        return str(self.vector)
    def length(self):
        return sum(self.vector[i] ** 2 for i in range(len(self.vector))) ** .5

class Path:
    def __init__(self, begin=None, time=0):
        """List of vectors with those operations :
            - addition of a path with a vector, a list, or a path
            - extraction of a column
            - extraction of a line
            - substraction of two paths
            - length of a path that return the length of the largest vector
            - prompt function
            - extraction of some vectors to simulate measurement"""
        if begin is None:
            self.path = []
            self.time = []
        if type(begin) == Vector:
            self.path = [begin]
            self.time = [time]
        if type(begin) == list:
            self.path = begin
            self.time = time
        self.pert_ext = []
            
    def __add__(self, state):
        if type(state) == Vector:
            self.path += [state]
        if type(state) == Path:
            self.path += state.path
            self.time += state.time
    def get_row(self, j):
        return self.path[j]
    def get_line(self, i):
        return [self.path[j].vector[i] for j in range(len(self.path))]
    def __sub__(self, path):
        s = Path()
        s.time = path.time
        for i in range(len(self.path)):
            s + (self.get_row(i) - path.get_row(i))
        return s
    def length(self):
        return max(self.get_row(i).length() for i in range(len(self.path)))
    def __repr__(self):
        return str(self.path)
    def measurement(self, nbr):
        try:
            m = [self.noise[i * len(self.path) // nbr] for i in range(nbr)]
        except:
            m = [self.path[i * len(self.path) // nbr] for i in range(nbr)]
        t = [self.time[i * len(self.path) // nbr] for i in range(nbr)]
        return m, t
    def plot3D(self, fig=None, ax=None, start=0, end=1):
        """Plot phase in 3 dimension space. Only for d > 2.
        If there's no fig or ax, both will be created."""
        N = len(self.path)
        start = int(start * N)
        end = int(end * N)
        if fig is None:
            fig = plt.figure()
            ax = plt.axes(projection='3d')
        ax.plot3D(self.get_line(0)[start:end],
                  self.get_line(1)[start:end],
                  self.get_line(2)[start:end])
        ax.scatter(self.path[start].vector[0],
                   self.path[start].vector[1],
                   self.path[start].vector[2])
    def plot2D(self, i=0, start=0, end=1, color=None):
        """Plot the ith coordinate vs time."""
        N = len(self.path)
        start = int(start * N)
        end = int(end * N)
        plt.plot(self.time[start:end], self.get_line(i)[start:end], color=color)
    def noising(self, sig):
        """in: List of standard deviation"""
        d = len(self.path[0])
        self.noise = []
        self.pdf = [[] for _ in range(d)]
        for i in range(len(self.path)):
            V = []
            for j in range(d):
                err = normale(sig2=sig[j] ** 2)
                V.append(self.path[i].vector[j] + err)
                self.pdf[j].append(err)
            self.noise.append(Vector(V))

class Matrix:
    def __init__(self, matrix):
        """Object that works like a real matrix :
            - addition of two matrix
            - multiplication on the right with a vector, a matrix or a scalar
            - matrix inversion
            - matrix transposition
            - prompt function"""
        self.matrix = matrix
        self.size = len(matrix), len(matrix[0])

    def eye(n):
        return Matrix([[int(i == j) for j in range(n)] for i in range(n)])
    def zeros(S):
        return Matrix([[0] * S[1] for _ in range(S[0])])
    def __add__(self, matrix):
        return Matrix([[self.matrix[i][j] + matrix.matrix[i][j]
                        for j in range(self.size[1])]
                       for i in range(self.size[0])])
    def __sub__(self, matrix):
        return self + (matrix * -1)
    def __mul__(self, obj):
        if type(obj) == Vector:
            n = len(obj.vector)
            assert n == self.size[1]
            v = Vector([0] * self.size[0])
            for i in range(self.size[0]):
                v.vector[i] = sum(self.matrix[i][j] * obj.vector[j] for j in range(n))
            return v
        elif type(obj) == Matrix:
            n, m = obj.size
            assert n == self.size[1]
            M = Matrix([[0] * m for _ in range(self.size[0])])
            for i in range(self.size[0]):
                for j in range(m):
                    M.matrix[i][j] = sum(self.matrix[i][k] * obj.matrix[k][j] for k in range(n))
            return M
        else:
            return Matrix([mul(obj, self.matrix[i]) for i in range(self.size[0])])
    def T(self):
        M = Matrix([[0] * self.size[0] for _ in range(self.size[1])])
        for i in range(self.size[0]):
            for j in range(self.size[1]):
                M.matrix[j][i] = self.matrix[i][j]
        return M
    def __repr__(self):
        return str(self.matrix)
    def inv(self):
        M = np.array(self.matrix)
        I = np.linalg.inv(M)
        return Matrix(I.tolist())
    
    def det(self):            
        return np.linalg.det(self.matrix)
    def __call__(self, x):
        return self * x
