import integration as solver
import my_objects as obj
import physical_systems as phys
from my_objects import plt

def find_maximums(L):
    dL = [L[i+1] - L[i] for i in range(len(L)-1)]
    T = [i for i in range(len(dL)-1) if dL[i+1] < 0 and dL[i] >0]
    return T

F = phys.Lorenz()
X0 = obj.Vector([-3.12346395, -3.12529803, 20.69823159])
tf = 1000
dt = 0.001
T = []
M = []
for i in range(100):
    p = solver.rk4(F, X0, tf, int(tf // dt))
    L = p.get_line(2)
    temp = find_maximums(L)
    T += [u + int(tf // dt) * i for u in temp]
    M += [p.path[i].vector[2] for i in temp]
    X0 = p.path[-1]

plt.scatter(M[:-1], M[1:])
plt.show()
