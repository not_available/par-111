from multiprocessing import Pool

import var3d as VAR3
import var4d as VAR4
import kalman as KAL

from physical_systems import *
from integration import *
from time import time

# set of initial condition such that we are in the attractor
# from 0, 1, 0 at different moments

Xs = [Vector([-3.12346395, -3.12529803, 20.69823159]), 
      Vector([-12.2275757, -13.28328434, 28.50731193]), 
      #Vector([4.87127426, 8.78267131, 11.57329377]),
      Vector([14.7894135, 10.17205532, 39.60479722]),
      #Vector([-1.356437747963595, -2.3874506835994262, 12.39004965753123]),
      #Vector([2.880211660583478, -1.4110908962663096, 27.43011752584817]),
      #Vector([11.943544780638467, 17.236558318684082, 24.468562940034793]),
      #Vector([0.23787273098039982, 0.78380959452991, 16.990699483271705]),
      ##Vector([-9.158409685546623, -2.2311190163321606, 34.78328142284465]),
      #Vector([-4.26790418523538, -7.416601964134486, 12.92555099280229]),
      #Vector([-2.384289093982846, 0.23867068754386495, 24.922221705453676]),
      Vector([10.152871709855022, 17.1395193580067, 17.60219045200671]),
      #Vector([-1.1628361169237469, -3.7990492332972368, 23.53340247263064]),
      Vector([-14.12015847241861, -6.880168531058075, 40.71154323900669]),
      Vector([-4.915880851808321, -7.675093506510434, 17.306586010972598])] 

Ts = [0.01, 0.1, 0.2, 0.25, 0.5, .6, .7, .8, .9, 1, 1.1, 1.2, 1.5, 2, 3, 4, 5]

dt = 10**-5
tf = 5
sig = [10**-2, 10**-2, 10**-2] #standard deviation

def Function(X0):
    
    F = Lorenz()
    FL = LorenzL()
    
    RMSE3, RMSE4, RMSEK = [0 for _ in Ts], [0 for _ in Ts], [0 for _ in Ts]
    N = int(tf / dt)
    temp3, temp4, tempk = 0, 0, 0

    random.seed(a=X0.vector[1])
    p = rk4(F, X0, tf, N)
    p.noising(sig)
    t3, t4, tk = 0, 0, 0
    
    for T in Ts:
        
        Nmes = max(1, int(tf / T))
        
        m, _ = p.measurement(Nmes)
        R = [Matrix([[sig[0]**2, 0, 0], [0, sig[1]**2, 0], [0, 0, sig[2]**2]]) for _ in m]

        V3 = VAR3.VAR_3D(F, B = R[0])
        V4 = VAR4.VAR_4D(F, FL, B = R[0])
        K = KAL.Kalman(F, FL)

        dt3 = time()
        V3(m[0], m, R, T, N // Nmes)
        t3 += time() - dt3
        dt4 = time()
        V4(m[0], m, R, T, N // Nmes)
        t4 += time() - dt4
        #dtk = time()
        #K(m[0], m, R, T, N // Nmes)
        #tk += time() - dtk

        RMSE3[Ts.index(T)] += sum((V3.p.path[i] - p.path[i]).length() / len(V3.p.path) for i in range(len(V3.p.path)) ) / len(Xs) / 3 ** .5
        RMSE4[Ts.index(T)] += sum((V4.p.path[i] - p.path[i]).length() / len(V4.p.path) for i in range(len(V4.p.path)) ) / len(Xs) / 3 ** .5
        #try:
        #    RMSEK[Ts.index(T)] += sum((K.p.path[i] - p.path[i]).length() / len(K.p.path) for i in range(len(K.p.path)) ) / len(Xs) / 3 ** .5
        #except:
        RMSEK[Ts.index(T)] += -50
    return RMSE3, RMSE4, RMSEK

def plot(R3, R4, RK, dt):
    _= plt.plot([t / dt for t in Ts], R3, label=('3D-VAR'), marker='x')
    _= plt.plot([t / dt for t in Ts], R4, label=('4D-VAR'), marker='x')
    _= plt.plot([t / dt for t in Ts], RK, label=('Kalman'), marker='x')
    _= plt.legend()
    _= plt.title(r'$t_f$ = ' + str(tf))
    _= plt.xlabel('# of time step between 2 measures')
    _= plt.ylabel(r'Root mean square error (fractor $3^{-1/2}$)')

if __name__ == "__main__":
    with Pool(10) as p:
        S = (p.map(Function, Xs))
