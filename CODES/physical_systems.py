from my_objects import Vector, Matrix
from math import sin, cos

class Lorenz:
    def __init__(self, sig=10, r=28, b=8/3):
        """Lorenz 63"""
        self.sig = sig
        self.r = r
        self.b = b

    def __call__(self, t, y):
        return Vector([self.sig * (y.vector[1] - y.vector[0]),
		       (self.r - y.vector[2]) * y.vector[0] - y.vector[1],
		       y.vector[0] * y.vector[1] - self.b * y.vector[2]])

class Lorenz96:
    def __init__(self, F):
        """Lorenz 96"""
        self.F = F
    def __call__(self, t, y):
        n = len(y.vector)
        return Vector([(y.vector[i+1-n] - y.vector[i-2]) *\
                       y.vector[i-1] - y.vector[i] + self.F for i in range(n)])

class Pendulum:
    """Classic pendulum"""
    def __init__(self, m, alpha, l, g=9.81):
        self.m = m
        self.alpha = alpha
        self.l = l
        self.g = g
    def __call__(self, t, X):
        x = X.vector
        return Vector([x[1],
                       - self.alpha / self.m * x[1] - self.g / self.l * sin(x[0])])

class LorenzL:
    def __init__(self, sig=10, r=28, b=8/3):
        self.sig = sig
        self.r = r
        self.b = b
    def __call__(self, U=Vector([0, 0, 0])):
        return Matrix([[-self.sig, self.sig, 0],
                       [self.r - U.vector[2], -1, -U.vector[0]],
                       [U.vector[1], U.vector[0], -self.b]])

class PendulumL:
    def __init__(self, m, alpha, l, g=9.81):
        self.m = m
        self.alpha = alpha
        self.l = l
        self.g = g
    def __call__(self, U=Vector([0, 0])):
        return Matrix([[0, 1], [-self.g / self.l * cos(U.vector[0]), -self.alpha / self.m]])
