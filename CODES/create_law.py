import random

class VA:
    def __init__(self, g, a, b, n):
        self.g = g
        self.a = a
        self.b = b
        self.n = n
        self.h = (b - a) / n

        self.i = self.simpson()
        self.F = lambda x : self.i[int((x - self.a) / self.h)] / self.i[-1]
        self.g = lambda x : g(x) / self.i[-1]

    def simpson(self, p=0, t=0):
        s = [0]
        current = self.a
        temp1 = self.g(current) * (current - t) ** p
        temp2 = self.g(current + self.h / 2) * (current + self.h/2 - t) ** p
        temp3 = self.g(current) * (current + self.h - t) ** p
        for i in range(self.n):
            s.append(s[-1] + self.h / 6 * (temp1 + 4 * temp2 + temp3))
            temp1 = temp3
            current = (i + 1) * self.h + self.a
            temp2 = self.g(current + self.h / 2) * (current + self.h / 2 - t) ** p
            temp3 = self.g(current + self.h) * (current + self.h - t) ** p
        return s

    def dichotomie(self, x):
        a = self.a
        b = self.b
        h = (b - a) / 2
        while h > 10 ** -10:
            if self.F(a + h) < x:
                a += h
                h /= 2
            else:
                b -= h
                h /= 2
        return a

    def detail(self):
        self.m = self.simpson(1)[-1] / (self.b - self.a)
        self.v = self.simpson(2, self.m)[-1]
        self.s = self.simpson(3, self.m)[-1] / self.v ** (3/2)
        self.t = self.simpson(4, self.m)[-1] / self.v ** 2

    def __call__(self):
        return self.dichotomie(random.random())
