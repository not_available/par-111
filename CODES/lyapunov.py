from physical_systems import *
from integration import *
import numpy as np

def lyapunov(p, FL, begin=0, step=1):
    dt = p.time[1] - p.time[0]
    d = len(p.path[0].vector)
    n = len(p.path)
    begin = int(n * begin)
    lya = []
    I = np.zeros((d, d))
    u = 0
    for i in range(n-1):
        I = I + dt * np.array(FL(p.path[i]).matrix)
        if i >= begin and u % step == 0:
            lya.append(np.real(np.linalg.eigh(I)[0] / p.time[i+1]))
        u += 1
    return lya
