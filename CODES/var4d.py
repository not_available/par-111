from integration import *
from physical_systems import *

class VAR_4D:
    """4d-Var method. F is the the law of system's evolution. FL(X) is the linearized system in X.
        - B is the covariance matrix of the guess initial condition
        - R is list of the covariance matrix of the measure
        - H allow us to simulate a measure
        - d is the dimension of the problem
        - m is the measurement's set
        - t is the time's measurmeent set
        - N is the number of point that will be calculated for each measure
        - X0 is the initial guess. Can be not that accurate"""
    def __init__(self, F, FL, d=3, H=None, B=None):

        if H is None:
            H = Matrix.eye(d)
        if B is None:
            B = Matrix.eye(d)

        self.Bi = B.inv()
        self.H = H
        self.F = F
        self.FL = FL

        self.sumM = lambda i : sum((i), start=Matrix.zeros((d, d)))
        self.sumV = lambda i : sum((i), start=Vector.zeros(d))

    def cost(self, m):
        X = m[0] - self.X0
        H = [self.m[i] - self.H * m[i] for i in range(self.n)]
        B = self.Bi * .5
        R = [self.Ri[i] * .5 for i in range(self.n)]
        return (X * B * X + self.sumV(H[i] * R[i] * H[i] for i in range(self.n))).vector[0]

    def grad(self, x, m):
        X = x - self.X0
        H = [self.m[i] - self.H * m[i] for i in range(self.n)]
        B = self.Bi
        R = [self.Ri[i] for i in range(self.n)]
        return B * X - self.sumV(self.H.T() * R[i] * H[i] for i in range(self.n))
    
    def newton4(self, x, prec=10**-5, nmax=10**2):
        Hi = (self.Bi + self.sumM(self.Ri)).inv()
        p = rk4(self.F, x, self.dt * self.n, self.N)
        m, _ = p.measurement(self.n)
        self.C.append(self.cost(m)+1)
        self.C.append(self.cost(m))
        gJ = self.grad(x, m)
        x1 = x - Hi * gJ
        compt = 1
        while (self.C[-1] - self.C[-2]) != 0 and compt < nmax:
            dx = x1 - x
            M = [m[i] + self.FL(x) * dx for i in range(self.n)]
            d = [self.m[i] - self.H * M[i] for i in range(self.n)]
            s = self.sumV(self.FL(x).T() * self.H.T() * self.Ri[i] * d[i] for i in range(self.n))
            Hi = (self.Bi + self.sumM(self.FL(x).T() * self.H.T() * self.Ri[i] * self.H * self.FL(x) for i in range(self.n))).inv()
            m = M
            x = x1
            gJ = self.Bi * (x - self.X0) - s
            x1 = x - Hi * gJ* 0.1
            self.C.append(self.cost(m))
            compt += 1
        return rk4(self.F, x1, self.dt * self.n, self.N)

    def __call__(self, X0, m, R, dt, N):
        self.Ri = [R[i].inv() for i in range(len(m))]
        
        self.n = len(m)
        self.m = m
        self.N = N * len(m)
        self.dt = dt
        self.C = []

        self.X0 = X0
        self.p = self.newton4(X0)
