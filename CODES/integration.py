from my_objects import *

def euler(f, y0, t_end, N=10**3, t0=0):
    """COMPLEXITY : O(d N) with d the dimension of the problem and N is N
       ERROR : O(1/N)"""
    p = Path(y0)
    dt = t_end / N 
    for i in range(N):
        c = f(i * dt, p.get_row(-1)) * dt
        p + (c + p.get_row(-1))
    p.time = [t0 + i * dt for i in range(N+1)]
    return p
    
def rk2(f, y0, t_end, N=10**3, t0=0):
    """COMPLEXITY : O(d N) with d the dimension of the problem and N is N
       ERROR : O(1/N^2)"""
    p = Path(y0)
    dt = t_end / N 
    for i in range(N):
        k = f(i * dt, p.get_row(-1))
        c = f((i + 1 / 2) * dt, p.get_row(-1) +  k * (dt / 2)) * dt
        p + (c + p.get_row(-1))
    p.time = [t0 + i * dt for i in range(N+1)]
    return p

def rk4(f, y0, t_end, N=10**3, t0=0, laws=None):
    """COMPLEXITY : O(d N) with d the dimension of the problem and N is N
       ERROR : O(1/N^4)"""
    p = Path(y0)
    dt = t_end / N
    if laws is None:
        pert = lambda : Vector([0 for _ in range(len(y0.vector))])
    else:
        pert = lambda : Vector([l() for l in laws])
    for i in range(N):
        err = pert()
        p.pert_ext.append(err)
        k1 = f(i * dt, p.get_row(-1))
        k2 = f((i + (1)/2) * dt, p.get_row(-1) + k1 * (dt / 2))
        k3 = f((i + (1)/2) * dt, p.get_row(-1) + k2 * (dt / 2))
        k4 = f((i + 1) * dt, p.get_row(-1) + k3 * dt)
        c = (k1 + k2 * 2 + k3 * 2 + k4) * (dt / 6)
        p + (c + p.get_row(-1) + err)
    p.time = [t0 + i * dt for i in range(N+1)]
    return p
