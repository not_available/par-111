from integration import *
from physical_systems import *
from math import exp, log10
from time import time

N_step = [10 ** i for i in range(2, 6)] +\
         [2 * 10 ** i for i in range(2, 6)] +\
         [5 * 10 ** i for i in range(2, 6)]

N_dim = [10 ** i for i in range(1, 4)] +\
        [2 * 10 ** i for i in range(1, 4)] +\
        [5 * 10 ** i for i in range(1, 4)]

N_err = [10 ** i for i in range(1, 4)] +\
        [2 * 10 ** i for i in range(1, 4)] +\
        [5 * 10 ** i for i in range(1, 4)]

def COMPLEXITY_step(IM, N):
    """Complexity estimation, give a sample of N.
    # VALUES THAT MUST BE COMPUTED"""
    f = Lorenz(10, 28, 8/3)
    T = []
    for u in N:
        t = time()
        _ = IM(f, Vector([1, 0, 0]), 1, u)
        T.append(time() - t)
    lN = [log10(u) for u in N]
    lT = [log10(u) for u in T]
    A = [(lT[-i] - lT[-i-1]) / (lN[-i] - lN[-i-1]) for i in range(len(N) - 1)]
    a = max(A)
    b = 10 ** (lT[-1] - a * lN[-1])
    return "{} N ^ {}".format(b, a)

def COMPLEXITY_dim(IM, N):
    """Complexity estimation, give a sample of N.
    DIMENSION OF THE PROBLEME"""
    f = Lorenz96(8)
    T = []
    for u in N:
        t = time()
        _ = IM(f, Vector([1, 0, 0, 0, 0] + [0] * u), 1, 1000)
        T.append(time() - t)
    lN = [log10(u + 5) for u in N]
    lT = [log10(u) for u in T]
    A = [(lT[-i] - lT[-i-1]) / (lN[-i] - lN[-i-1]) for i in range(len(N) - 1)]
    a = max(A)
    b = 10 ** (lT[-1] - a * lN[-1])
    return "{} N ^ {}".format(b, a)

def ERROR(IM, f, sol, x0, N, t_end=1):
    """Error estimation, give a sample of N"""
    err = []
    for u in N:
        p0 = Path(x0)
        for i in range(u):
            p0 + sol((i+1) * t_end / u, x0)
        p = IM(f, x0, t_end, u)
        err.append((p - p0).length())
    lN = [log10(u) for u in N]
    le = [log10(u) for u in err]
    A = [(le[-i] - le[-i-1]) / (lN[-i] - lN[-i-1]) for i in range(len(N) - 1)]
    a = max(A)
    b = 10 ** (le[-1] - a * lN[-1])
    return "{} N ^ {}".format(b, a)

# SOME DIFFERENTIAL EQUATION AND THEIR SOLUTIONS
    
"""
f = lambda t, x: Vector([-x.vector[0]])
sol = lambda t, x0 : Vector([x0.vector[0] * exp(-t)])
#"""
"""
f = lambda t, x: Vector([x.vector[1], x.vector[2], x.vector[3], -4 * x.vector[0]])
sol = lambda t, x0 : Vector([sin(t) * exp(t), (sin(t)+cos(t))*exp(t), 2*cos(t)*exp(t), 2*(cos(t)-sin(t))*exp(t)])
x0 = Vector([0, 1, 2, 2])
#"""
"""
f = lambda t, x: Vector([-2 * t * x.vector[0], (-2+4*t**2)*x.vector[0], x.vector[1]*x.vector[2]/x.vector[0]+8*t*x.vector[0]])
sol = lambda t, x0 : Vector([exp(-t**2), -2*t*exp(-t**2), (4*t**2-2)*exp(-t**2)])
x0 = Vector([1, 0, -2])
#"""
"""
f = lambda t, x : Vector([-x.vector[0]/(t+1), 2 * x.vector[0] ** 3, 3*x.vector[2]*x.vector[0]])
sol = lambda t, x0 : Vector([-1/(t+1), 1/(t+1)**2, -2/(t+1)**3])
x0 = Vector([-1, 1, -2])
#"""
