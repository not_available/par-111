from physical_systems import *
from integration import *

class Kalman:
    def __init__(self, F, FL, d=3, H=None, Q=None):
        """Extended Kalman filter object. Give the exact system's model
        and a way to linearize it everywhere, the measurement matrix
        and covariance matrix due to model and measure."""
        self.F = F
        self.FL = FL
        if H is None:
            H = Matrix.eye(d)
        if Q is None:
            Q = Matrix.eye(d)
        self.H = H
        self.Q = Q

    def prediction(self):
        """Prediction of the future state with the model's matrix.
        Require:
            - X analysis state
            - P covariance matrix analysis state
            - A model matrix
            - Q model error covariance
            - B model matrix of an input
            - U input
        Compute:
            - X forecast state (t+1)
            - P covaraice matrix forecast state (t+1)
        """
        self.Xf = rk4(self.F, self.Xa, self.dt, 1).path[-1]
        self.Pf = self.FL(self.Xa) * self.Pa * self.FL(self.Xa).T() + self.Q
        self.pf.path.append(self.Xf)
        self.pf.time.append((self.step + 1) * self.dt)

    def update(self):
        """Update of the forecast state with the optimal Kalman gain.
        Require:
            - X forcast state
            - P covariance matrix forecast state
            - m measurment
            - H measurement's operator
            - R measurement error covariance
        Compute:
            - X analysis state
            - P covariance matrix analysis state
            - K optimal kalman gain
            - S innovation covariance
        """
        V = self.m[self.step] - self.H(self.Xf)
        S = self.H * self.Pf * self.H.T() + self.R[self.step]
        K = self.Pf * self.H.T() * S.inv()
        self.Xa = self.Xf + K(V)
        self.Pa = self.Pf - K * S * K.T()
        self.pa.path.append(self.Xa)
        self.pa.time.append(self.step * self.dt)

    def __call__(self, X0, m, R, dt, N=None):
        self.Xa = X0
        self.Pa = R[0]
        self.R = R
        self.m = m
        self.step = 0
        self.dt = dt
        self.pa = Path(self.Xa)
        self.pf = Path(self.Xa)
        for _ in range(len(m)-1):
            self.prediction()
            self.step += 1
            self.update()
        if N:
            self.path(N)
    def path(self, N):
        self.paths = [rk4(self.F, self.pa.path[i], self.dt, N, self.dt * i) for i in range(len(self.pa.path))]
        self.p = self.paths[0]
        for i in range(1, len(self.paths)):
            self.p.path += self.paths[i].path[1:]
            self.p.time += self.paths[i].time[1:]

class KalmanL(Kalman):
    def __init__(self, A, H, Q, R):
        """Kalman filter for a linear problem. Give matrix of the model,
        measurement matrix and covariance matrix of model and measure."""
        Kalman.__init__(self, lambda t, X : A(X), lambda X : A, H, Q, R)

# F = Lorenz()
# FL = LorenzL()
# K = Kalman(F, FL)
# X0 = Vector([0, 1, 0])
# X1 = Vector([0, 0.5, 0])
# t = 20
# dt = 10 ** -5
# N = int(t  /dt)
# p = rk4(F, X0, t, N)
# n = 100
# m, T = p.measurement(n)
# R = [Matrix.eye(3) for _ in range(len(m))]
# K(X1, m, R, t/n, int(t/n/dt))
# K.p.plot2D()
# plt.show()
