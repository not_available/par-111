import random
from math import log, sin, cos, pi

def uniforme(a=0, b=1, s=None):
    random.seed(s)
    u = (b - a) * random.random() + a
    random.seed()
    return u

def normale(mu=0, sig2=1, s1=None, s2=None):
    """in: variance"""
    u1, u2 = uniforme(s=s1), uniforme(s=s2)
    z1 = (-2 * log(u1)) ** .5 * cos(2 * pi * u2)
    z2 = (-2 * log(u1)) ** .5 * sin(2 * pi * u2)
    return mu + z1 * sig2 ** .5
