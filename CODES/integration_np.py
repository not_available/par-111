"""
integration en temps en utilisant des tableaux numpy
"""
import numpy as np

def euler(f, y0, t, args=()):
    """COMPLEXITY : O(d N) with d the dimension of the problem and N is N
       ERROR : O(1/N)"""
    n = len(t)
    y = np.zeros((n, len(y0)))
    y[0] = y0
    for i in range(n-1):
        y[i+1] = y[i] + (t[i+1] - t[i]) * f(y[i], t[i], *args)
    return y
    
def rk2(f, y0, t, args=()):
    """COMPLEXITY : O(d N) with d the dimension of the problem and N is N
       ERROR : O(1/N^2)"""
    n = len(t)
    y = np.zeros((n, len(y0)))
    y[0] = y0
    for i in range(n-1):
        h = t[i+1] - t[i]
        y[i+1] = y[i] + h * f(y[i] + f(y[i], t[i], *args) * h / 2., t[i] + h / 2., *args)
    return y

def rk4(f, y0, t, args=()):
    """COMPLEXITY : O(d N) with d the dimension of the problem and N is N
       ERROR : O(1/N^4)"""
    n = len(t)
    y = np.zeros((n, len(y0)))
    y[0] = y0
    for i in range(n-1):
        h = t[i+1] - t[i]
        k1 = f(y[i], t[i], *args)
        k2 = f(y[i] + k1 * h / 2., t[i] + h / 2., *args)
        k3 = f(y[i] + k2 * h / 2., t[i] + h / 2., *args)
        k4 = f(y[i] + k3 * h / 2., t[i] + h, *args)
        y[i+1] = y[i] + h / 6. * (k1 + 2.*k2 + 2.*k3 + k4)
    return y
