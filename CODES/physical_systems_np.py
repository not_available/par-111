"""
    Dynamical systems 
    using numpy 
"""
import numpy as np

class Lorenz:
    def __init__(self, sig=10, r=28, b=8/3):
        """Lorenz 63"""
        self.sig = sig
        self.r = r
        self.b = b
    def __call__(self, y, t):
        return np.array([self.sig * (y[1] - y[0]),\
                        (self.r - y[2]) * y[0] - y[1],\
                        y[0] * y[1] - self.b * y[2]])

class Lorenz96:
    def __init__(self, F):
        """Lorenz 96"""
        self.F = F
    def __call__(self, y, t):
        n = len(y.vector)
        return np.array([(y[i+1-n] - y[i-2]) *\
                         y[i-1] - y[i] + self.F for i in range(n)])

class Pendulum:
    """Classic pendulum"""
    def __init__(self, m, alpha, l, g=9.81):
        self.m = m
        self.alpha = alpha
        self.l = l
        self.g = g
    def __call__(self, X, t):
        x = X.vector
        return np.array([\x[1], \
            - self.alpha / self.m * x[1] - self.g / self.l * sin(x[0])])

class LorenzL:
    def __init__(self, sig=10, r=28, b=8/3):
        self.sig = sig
        self.r = r
        self.b = b
    def __call__(self, U=np.array([0, 0, 0])):
        return np.array([[-self.sig, self.sig, 0], [self.r - U[2], -1, -U[0]], [U[1], U[0], -self.b]])

class PendulumL:
    def __init__(self, m, alpha, l, g=9.81):
        self.m = m
        self.alpha = alpha
        self.l = l
        self.g = g
    def __call__(self, U=np.array([0, 0])):
        return np.array([[0, 1], [-self.g / self.l * cos(U[0]), -self.alpha / self.m]])
