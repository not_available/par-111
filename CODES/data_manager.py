def write_data(p, name, fline=None, start=0, end=1, step=1, dec=16):
    """Write time and componants of p in the file name. You can set the beginnig
    and the end (both betwenn 0 and 1) and skip some values with step"""
    N = len(p.get_row(0))
    n = len(p.time)
    start = int(start * n)
    end = int(end * n)
    with open(name, 'w') as contenu:
        if fline is not None:
            _ = contenu.write(fline + '\n')
        for i in range(start, end, step):
            _ = contenu.write('{}\t'.format(round(p.time[i], dec)))
            for j in range(N):
                _ = contenu.write('{}\t'.format(round(p.get_row(i).vector[j], dec)))
            _ = contenu.write('\n')
    return

def read_data(file, step=1, skip=0):
    """Extract data from file file. You can skip some values."""
    p = Path()
    with open(file, 'r') as contenu:
        u = 0
        j=0
        for lines in contenu:
            if j >= skip:
                if u % step == 0:
                    l = lines.split('\t')
                    s = Vector([float(l[i]) for i in range(1, 4)])
                    p + s
                    p.time.append(float(l[0]))
                u += 1
            j += 1
    return p
