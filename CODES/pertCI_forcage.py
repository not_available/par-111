from integration import *
from error import *
from physical_systems import *
from create_law import *
from math import exp

# ajuster les paramètres pour moyenne nulle intégrale 1 et variance V :
#   prendre un fonction f voulue sur un domaine voulu
#   noter a son intégrale
#   noter m l'intégrale de x f(x)/a
#   noter v l'intégrale de x^2 f(x-m) / a
#   considérer la fonction f(x * sqrt(v / V) - m) / a * sqrt(v / V)

class Data:
    def __init__(self, d=[]):
        self.data = d

    def update(self):
        self.mean()
        self.var()
        self.S()
        self.T()

    def mean(self):
        self.m = sum(self.data) / len(self.data)

    def var(self):
        self.v = sum((d - self.m) ** 2 for d in self.data) / len(self.data)

    def S(self):
        if self.v == 0:
            self.s = 0
        else:
            self.s = sum((d - self.m) ** 3 for d in self.data) / len(self.data) / self.v ** (3/2)

    def T(self):
        if self.v == 0:
            self.t = 0
        else:
            self.t = sum((d - self.m) ** 4 for d in self.data) / len(self.data) / self.v ** 2

def bruit_CI(F, loi, k, X0, comp, t_end, N, samples=0):
    
    d = len(X0.vector)
    err = [loi() for _ in range(k)]
    dt = t_end / N
    X = [X0 + Vector([0] * comp + [c] + [0] * (len(X0.vector) - 1 - comp)) for c in err]
    M, V, S, T = [], [], [], []

    EX = []
    
    for n in range(N+1):
        U = [Data(d=[]) for _ in range(d)]
        for i in range(k):
            for j in range(d):
                U[j].data.append(X[i].vector[j] - X0.vector[j])
            X[i] = rk4(F, X[i], dt, 1).path[-1]

        X0 = rk4(F, X0, dt, 1).path[-1]
        m, v, s, t = [], [], [], []

        if len(EX) <= int(samples * n // N):
            EX.append([[X[i].vector[j] for i in range(k)] for j in range(d)])
        for j in range(d):
            U[j].update()
            m.append(U[j].m)
            v.append(U[j].v)
            s.append(U[j].s)
            t.append(U[j].t)

        M.append(m)
        V.append(v)
        S.append(s)
        T.append(t)

    return M, V, S, T, EX

def forcage(F, loi, k, X0, comp, t_end, N, samples=0):
    d = len(X0.vector)
    dt = t_end / N
    X = [X0 for _ in range(k)]
    M, V, S, T = [], [], [], []
    E = []
    EX = []
    laws = [lambda : 0] * (comp) + [lambda : loi()] + [lambda : 0] * (d - comp-1)
    for n in range(N+1):
        U = [Data(d=[]) for _ in range(d)]
        for i in range(k):
            for j in range(d):
                U[j].data.append(X[i].vector[j] - X0.vector[j])
            p = rk4(F, X[i], dt, 1, laws=laws)
            E.append(p.pert_ext[-1].vector[comp])
            X[i] = p.path[-1]

        X0 = rk4(F, X0, dt, 1).path[-1]
        m, v, s, t = [], [], [], []

        if len(EX) <= int(samples * n // N):
            EX.append([[X[i].vector[j] for i in range(k)] for j in range(d)])
        for j in range(d):
            U[j].update()
            m.append(U[j].m)
            v.append(U[j].v)
            s.append(U[j].s)
            t.append(U[j].t)

        M.append(m)
        V.append(v)
        S.append(s)
        T.append(t)

    return M, V, S, T, E, EX

def save_params(file, M, V, S, T, dt=0.01, step=1, prec=3):
    d = len(M[0])
    with open(file, 'w') as contenu:
        s = 't' + ''.join('\tm{}'.format(i) for i in range(d)) +\
            ''.join('\tv{}'.format(i) for i in range(d)) +\
            ''.join('\ts{}'.format(i) for i in range(d)) +\
            ''.join('\tt{}'.format(i) for i in range(d)) + '\n'
        _ = contenu.write(s)
        for i in range(0, len(V), step):
            f = lambda x : round(x, prec)
            s = '{}'.format(i*dt) + ''.join('\t{}'.format(f(M[i][j])) for j in range(d)) +\
                ''.join('\t{}'.format(f(V[i][j])) for j in range(d)) +\
                ''.join('\t{}'.format(f(S[i][j])) for j in range(d)) +\
                ''.join('\t{}'.format(f(T[i][j])) for j in range(d)) + '\n'
            _ = contenu.write(s)
    return

def save_dist(file, EX, dt=5, samples=100):
    for i in range(len(EX)):
        Y, X = []
        for d in range(len(EX[i])):
            y, x, _ = plt.hist(EX[i][d], samples)
            X.append(x)
            Y.append(y)
        with open(file.format(i * dt), 'w') as contenu:
            for j in range(len(X[0])):
                for d in range(len(EX[0])):
                    _ = contenu.write('{}\t{}\t'.format(X[d][j], Y[d][j]))
                _ = contenu.write('\n')
    return 

# base 1 f(x) = (x - 1) ^ 2 exp(-x^2)
# base 2 f(x) = (x^2 + x + 1) exp(-x^2)
# base 3 f(x) = (x^4 + x + 1) exp(-x^2)
# base 4 f(x) = (x^6 + x + 1) exp(-x^2)

F = Lorenz()
x = [-0.001 + i / 10000000 for i in range(20001)]
P = []
for i in range(len(x)):
    p = rk4(F, Vector([0, 1 + x[i], 0]), 50, 5*10**3)
    m, _ = p.measurement(201)
    P.append(m)
    
