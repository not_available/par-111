from integration import *
from physical_systems import *

class VAR_3D:
    """3d-Var method. F is the the law of system's evolution.
        - B is the covariance matrix of the guess initial condition
        - R is the covariance matrix of the measure
        - H allow us to simulate a measure
        - d is the dimension of the problem
        - m is the measurement's set
        - t is the time's measurmeent set
        - N is the number of point that will be calculated for each measure
        - X0 is the initial guess. Can be not that accurate"""
    def __init__(self, F, d=3, H=None, B=None):

        if H is None:
            H = Matrix.eye(d)
        if B is None:
            B = Matrix.eye(d)

        self.Bi = B.inv()
        self.H = H
        self.F = F
        
    def cost(self, x):
        X = x - self.begin[self.step-1]
        H = self.H(x) - self.m[self.step-1]
        B = self.Bi * .5
        R = self.Ri[self.step-1] * .5
        return (X * B * X + H * R * H).vector[0]

    def grad(self, x):
        X = x - self.begin[self.step-1]
        H = self.H(x) - self.m[self.step-1]
        B = self.Bi
        R = self.Ri[self.step-1]
        return B * X + R * H

    def newton3(self, x, prec=10**-5, nmax=10**2):
        self.C.append(self.cost(x))
        Hi = (self.Bi + self.Ri[self.step]).inv()
        gJ = self.grad(x)
        x1 = x - Hi * gJ
        self.C.append(self.cost(x1))
        compt = 1
        while (self.C[-1] - self.C[-2]) != 0 and compt < nmax:
            x = Vector(x1.vector)
            gJ = self.grad(x)
            x1 = x - Hi * gJ
            self.C.append(self.cost(x1))
            compt += 1
        return x1
    
    def next(self):
        p = rk4(self.F, self.begin[-1], self.dt, self.N, self.dt * self.step)
        self.paths += [p]
        self.guess.append(self.paths[-1].path[-1])
        self.step += 1
        self.begin.append(self.newton3(self.guess[-1]))

    def __call__(self, X0, m, R, dt, N):
        
        self.n = len(m)            
        self.Ri = [R[i].inv() for i in range(len(m))]
        self.N = N
        self.m = m
        self.dt = dt
        self.step = 0
        self.C = []

        self.guess = [X0]
        self.begin = [X0]
        self.paths = []
        
        for _ in range(self.n-1):
            self.next()

        p = rk4(self.F, self.begin[-1], self.dt, self.N, self.dt * (self.n-1))
        self.paths += [p]

        self.p = self.paths[0]
        for i in range(1, len(self.paths)):
            self.p.path += self.paths[i].path[1:]
            self.p.time += self.paths[i].time[1:]

